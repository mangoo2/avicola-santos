<?php
$a=session_id();
if(empty($a)) session_start();
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloCatalogos extends CI_Model {
    public function __construct() {
        parent::__construct();
        if (isset($_SESSION['bodega_tz'])) {
            $this->bodega=$_SESSION['bodega_tz'];
        }else{
            $this->bodega=0;
        }
        
    }

    public function tabla_inserta($tabla,$data){
        $this->db->insert($tabla,$data);   
        return $this->db->insert_id();
    }

    function updateCatalogo_value($data,$info,$catalogo){
        $this->db->set($data);
        $this->db->where($info);
        $this->db->update($catalogo);
    }

    public function getselectwhere_n_consulta($tables,$values){
        $this->db->select("*");
        $this->db->from($tables);
        $this->db->where($values);
        $query=$this->db->get();
        return $query->result();
    }

    public function getGastos(){
        $this->db->select("g.*,u.Usuario");
        $this->db->from("gastos g");
        $this->db->join("usuarios u","u.UsuarioID=g.id_usuario");
        $this->db->where("estatus",1);
        $query=$this->db->get();
        return $query->result();
    }

    //====================== categoria===================================
    function categorias_all() {
        $strq = "SELECT * FROM categoria where activo=1";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function categoriadell($id){
        $strq = "UPDATE categoria SET activo=0 WHERE categoriaId=$id";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function categoriaadd($nom){
        $strq = "INSERT INTO categoria(categoria) VALUES ('$nom')";
        $query = $this->db->query($strq);
        $id=$this->db->insert_id();
        $this->db->close();
        
        return $id;
    }
    function categoriupdate($nom,$id){
        $strq = "UPDATE categoria SET categoria='$nom' WHERE categoriaId=$id";
        $query = $this->db->query($strq);
        $this->db->close();
    }
    function categoriaaddimg($img,$id){
        $strq = "UPDATE categoria SET img='$img' WHERE categoriaId=$id";
        $query = $this->db->query($strq);
        $this->db->close();
    }
    //=======================fin categoria================
    //====================== marcas===================================
    function marcas_all() {
        $strq = "SELECT * FROM marca where activo=1 order by marca asc";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function marcadell($id){
        $strq = "UPDATE marca SET activo=0 WHERE marcaid=$id";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function marcaadd($nom){
        $strq = "INSERT INTO marca(marca) VALUES ('$nom')";
        $query = $this->db->query($strq);
        $id=$this->db->insert_id();
        $this->db->close();
        
        return $id;
    }
    function marcaupdate($nom,$id){
        $strq = "UPDATE marca SET marca='$nom' WHERE marcaid=$id";
        $query = $this->db->query($strq);
        $this->db->close();
    }
    function marcaaddimg($img,$id){
        $strq = "UPDATE marca SET imgm='$img' WHERE marcaid=$id";
        $query = $this->db->query($strq);
        $this->db->close();
    }
    //=======================fin marcas================
    function updatenota($use,$nota){
        $bodega= $_SESSION['bodega_tz'];
        $strq = "UPDATE notas SET mensaje='$nota',usuario='$use',reg=NOW() where bodega=$bodega";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function configticket(){
        $strq = "SELECT * FROM ticket";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function configticketupdate($titulo,$mensaje1,$mensaje2,$fuente,$tamano,$margsup){
        $strq = "UPDATE ticket SET titulo='$titulo',mensaje='$mensaje1',mensaje2='$mensaje2',fuente='$fuente',tamano='$tamano',margensup='$margsup'";
        $query = $this->db->query($strq);
        $this->db->close();
    }
    function marcashuevo_all($idp){
        $strq = "SELECT mar.imgm, mar.marca,mar.marcaid
                FROM sproducto as pro
                inner join marca as mar on mar.marcaid=pro.MarcaId
                where pro.productoid=$idp and pro.activo=1";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function presentacion_all($pro,$mar){
        if ($this->bodega=='2') {
            $stock='pros.stok2';
        }elseif($this->bodega=='3') {
            $stock='pros.stok3';
        }else{
            $stock='pros.stok';
        }
        $strq ="SELECT pre.presentacionId,pre.presentacion, pros.subId,pros.precio,
                pros.precio2,pros.precio3,pros.stok,pros.stok2,pros.stok3
                FROM sproducto as pro
                inner join sproductosub as pros on pros.productoaddId=pro.productoaddId and $stock>=1
                inner join presentaciones as pre on pre.presentacionId=pros.PresentacionId
                WHERE pro.productoId=$pro AND pro.MarcaId=$mar and pro.activo=1
                ";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function searchcliente($search){
        $strq ="SELECT * FROM clientes WHERE activo=1 and Nom like '%$search%'";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function getdescuento($id){
        $strq = "SELECT * FROM descuentos where bodega=$id";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $activado =$row->activado;
        } 
        return $activado;
    }
    function getdescuentoup($sta,$bod){
        $strq = "UPDATE descuentos set activado=$sta where bodega=$bod";
        $query = $this->db->query($strq);
        $this->db->close();
    }
    function metodopagos() {
        $strq = "SELECT * FROM metodopago where activo=1";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function Insert($Tabla,$data){
        $this->db->insert($Tabla, $data);
        $id=$this->db->insert_id();
        return $id;
    }
    function updateCatalogo($Tabla,$data,$where){
        $this->db->set($data);
        $this->db->where($where);
        $this->db->update($Tabla);
        //return $id;
    }
    function getselectwheren($table,$where){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($where);
        $query=$this->db->get(); 
        return $query;
    }

    public function getselect($tables){
        $this->db->select("*");
        $this->db->from($tables);
        //$this->db->where($cols,$values);/// Se puede ocupar un array para n condiciones
        $query=$this->db->get();
        //$this->db->close();
        return $query->result();
    }

    function get_ventas_alias($tipo){
        $strq = "SELECT * FROM ventas WHERE alias = $tipo ORDER BY id_venta DESC LIMIT 1";
        $query = $this->db->query($strq);
        $folio=0;
        foreach ($query->result() as $x) {
            $folio =$x->idventa_alias;
        } 
        return $folio;
    }
    
}