<?php
$a=session_id();
if(empty($a)) session_start();
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloProductos extends CI_Model {
    public function __construct() {
        parent::__construct();
        $this->bodega=$_SESSION['bodega_tz'];
    }
    
    function filas() {
        $strq = "SELECT COUNT(*) as total FROM sproducto where activo=1";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $total =$row->total;
        } 
        return $total;
    }
    /*
    function total_paginados($por_pagina,$segmento) {
        //$consulta = $this->db->get('productos',$por_pagina,$segmento);
        //return $consulta;
        if ($segmento!='') {
            $segmento=','.$segmento;
        }else{
            $segmento='';
        }
        $strq = "SELECT spro.productoaddId,pro.categoria,pro.img, mar.marca,pro.categoriaId,spros.stok,spros.stok2,spros.stok3
                FROM sproducto as spro
                left join sproductosub as spros on spros.productoaddId=spro.productoaddId and spros.tipo=1
                inner JOIN categoria as pro on pro.categoriaId=spro.productoId
                inner join marca as mar on mar.marcaid=spro.MarcaId
                where spro.activo=1
                LIMIT $por_pagina $segmento";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }*/
    function productosall() {
        $strq = "SELECT spro.productoaddId,pro.categoria,pro.img, mar.marca,pro.categoriaId,spros.stok,spros.stok2,spros.stok3,spros.precio_m
                FROM sproducto as spro
                left join sproductosub as spros on spros.productoaddId=spro.productoaddId and spros.tipo=1
                inner JOIN categoria as pro on pro.categoriaId=spro.productoId
                inner join marca as mar on mar.marcaid=spro.MarcaId
                where spro.activo=1";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function total_paginadosp($por_pagina,$segmento) {
        //$consulta = $this->db->get('productos',$por_pagina,$segmento);
        //return $consulta;
        if ($segmento!='') {
            $segmento=','.$segmento;
        }else{
            $segmento='';
        }
        $strq = "SELECT pro.productoid,pro.nombre,pro.img,pro.codigo FROM productos as pro
        inner join categoria as cat on cat.categoriaId=pro.categoria 
        where pro.activo=1 LIMIT $por_pagina $segmento";
        return $strq;
    }
    function productoallsearch($pro){
        //$strq = "SELECT * FROM productos where activo=1 and codigo like '%".$pro."%' or activo=1 and nombre like '%".$pro."%'";
        $strq = "CALL SP_GET_SEARCHPRODUCTO('$pro')";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function categorias() {
        $strq = "SELECT * FROM categoria where activo=1 order by categoria asc";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    public function productosinsert($prod,$marca,$compra){
            $strq = "INSERT INTO sproducto(`productoId`, `MarcaId`,precompra) VALUES ($prod,$marca,'$compra')";
            $this->db->query($strq);
            $id=$this->db->insert_id();
            return $id;
    }
    public function productosupdate($id,$prod,$marca,$compra){
            $strq = "UPDATE sproducto SET productoId='$prod',MarcaId=$marca,precompra='$compra' where productoaddId=$id";
            $this->db->query($strq);
    }
    function productoaddetalleinser($idpro,$presentacion,$cantidad,$precio,$apartirmm,$prepreciomm,$apartirm,$prepreciom,$tipo,$cant2,$cant3){
        $strq = "INSERT INTO sproductosub(productoaddId, PresentacionId, tipo, precio, stok, precio_mm, cantidad_mm, precio_m, cantidad_m, stok2,stok3) 
        VALUES ($idpro,$presentacion,$tipo,'$precio','$cantidad','$prepreciomm','$apartirmm','$prepreciom','$apartirm','$cant2','$cant3')";
            $this->db->query($strq);
            return $strq;
    }

    public function tabla_inserta($tabla,$data){
        $this->db->insert($tabla,$data);   
        return $this->db->insert_id();
    }

    function productoaddetalleupdate($idp,$idpro,$presentacion,$cantidad,$precio,$apartirmm,$prepreciomm,$apartirm,$prepreciom,$tipo,$cant2,$cant3){
        $strq = "UPDATE sproductosub 
                SET productoaddId=$idpro,PresentacionId=$presentacion,tipo=$tipo,
                    precio='$precio',stok='$cantidad',precio_mm=$prepreciomm,cantidad_mm='$apartirmm',precio_m='$prepreciom',cantidad_m='$apartirm',
                    stok2='$cant2',
                    stok3='$cant3'
                    WHERE subId=$idp";
            $this->db->query($strq);
            return $strq;
    }
    public function imgpro($file,$pro){
        $strq = "UPDATE productos SET img='$file' WHERE productoid=$pro";
        $this->db->query($strq);
    }
    function totalproductosenexistencia() {
        $strq = "SELECT ROUND(sum(spro.stok),2) as total 
                 FROM sproductosub as spro
                 inner join sproducto as pro on spro.productoaddId=pro.productoaddId and pro.activo=1
                 where spro.tipo=1
                 ";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $total =$row->total;
        } 
        return $total;
    }
    function totalproductopreciocompra() {
        $strq = "SELECT ROUND(sum(preciocompra),2) as total FROM `productos` where activo=1";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $total =$row->total;
        } 
        return $total;
    }
    function totalproductoporpreciocompra(){
        $strq = "SELECT ROUND(sum(preciocompra*stock),2) as total FROM `productos` where activo=1";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $total =$row->total;
        } 
        return $total;
    }
    function getproducto($id){
        $strq = "SELECT * FROM sproducto where productoaddId=$id";
        $query = $this->db->query($strq);
        return $query;
    }

    function getproductodll($id,$tipo){
        $strq = "SELECT * 
                FROM sproductosub as spro
                inner join presentaciones as pre on pre.presentacionId=spro.PresentacionId
                where spro.productoaddId=$id and spro.tipo=$tipo";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function productosdelete($id){
        $strq = "UPDATE sproducto SET activo=0 WHERE productoaddId=$id";
        $this->db->query($strq);
    }
    function presentallsearch($text){
        $strq = "SELECT * FROM presentaciones where activo=1 and presentacion like '%".$text."%' ORDER BY presentacion ASC";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function getsproductosub($id){
        $strq = "SELECT * FROM sproductosub where subId=$id";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    //                               tipo merma, presentacion
    function addmerma($productoaddId,$marca,$PresentacionId,$cantidad,$bodega){
        //==============================================================
           /*$strq5 = "SELECT * FROM sproducto where productoId=25 and MarcaId=$marca";
            $query5 = $this->db->query($strq5);
            $this->db->close();
            $vid=0;
            foreach ($query5->result() as $row) {
                $vid =$row->productoaddId;
                    $strq8 = "UPDATE sproducto SET activo=1 WHERE productoaddId=$vid";
                    $query8 = $this->db->query($strq8);
                    $this->db->close();
            }
            if ($vid==0) {
                $strq = "INSERT INTO sproducto(`productoId`, `MarcaId`) VALUES (25,$marca)";
                $this->db->query($strq);
                $idmerma=$this->db->insert_id();
                $this->db->close();
                //=============================================
                $strqx = "INSERT INTO sproductosub(productoaddId, PresentacionId, tipo, precio, stok, precio_mm, cantidad_mm, precio_m, cantidad_m) VALUES ($idmerma,$PresentacionId,1,0,$cantidad,0,0,0,0)";
                $this->db->query($strqx);
                $this->db->close();
            }else{
                $idmerma=$vid;
                //==============================================
                $strqz = "SELECT * FROM sproductosub where productoaddId=$idmerma and PresentacionId=$PresentacionId";
                $queryz = $this->db->query($strqz);
                $this->db->close();
                $vids=0;
                foreach ($queryz->result() as $row) {
                    $vids =$row->subId;
                }
                if ($vids==0) {
                    $strq = "INSERT INTO sproductosub(productoaddId, PresentacionId, tipo, precio, stok, precio_mm, cantidad_mm, precio_m, cantidad_m) VALUES ($idmerma,$PresentacionId,1,0,$cantidad,0,0,0,0)";
                    $this->db->query($strq);
                }else{
                    $strq = "UPDATE sproductosub SET stok=stok+$cantidad WHERE subId=$vids";
                    $this->db->query($strq);


                }

            }*/
        
        //====================================
        $strq2 = "SELECT * FROM presentaciones where presentacionId=$PresentacionId";
        $query2 = $this->db->query($strq2);
        $this->db->close();
        $unidad=1;
        foreach ($query2->result() as $row) {
            $unidad =$row->unidad;
        }
        //===================================
        $tounidad=$unidad*$cantidad;
        //================================
        if($bodega==1){
            $stock_edit="stok";
        }else if($bodega==2){
            $stock_edit="stok2";
        }else if($bodega==3){
            $stock_edit="stok3";
        }
        $strq4 = "UPDATE sproductosub SET $stock_edit=$stock_edit-$tounidad WHERE productoaddId=$productoaddId and tipo=1";
        $query4 = $this->db->query($strq4);
        $this->db->close();
        //===============================================


        $strq3 = "SELECT * FROM sproductosub where productoaddId=$productoaddId and tipo=1";
        $query3 = $this->db->query($strq3);
        $this->db->close();
        foreach ($query3->result() as $row) {
            $stok =$row->stok;
        }

         $strq3 = "SELECT * FROM sproductosub where productoaddId=$productoaddId and tipo=0";
        $query3 = $this->db->query($strq3);
        $this->db->close();
        foreach ($query3->result() as $row) {
            $subId =$row->subId;
            $PresentacionId =$row->PresentacionId;

            $strq2 = "SELECT * FROM presentaciones where presentacionId=$PresentacionId";
            $query2 = $this->db->query($strq2);
            $this->db->close();
            $unidad=1;
            foreach ($query2->result() as $row) {
                $unidad =$row->unidad;
            }
            $newstok=$stok/$unidad;

            $strq4 = "UPDATE sproductosub SET stok=$newstok WHERE subId=$subId and tipo=0";
            $query4 = $this->db->query($strq4);
            $this->db->close();
        }

    }
    function traspasos($producto,$origen,$cantidad){
        if ($origen=='2') {
            $stock='stok2';
        }elseif($origen=='3') {
            $stock='stok3';
        }else{
            $stock='stok';
        }
        //===================================
        $strq1 = "SELECT * FROM sproductosub where subId=$producto";
        $query1 = $this->db->query($strq1);
        $this->db->close();
        $PresentacionId=0;
        $productoaddId=0;
        foreach ($query1->result() as $row) {
            $PresentacionId =$row->PresentacionId;
            $productoaddId =$row->productoaddId;
        }
        //====================================
        $strq2 = "SELECT * FROM presentaciones where presentacionId=$PresentacionId";
        $query2 = $this->db->query($strq2);
        $this->db->close();
        $unidad=1;
        foreach ($query2->result() as $row) {
            $unidad =$row->unidad;
        }
        //===================================
        $tounidad=$unidad*$cantidad;
        //================================
        $strq4 = "UPDATE sproductosub SET $stock=$stock-$tounidad WHERE productoaddId=$productoaddId and tipo=1";
        $query4 = $this->db->query($strq4);
        $this->db->close();
        //===============================================


        $strq3 = "SELECT * FROM sproductosub where productoaddId=$productoaddId and tipo=1";
        $query3 = $this->db->query($strq3);
        $this->db->close();
        foreach ($query3->result() as $row) {
            if ($origen=='2') {
                $stok =$row->stok2;
            }elseif ($origen=='3') {
                $stok =$row->stok3;
            }else{
                $stok =$row->stok;
            }
        }

         $strq3 = "SELECT * FROM sproductosub where productoaddId=$productoaddId and tipo=0";
        $query3 = $this->db->query($strq3);
        $this->db->close();
        foreach ($query3->result() as $row) {
            $subId =$row->subId;
            $PresentacionId =$row->PresentacionId;

            $strq2 = "SELECT * FROM presentaciones where presentacionId=$PresentacionId";
            $query2 = $this->db->query($strq2);
            $this->db->close();
            $unidad=1;
            foreach ($query2->result() as $row) {
                $unidad =$row->unidad;
            }
            $newstok=$stok/$unidad;

            $strq4 = "UPDATE sproductosub SET $stock=$newstok WHERE subId=$subId and tipo=0";
            $query4 = $this->db->query($strq4);
            $this->db->close();
        }
        return $strq4;
    }
    function traspasos2($producto,$destino,$cantidad){
        if ($destino=='2') {
            $stock='stok2';
        }elseif($destino=='3') {
            $stock='stok3';
        }else{
            $stock='stok';
        }
        $strq = "SELECT * FROM sproductosub where productoaddId=$producto and tipo=1";
        $query = $this->db->query($strq);
        $this->db->close();
        $subId=0;
        foreach ($query->result() as $row) {
            $subId =$row->subId;
            $PresentacionId = $row->PresentacionId;
        }
        if ($subId>0) {
            //============================================================================
            $strq2 = "SELECT * FROM presentaciones where presentacionId=$PresentacionId";
            $query2 = $this->db->query($strq2);
            $this->db->close();
            $unidad=1;
            foreach ($query2->result() as $row) {
                $unidad =$row->unidad;
            }
            //===================================
            $tounidad=$unidad*$cantidad;
            //================================
            $strq4 = "UPDATE sproductosub SET $stock=$stock+$tounidad WHERE productoaddId=$producto and tipo=1";
            $query4 = $this->db->query($strq4);
            $this->db->close();
            //===============================================
            //===============================================
            $strq3 = "SELECT * FROM sproductosub where productoaddId=$producto and tipo=1";
            $query3 = $this->db->query($strq3);
            $this->db->close();
            foreach ($query3->result() as $row) {
                if ($destino=='2') {
                    $stok =$row->stok2;
                }elseif ($destino=='3') {
                    $stok =$row->stok3;
                }else{
                    $stok =$row->stok;
                }
            }

             $strq3 = "SELECT * FROM sproductosub where productoaddId=$producto and tipo=0";
            $query3 = $this->db->query($strq3);
            $this->db->close();
            foreach ($query3->result() as $row) {
                $subId =$row->subId;
                $PresentacionId =$row->PresentacionId;

                $strq2 = "SELECT * FROM presentaciones where presentacionId=$PresentacionId";
                $query2 = $this->db->query($strq2);
                $this->db->close();
                $unidad=1;
                foreach ($query2->result() as $row) {
                    $unidad =$row->unidad;
                }
                $newstok=$stok/$unidad;

                $strq4 = "UPDATE sproductosub SET $stock=$newstok WHERE subId=$subId and tipo=0";
                $query4 = $this->db->query($strq4);
                $this->db->close();
            }
        }
    }
    function traspasos3($producto,$destino,$cantidad){
        if ($destino=='2') {
            $stock='stok2';
        }elseif($destino=='3') {
            $stock='stok3';
        }else{
            $stock='stok';
        }
        $strq = "SELECT * FROM sproductosub where productoaddId=$producto and tipo=1";
        $query = $this->db->query($strq);
        $this->db->close();
        $subId=0;
        foreach ($query->result() as $row) {
            $subId =$row->subId;
            $PresentacionId = $row->PresentacionId;
        }
        if ($subId>0) {
            //============================================================================
            $strq2 = "SELECT * FROM presentaciones where presentacionId=$PresentacionId";
            $query2 = $this->db->query($strq2);
            $this->db->close();
            $unidad=1;
            foreach ($query2->result() as $row) {
                $unidad =$row->unidad;
            }
            //===================================
            $tounidad=$unidad*$cantidad;
            //================================
            $strq4 = "UPDATE sproductosub SET $stock=$stock-$tounidad WHERE productoaddId=$producto and tipo=1";
            $query4 = $this->db->query($strq4);
            $this->db->close();
            //===============================================
            //===============================================
            $strq3 = "SELECT * FROM sproductosub where productoaddId=$producto and tipo=1";
            $query3 = $this->db->query($strq3);
            $this->db->close();
            foreach ($query3->result() as $row) {
                if ($destino=='2') {
                    $stok =$row->stok2;
                }elseif ($destino=='3') {
                    $stok =$row->stok3;
                }else{
                    $stok =$row->stok;
                }
            }

             $strq3 = "SELECT * FROM sproductosub where productoaddId=$producto and tipo=0";
            $query3 = $this->db->query($strq3);
            $this->db->close();
            foreach ($query3->result() as $row) {
                $subId =$row->subId;
                $PresentacionId =$row->PresentacionId;

                $strq2 = "SELECT * FROM presentaciones where presentacionId=$PresentacionId";
                $query2 = $this->db->query($strq2);
                $this->db->close();
                $unidad=1;
                foreach ($query2->result() as $row) {
                    $unidad =$row->unidad;
                }
                $newstok=$stok/$unidad;

                $strq4 = "UPDATE sproductosub SET $stock=$newstok WHERE subId=$subId and tipo=0";
                $query4 = $this->db->query($strq4);
                $this->db->close();
            }
        }
    }
    function deleterowv($id){
        $strq2 = "SELECT * FROM venta_detalle where id_producto=$id";
        $query2 = $this->db->query($strq2);
        $this->db->close();
        $row=0;
        foreach ($query2->result() as $row) {
            $row=1;
        }
        return $row;
    }
    function deleterow($id){
        $strq2 = "DELETE FROM sproductosub WHERE subId=$id";
        $query2 = $this->db->query($strq2);
        $this->db->close();
    }
    function getlistprodutos($params){
        /*$strq = "SELECT spro.productoaddId,pro.categoria,pro.img, mar.marca,pro.categoriaId,spros.stok,spros.stok2,spros.stok3
                FROM sproducto as spro
                left join sproductosub as spros on spros.productoaddId=spro.productoaddId and spros.tipo=1
                inner JOIN categoria as pro on pro.categoriaId=spro.productoId
                inner join marca as mar on mar.marcaid=spro.MarcaId
                where spro.activo=1
                LIMIT $por_pagina $segmento";
        */

        $columns = array( 
            0=>'spro.productoaddId',
            1=>'pro.img',
            2=>'pro.categoria',
            3=>'mar.marca',
            4=>'spros.stok',
            5=>'spros.stok2',
            6=>'spros.stok3',
            7=>'pro.categoriaId',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('sproducto spro');
        $this->db->join('sproductosub spros', 'spros.productoaddId=spro.productoaddId','left');
        $this->db->join('categoria pro', 'pro.categoriaId=spro.productoId');
        $this->db->join('marca mar', 'mar.marcaid=spro.MarcaId');
        $this->db->where(array('spro.activo'=>1,'spros.tipo'=>1));    

        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    public function getlistprodutost($params){
        $columns = array( 
            0=>'spro.productoaddId',
            1=>'pro.img',
            2=>'pro.categoria',
            3=>'mar.marca',
            4=>'spros.stok',
            5=>'spros.stok2',
            6=>'spros.stok3',
            7=>'pro.categoriaId',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(*) as total');
        $this->db->from('sproducto spro');
        $this->db->join('sproductosub spros', 'spros.productoaddId=spro.productoaddId','left');
        $this->db->join('categoria pro', 'pro.categoriaId=spro.productoId');
        $this->db->join('marca mar', 'mar.marcaid=spro.MarcaId');
        $this->db->where(array('spro.activo'=>1,'spros.tipo'=>1));

        //$where = ;
        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        //$this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        //$this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query->row()->total;
    }

    function getlistMermas($params){
        $columns = array( 
            0=>'m.id',
            1=>'pro.categoria',
            2=>'mar.marca',
            3=>'tipo_merma',
            4=>'p.presentacion as namepresenta',
            5=>'bodega',
            6=>'cantidad',
            7=>'motivo',
            8=>'u.Usuario',
            9=>'fecha_reg',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('mermas m');
        $this->db->join('sproducto spro', 'spro.productoaddId=m.id_producto');
        $this->db->join('sproductosub spros', 'spros.productoaddId=spro.productoaddId');
        $this->db->join('categoria pro', 'pro.categoriaId=spro.productoId');
        $this->db->join('presentaciones p', 'p.presentacionId=m.presentacion');
        $this->db->join('marca mar', 'mar.marcaid=spro.MarcaId');
        $this->db->join('usuarios u', 'u.UsuarioId=m.id_usuario');
        $this->db->where('m.estatus',1);
        $this->db->group_by('m.id');

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    public function getTotalMermas($params){
        $columns = array( 
            0=>'m.id',
            1=>'pro.categoria',
            2=>'mar.marca',
            3=>'tipo_merma',
            4=>'p.presentacion as namepresenta',
            5=>'bodega',
            6=>'cantidad',
            7=>'motivo',
            8=>'u.Usuario',
            9=>'fecha_reg',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(*) as total');
        $this->db->from('mermas m');
        $this->db->join('sproducto spro', 'spro.productoaddId=m.id_producto');
        $this->db->join('sproductosub spros', 'spros.productoaddId=spro.productoaddId');
        $this->db->join('categoria pro', 'pro.categoriaId=spro.productoId');
        $this->db->join('presentaciones p', 'p.presentacionId=m.presentacion');
        $this->db->join('marca mar', 'mar.marcaid=spro.MarcaId');
        $this->db->join('usuarios u', 'u.UsuarioId=m.id_usuario');
        $this->db->where('m.estatus',1);
        $this->db->group_by('m.id');
        
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $query=$this->db->get();
        return $query->row()->total;
    }

}