<div class="row">
    <div class="col-md-12">
      
    </div>
    <div class="col-xl-3 col-lg-6 col-md-6 col-12 mx-auto">
        <div class="card gradient-blackberry">
            <div class="card-body">
                <div class="card-block pt-2 pb-0">
                    <div class="media">
                        <div class="media-body white text-left">
                            <h3 class="font-large-1 mb-0"><?php echo number_format($total_rows,0,'.',',');?></h3>
                            <span>Productos registrados</span>
                        </div>
                        <div class="media-right white text-right">
                            <i class="icon-pie-chart font-large-1"></i>
                        </div>
                    </div>
                </div>
                <div id="Widget-line-chart" class="height-75 WidgetlineChart WidgetlineChartshadow mb-2">                   
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-lg-6 col-md-6 col-12 mx-auto">
        <div class="card gradient-ibiza-sunset">
            <div class="card-body">
                <div class="card-block pt-2 pb-0">
                    <div class="media">
                        <div class="media-body white text-left">
                            <h3 class="font-large-1 mb-0"><?php echo number_format($totalexistencia,0,'.',',');?></h3>
                            <span>Productos en existencia</span>
                        </div>
                        <div class="media-right white text-right">
                            <i class="icon-bulb font-large-1"></i>
                        </div>
                    </div>
                </div>
                <div id="Widget-line-chart1" class="height-75 WidgetlineChart WidgetlineChartshadow mb-2">                  
                </div>

            </div>
        </div>
    </div>

  </div>
  <!--Statistics cards Ends-->

  <!--Line with Area Chart 1 Starts-->
  <div class="row">
    <div class="col-sm-12">
        <!--<div class="card">
            <div class="card-header">
                <h4 class="card-title">PRODUCTS SALES</h4>
            </div>
            <div class="card-body">
                <div class="card-block">
                    
                </div>
            </div>
        </div>-->
        <div class="col-md-5 mx-auto"></div>
        <?php if (isset($_SESSION['perfilid_tz'])) { if ($_SESSION['perfilid_tz']==1) { ?>
                <div class="col-md-4 mx-auto">
                      <div class="form-group ">
                          <input type="checkbox" id="descuento" class="switchery" data-size="lg" data-on-text="SI" data-off-text="NO" <?php if ($getdescuento==1) { echo "checked";}?> />
                          <label for="descuento" class="font-medium-2 text-bold-600 ml-1">Descuento</label>
                          
                      </div>
                  </div>                
        <?php } } ?>
    </div>
  </div>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/switchery.min.css">
    <script src="<?php echo base_url(); ?>app-assets/vendors/js/switchery.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>app-assets/js/switch.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function($) { 
            $('#descuento').change(function(event) {
                var descuento = $('#descuento').is(':checked')==true?1:0;
                $.ajax({
                    type: 'POST',
                    url: 'Inicio/descuento',
                    data: {des: descuento,bod:1},
                    async: false,
                    statusCode: {
                        404: function(data) {
                            notification("topright", "error", "fa fa-exclamation-triangle vd_yellow", "Excepción!", 'No se encuentra el archivo');
                        },
                        500: function(data) {
                            notification("topright", "error", "fa fa-exclamation-triangle vd_yellow", "Excepción!", data);
                        }
                    },
                    success: function(data) {

                    }
                });
            });  
        });
    </script>