<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/print.css" media="print">
<style type="text/css">
    .personalbodega{
        display: none;
    }
    .div_bottms{
        width: 128px;
    }
    #data-tables-ventas td{
        font-size: 12px;
    }
</style>
<div class="row">
                <div class="col-md-12">
                  <h2>Lista ventas</h2>
                </div>
              </div>
              <!--Statistics cards Ends-->

              <!--Line with Area Chart 1 Starts-->
              <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Listado de ventas</h4>
                        </div>
                        <div class="col-md-12 inputbusquedas">
                          <div class="col-md-11">
                            
                          </div>
                          <div class="col-md-1">
                            <!--<a id="btnImprimir" onclick="imprimir();"><button type="button" class="btn btn-raised gradient-purple-bliss white"  style="background: #2e58a6;" ><i class="fa fa-print"></i></button></a>-->
                          </div>
                          <div class="col-md-2 personalbodega">
                            <select class="form-control" id="personalbodega" onchange="seleccionar()">
                              <option value="0">Todo</option>
                              <option value="1" <?php if (isset($_GET['bod'])) { if ($_GET['bod']==1) { echo "selected"; } }?>  >Matriz</option>
                            </select>
                          </div>
                          <div class="col-md-2">
                            <select class="form-control" id="tipo_dia" onchange="seleccionar_dia()">
                              <option value="0">Todo</option>
                              <option value="1">Domingo</option>
                              <option value="2">Lunes a sábado</option>
                            </select>
                          </div>
                        </div>
                        <div class="card-body">
                            <div class="card-block">
                                <!--------//////////////-------->
                                <table class="table table-striped" id="data-tables-ventas" style="width: 100%">
                                      <thead>
                                        <tr>
                                          <th>Folio</th>
                                          <th>Fecha</th>
                                          <th>Vendedor</th>
                                          <th>Monto</th>
                                          <th>Metodo</th>
                                          <th>Cliente</th>
                                          <th></th>
                                          <th></th>
                                        </tr>
                                      </thead>
                                      <tbody></tbody>
                                </table>    
                        <!--------//////////////-------->
                            </div>
                        </div>
                    </div>
                </div>
              </div>
<!------------------------------------------------>
<style type="text/css">
    #iframereporte{
        background: white;
    }
    iframe{
        height: 500px;
        border:0;
        width: 100%;
    }
</style>
<div class="modal fade text-left" id="iframeri" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Ticket</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <!--<div class="modal-body">-->
            <div id="iframereporte"></div>
            <!--</div>-->
            <div class="modal-footer">
                <button type="button" class="btn btn-raised gradient-ibiza-sunset white" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade text-left" id="cancelar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Mensaje de confirmaci&oacute;n</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ¿Desea cancelar el ticket No. <span id="NoTicket"></span> por un total de <span id="CantidadTicket"></span>?
                      <input type="hidden" id="hddIdVenta">
            </div>
            <div class="modal-footer">
                <button type="button" id="sicancelar" class="btn btn-raised gradient-purple-bliss white" data-dismiss="modal">Aceptar</button>
                <button type="button" class="btn btn-raised gradient-ibiza-sunset white" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade text-left" id="modaledit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Ticket</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body itemsventasedit">
            </div>
            <div class="modal-footer">
                <button type="button" id="sieditar" class="btn btn-raised gradient-purple-bliss white">Aceptar</button>
                <button type="button" class="btn btn-raised gradient-ibiza-sunset white" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade text-left" id="modalpays" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Pagos a venta</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table>
                    <thead>
                        <tr>
                            <th><h4 class="modal-title"><b># Venta: </b></h4></th>
                            <th><h4 class="modal-title" id="idtxt"></h4></th>
                        </tr>
                        <tr>
                            <th><h4 class="modal-title"><b>Total venta: </b></h4>
                                <input type="hidden" id="totalv">
                            </th>
                            <th>
                                <h4 class="modal-title" id="montotxt"></h4>
                            </th>
                        </tr>
                        <tr>
                            <th><h4 class="modal-title"><b>Total pagos: </b></h4>
                                <input type="hidden" id="totalp">
                            </th>
                            <th><h4 class="modal-title" id="pagostxt"></h4></th>
                        </tr>
                        <tr>
                            <th><h4 class="modal-title"><b>Resta: </b></h4>
                                <input type="hidden" id="resta">
                            </th>
                            <th><h4 class="modal-title" id="restatxt"></h4></th>
                        </tr>
                    </thead>
                </table>
                <div class="col-md-12">
                    <hr>
                </div>
                <div class="row col-md-12">
                    <form class="form" method="post" role="form" id="form_pay">
                        <input type="hidden" name="id_venta" id="id_venta" class="form-control" value="0">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Folio:</label>
                                <input type="text" name="folio" id="folio" class="form-control" value="">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Monto:</label>
                                <input type="number" name="monto" id="monto" class="form-control" value="">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Fecha:</label>
                                <input type="date" name="fecha" id="fecha" class="form-control" value="">
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="form-group">
                                <label style="color:transparent;" class="col-sm-3 control-label">add</label>
                                <button id="savepay" type="button" class='btn btn-success'><i class='fa fa-plus-square' aria-hidden='true'></i></button>
                            </div>
                            
                        </div>
                    </form>
                </div>
                <div class="col-md-12">
                    <hr>
                    <h4 class="modal-title">Pagos asignados</h4>
                    <div id="cont_pays">
                        <table class="table table-striped" id="table_pays" style="width: 100%">
                            <thead>
                                <tr>
                                  <th>Folio</th>
                                  <th>Monto</th>
                                  <th>Fecha</th>
                                  <th></th>
                                </tr>
                            </thead>
                            <tbody id="body_pays"></tbody>
                        </table> 
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-raised gradient-ibiza-sunset white" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade text-left" id="modalpass" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true" >
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel1">Autorizar Cancelación</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <div class="form-group">
              <label class="col-sm-3 control-label">Contraseña</label>
              <div class="col-sm-8 controls">
                <input type="password" autocomplete="off" class="input-border-btm form-control" id="pass_user">
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" onclick="validaPass()" class="btn grey btn-outline-secondary" id="verifipass">Aceptar</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
    var id_ticket=0; var cantidadTick=0;
  $(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();
    
  });
  function ticket(id){
    $("#iframeri").modal();
    $('#iframereporte').html('<iframe src="<?php echo base_url(); ?>Visorpdf?filex=Ticket&iden=id&id='+id+'"></iframe>');
  }
  function cancelar(ID,CantidadTicket){
    $("#modalpass").modal();
    $("#pass_user").val("");
    id_ticket=ID;
    cantidadTick=CantidadTicket;
    /*$('#cancelar').modal();
    $("#hddIdVenta").val(ID);
    $("#NoTicket").html(ID);
    $("#CantidadTicket").html(CantidadTicket);  */
  }

  function seleccionar(){
    var bode=$('#personalbodega option:selected').val();
    location.href="<?php echo base_url(); ?>ListaVentas?bod="+bode;
  }

  function seleccionar_dia(){
    loadtable();  
  }
  
  function imprimir(){
      window.print();
  }

    function validaPass(){
        if ($("#pass_user").val().trim()!="") {
            $.ajax({
                type: 'POST',
                url: base_url+'Inicio/verificarPass',
                data: {pass: $("#pass_user").val()},
                async: false,
                success: function(data) {
                    //console.log(data);
                    if (data==1) {
                        $("#pass_user").val("");
                        $("#modalpass").modal("hide");
                        toastr.success('Cancelación autorizada','Correcto!');
                        $('#cancelar').modal();
                        $("#hddIdVenta").val(id_ticket);
                        $("#NoTicket").html(id_ticket);
                        $("#CantidadTicket").html(cantidadTick);  
                    }else{
                        $("#pass_user").val("");
                        $("#modalpass").modal("hide");
                        toastr.error('Error', 'Contraseña incorrecta');
                    }
                }
            });
        }else{
            toastr.error('Error', 'Ingrese una contraseña');
        }
    }

  
</script>