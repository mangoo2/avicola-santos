<!DOCTYPE html><html lang="es" class="loading">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <meta name="author" content="Mangoo agb">
        <title>POS Avicola Santos</title>
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url(); ?>public/img/logo.png">
        <link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>public/img/logo.png">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-touch-fullscreen" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"><!--colocar el mismo color a la barra de estado superiori-->
        <meta name="description" content="Sistema de punto de venta">
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,700,900|Montserrat:300,400,500,600,700,800,900" rel="stylesheet">

        <meta name="theme-color" content="#2f58a6"/>
        
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/fonts/feather/style.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/fonts/simple-line-icons/style.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/fonts/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/perfect-scrollbar.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/prism.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/sweetalert2.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/tables/datatable/datatables.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/toastr.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/chosen.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/balloon.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/pickadate/pickadate.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/js/formvalidation/formValidation.min3f0d.css?v2.2.0">
        
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/app.css">   
        <script src="<?php echo base_url(); ?>app-assets/vendors/js/core/jquery-3.2.1.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>app-assets/vendors/js/datatable/datatables.min.js" type="text/javascript"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/select2.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/animate.css">
        
        <link rel="manifest" href="<?php echo base_url(); ?>manifest.json">
        <script async src="<?php echo base_url(); ?>public/js/jquery.numpad.js"></script>
        <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/jquery.numpad.css">

        <link href="<?php echo base_url();?>plugins/confirm/jquery-confirm.min.css" type="text/css" rel="stylesheet">
        
        <input type="hidden" name="ssessius" id="ssessius" value="<?php if(isset($_SESSION['usuarioid_tz'])){  echo $_SESSION['usuarioid_tz'];}?>" readonly>
        <input type="hidden" id="base_url_g" value="<?php echo base_url(); ?>">
    </head>
    <style type="text/css">
        table.dataTable thead > tr > th.sorting_asc, table.dataTable thead > tr > th.sorting_desc, table.dataTable thead > tr > th.sorting, table.dataTable thead > tr > td.sorting_asc, table.dataTable thead > tr > td.sorting_desc, table.dataTable thead > tr > td.sorting{
          background-color: rgb(255, 189, 1, 0.8);
          color:white;
          font-weight: bold;
        }
        .page-item.active .page-link {
            z-index: 2;
            color: #FFFFFF !important;
            background-color: rgb(255, 189, 1, 0.8) !important;
            border-color: rgb(255, 189, 1, 0.8) !important;
        }
        .page-link {
            position: relative;
            display: block;
            padding: 0.5rem 0.75rem;
            margin-left: -1px;
            line-height: 1.25;
            color: rgb(255, 189, 1, 0.8);
            background-color: #FFFFFF;
            border: 1px solid #DDDDDD;
        }
    </style>
    <body data-col="2-columns" class=" 2-columns ">
        <!-- ////////////////////////////////////////////////////////////////////////////-->
            <div class="wrapper">