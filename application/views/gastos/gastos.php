<div class="row">
    <div class="col-md-12">
      <h2>Gastos</h2>
    </div>    
</div>

  <div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Listado de gastos</h4>
            </div>
            <div class="col-md-12">
              <div class="col-md-8">
                <button type="button" id="addGasto" class="btn btn-raised gradient-ibiza-sunset white sidebar-shadow"><i class="fa fa-plus"></i></span> Nuevo</button>
              </div>
              
            </div>
            <div class="card-body">
                <div class="card-block">
                    <table class="table table-striped" id="table" style="width: 100%">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Concepto</th>
                          <th>Monto</th>
                          <th>Fecha</th>
                          <th>Usuario</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade text-left" id="modal_edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Datos de Gasto</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="form" method="post" role="form" id="form_gasto">
                    <input type="hidden" name="id" id="id" class="form-control" value="0">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Concepto:</label>
                            <div class="col-sm-8 controls">
                                <input type="text" name="concepto" id="concepto" class="form-control" value="">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Monto:</label>
                            <div class="col-sm-8 controls">
                                <input type="number" name="monto" id="monto" class="form-control" value="">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Fecha:</label>
                            <div class="col-sm-8 controls">
                                <input type="date" name="fecha" id="fecha" class="form-control" value="">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="savegasto" class="btn btn-raised gradient-purple-bliss white" data-dismiss="modal">Aceptar</button>
                <button type="button" class="btn btn-raised gradient-ibiza-sunset white" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

