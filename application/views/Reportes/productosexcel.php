<?php

require_once 'PHPExcel/PHPExcel.php';
require_once 'PHPExcel/PHPExcel/IOFactory.php';

$objPHPExcel = new PHPExcel();

$colarray = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
	'AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ',
	'BA','BB','BC','BD','BE','BF','BG','BH','BI','BJ','BK','BL','BM','BN','BO','BP','BQ','BR','BS','BT','BU','BV','BW','BX','BY','BZ',
	'CA','CB','CC','CD','CE','CF','CG','CH','CI','CJ','CK','CL','CM','CN','CO','CP','CQ','CR','CS','CT','CU','CV','CW','CX','CY','CZ'
);

//================ Primera hora =================================
$objPHPExcel->setActiveSheetIndex(0);
$ci0=2;
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'NOMBRE');
$objPHPExcel->getActiveSheet()->setCellValue('B1', 'MARCA');
$objPHPExcel->getActiveSheet()->setCellValue('C1', 'STOCK');
$objPHPExcel->getActiveSheet()->setCellValue('D1', 'STOCK BODEGA');
//$objPHPExcel->getActiveSheet()->setCellValue('E1', 'STOCK A2');
$objPHPExcel->getActiveSheet()->setCellValue('E1', 'PRECIO');

$objPHPExcel->getActiveSheet()->getStyle('A1:E1')->getFill()->applyFromArray(array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'startcolor' => array(
             'rgb' => '626567'
        )
    ));

$objPHPExcel->getActiveSheet()->getStyle('A1:F1')->applyFromArray(array(
        'font'  => array(
				           'bold'  => false,
				           'color' => array('rgb' => 'FDFEFE'),
				           'size'  => 11,
				           'name'  => 'Calibri'
				       	)
    ));

$cr=6;


foreach ($getventasd->result() as $itemp) {

		$objPHPExcel->getActiveSheet()->setCellValue('A'.$ci0, $itemp->categoria);
		$objPHPExcel->getActiveSheet()->setCellValue('B'.$ci0, $itemp->marca);
		$objPHPExcel->getActiveSheet()->setCellValue('C'.$ci0, $itemp->stok);
		$objPHPExcel->getActiveSheet()->setCellValue('D'.$ci0, $itemp->stok2);
		//$objPHPExcel->getActiveSheet()->setCellValue('E'.$ci0, $itemp->stok3);
		$objPHPExcel->getActiveSheet()->setCellValue('E'.$ci0, $itemp->precio_m);
		//$objPHPExcel->getActiveSheet()->setCellValue('E'.$ci0, $itemp->cuenta);
		//$objPHPExcel->getActiveSheet()->setCellValue('F'.$ci0, $itemp->metodopago);
		//========================================================================
		
		//========================================================================
		$ci0++;
	
}

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setAutoSize(true);

// Titulo de la hoja 
$objPHPExcel->getActiveSheet()->setTitle('productos');

// Redirigir la salida al navegador web de un cliente (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="productos.xls"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');