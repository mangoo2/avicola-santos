<div class="row">
    <div class="col-md-12">
      <h2>Categorias </h2>
    </div>
</div>
<!--Statistics cards Ends-->
<!--Line with Area Chart 1 Starts-->
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Categorias</h4>
            </div>
            <div class="card-body">
                <div class="card-block">
                    <!--------//////////////-------->
                    <div class="row">
                        <div class="col-md-6">
                          
                          <div class="col-md-12">
                              <div class="fileinput fileinput-new" data-provides="fileinput" style="width: 100%">
                                <div class="fileinput-preview thumbnail previewcat" data-trigger="fileinput" style="width: 100%; height: 150px; border: solid #a6a9ae 1px; border-radius: 12px;">
                                 <!--img-->
                                </div>
                                <div>
                                    <span class="btn btn-primary btn-file">
                                        <span class="fileinput-new">Foto</span>
                                        <span class="fileinput-exists">Cambiar</span>
                                        <input type="file" name="imgcat" id="imgcat" data-allowed-file-extensions='["jpg", "png"]'>
                                    </span>
                                    <a href="#" class="btn btn-primary fileinput-exists" data-dismiss="fileinput">Quitar</a>
                                </div>
                            </div>
                          </div>
                          <div class="col-md-12">
                            <div class="input-group">
                              <input type="hidden" name="categoriaid" id="categoriaid" value="0">
                              <input type="text" name="categoria" id="categoria" class="form-control">
                              <div class="input-group-append">
                                <span class="input-group-btn" id="button-addon2">
                                  <button class="btn btn-raised gradient-purple-bliss white" style="background: #2e58a6;"id="categoriaadd" type="button">Agregar</button>
                                </span>
                              </div>
                            </div>                            
                          </div>
                          <div class="col-md-12">
                            <table class="table table-striped" id="data-tables">
                              <thead>
                                <tr>
                                  <th></th>
                                  <th>Categoria</th>
                                  <th></th>
                                </tr>
                              </thead>
                              <tbody>

                                <?php foreach ($categoriasll->result() as $item){ ?>
                                 <tr id="trven_<?php echo $item->categoriaId; ?>">
                                          <?php if($item->img==''){
                                            $imgcat='public/img/ops.png';
                                          }else{
                                            $imgcat='public/img/categoriat/'.$item->img;
                                          }?>
                                          <td><img src="<?php echo base_url(); ?><?php echo $imgcat;?>" style="width: 127px;"></td>
                                          <td><?php echo $item->categoria; ?></td>
                                          
                                          <td>
                                            <?php if($item->categoriaId!=1){ ?>
                                            <button class="btn btn-raised gradient-flickr white" style="background: red;"onclick="categodelete(<?php echo $item->categoriaId; ?>)" title="Eliminar" data-toggle="tooltip" data-placement="top">
                                              <i class="fa fa-times"></i>
                                            </button>
                                        <?php } ?>
                                            <button class="btn btn-raised gradient-purple-bliss white" style="background: rgb(255 189 1) !important;background: #2e58a6;" onclick="editarcat(<?php echo $item->categoriaId; ?>,'<?php echo $item->categoria; ?>','<?php echo $imgcat; ?>')" title="Editar" data-toggle="tooltip" data-placement="top">
                                              <i class="fa fa-pencil"></i>
                                            </button>
                                          </td>
                                  </tr>
                                  
                                <?php } ?>
                                    
                              </tbody>
                            </table>
                          </div>
                        </div>
                        <!--------------------------------->
                        <div class="col-md-6">
                          
                          <div class="col-md-12">
                              <div class="fileinput fileinput-new" data-provides="fileinput" style="width: 100%">
                                            <div class="fileinput-preview thumbnail previewmar" data-trigger="fileinput" style="width: 100%; height: 150px; border: solid #a6a9ae 1px; border-radius: 12px;">
                                             <!--img-->
                                            </div>
                                            <div>
                                                <span class="btn btn-primary btn-file">
                                                    <span class="fileinput-new">Foto</span>
                                                    <span class="fileinput-exists">Cambiar</span>
                                                    <input type="file" name="imgmar" id="imgmar" data-allowed-file-extensions='["jpg", "png"]'>
                                                </span>
                                                <a href="#" class="btn btn-primary fileinput-exists" data-dismiss="fileinput">Quitar</a>
                                            </div>
                                        </div>
                          </div>
                          <div class="col-md-12">
                            <div class="input-group">
                              <input type="hidden" name="marcasid" id="marcasid" value="0">
                              <input type="text" name="marcas" id="marcas" class="form-control">
                              <div class="input-group-append">
                                <span class="input-group-btn" id="button-addon2">
                                  <button class="btn btn-raised gradient-purple-bliss white" style="background: #2e58a6;"id="marcasadd" type="button">Agregar</button>
                                </span>
                              </div>
                            </div>                            
                          </div>
                          <div class="col-md-12">
                            <table class="table table-striped" id="data-tables2">
                              <thead>
                                <tr>
                                  <th></th>
                                  <th>Marcas</th>
                                  <th></th>
                                </tr>
                              </thead>
                              <tbody>

                                <?php foreach ($marcasall->result() as $item){ ?>
                                 <tr id="trven_<?php echo $item->marcaid; ?>">
                                          <?php if($item->imgm==''){
                                            $imgmar='public/img/ops.png';
                                          }else{
                                            $imgmar='public/img/marcast/'.$item->imgm;
                                          }?>
                                          <td><img src="<?php echo base_url(); ?><?php echo $imgmar;?>" style="width: 127px;"></td>
                                          <td><?php echo $item->marca; ?></td>
                                          
                                          <td>
                                            
                                            <button class="btn btn-raised gradient-flickr white" style="background: red;" onclick="marcadelete(<?php echo $item->marcaid; ?>)" title="Eliminar" data-toggle="tooltip" data-placement="top">
                                              <i class="fa fa-times"></i>
                                            </button>
                                            <button class="btn btn-raised gradient-purple-bliss white" style="background: rgb(255 189 1) !important;background: #2e58a6;"onclick="editarmar(<?php echo $item->marcaid; ?>,'<?php echo $item->marca; ?>','<?php echo $imgmar; ?>')" title="Editar" data-toggle="tooltip" data-placement="top">
                                              <i class="fa fa-pencil"></i>
                                            </button>
                                          </td>
                                  </tr>
                                  
                                <?php } ?>
                                    
                              </tbody>
                            </table>
                          </div>
                        </div>

                        
                        
                    </div>
                    
                    
                    <!--------//////////////-------->
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function categodelete(id){
      $.ajax({
        type:'POST',
        url:'Categoria/categoriadell',
        data:{id:id},
        async:false,
        success:function(data){
          toastr.success('Eliminado Correctamente','Hecho!');
          $('#trven_'+id).remove();
        }
      });
    
    }
    $(document).ready(function () {
        $('#data-tables').DataTable();
        $('#data-tables2').DataTable();
        $('#categoriaadd').click(function(event) {
            
                $.ajax({
                    type:'POST',
                    url: 'Categoria/categoriaadd',
                    data: {
                        id:$('#categoriaid').val(),
                        nom: $('#categoria').val()
                    },
                    async: false,
                    statusCode:{
                        404: function(data){
                            toastr.error('Error!', 'No Se encuentra el archivo');
                        },
                        500: function(){
                            toastr.error('Error', '500');
                        }
                    },
                    success:function(data){
                      console.log(data);
                        var idcart=data;
                        if ($('#imgcat')[0].files.length > 0) {
                            var inputFileImage = document.getElementById('imgcat');
                            var file = inputFileImage.files[0];
                            var data = new FormData();
                            data.append('img',file);
                            data.append('idcat',idcart);
                            $.ajax({
                                url:'Categoria/imgcat',
                                type:'POST',
                                contentType:false,
                                data:data,
                                processData:false,
                                cache:false,
                                success: function(data) {
                                  var array = $.parseJSON(data);
                                            if (array.ok=true) {
                                              $(".fileinput").fileinput("clear");
                                              toastr.success('Guardado Correctamente','Hecho!');
                                              location.href=''; 
                                              window.location.href = "";
                                            }else{
                                              toastr.error('Error', data.msg);
                                            }
                                },
                                error: function(jqXHR, textStatus, errorThrown) {
                                            var data = JSON.parse(jqXHR.responseText);
                                            console.log(data);
                                            if (data.ok=='true') {
                                              $(".fileinput").fileinput("clear");
                                            }else{
                                              toastr.error('Error', data.msg);
                                            }          
                                }
                            });
                        }else{
                            toastr.success('Guardado Correctamente','Hecho!');
                            location.href=''; 
                            window.location.href = "";
                        }
                    }
                });
        });
        $('#marcasadd').click(function(event) {
            
                $.ajax({
                    type:'POST',
                    url: 'Categoria/marcaadd',
                    data: {
                        id:$('#marcasid').val(),
                        nom: $('#marcas').val()
                    },
                    async: false,
                    statusCode:{
                        404: function(data){
                            toastr.error('Error!', 'No Se encuentra el archivo');
                        },
                        500: function(){
                            toastr.error('Error', '500');
                        }
                    },
                    success:function(data){
                      console.log(data);
                        var idmar=data;
                        if ($('#imgmar')[0].files.length > 0) {
                            var inputFileImage = document.getElementById('imgmar');
                            var file = inputFileImage.files[0];
                            var data = new FormData();
                            data.append('imgmar',file);
                            data.append('idmar',idmar);
                            $.ajax({
                                url:'Categoria/imgmar',
                                type:'POST',
                                contentType:false,
                                data:data,
                                processData:false,
                                cache:false,
                                success: function(data) {
                                  console.log(data);
                                  var array = $.parseJSON(data);
                                            if (array.ok=true) {
                                              $(".fileinput").fileinput("clear");
                                              toastr.success('Guardado Correctamente','Hecho!');
                                              location.href=''; 
                                              window.location.href = "";
                                            }else{
                                              toastr.error('Error', data.msg);
                                            }
                                },
                                error: function(jqXHR, textStatus, errorThrown) {
                                            var data = JSON.parse(jqXHR.responseText);
                                            console.log(data);
                                            if (data.ok=='true') {
                                              $(".fileinput").fileinput("clear");
                                            }else{
                                              toastr.error('Error', data.msg);
                                            }          
                                }
                            });
                        }else{
                            toastr.success('Guardado Correctamente','Hecho!');
                            location.href=''; 
                            window.location.href = "";
                        }                        
                    }
                });
        });
    });
    function editarcat(id,name,img){
      $('.previewcat').html('<img src="<?php echo base_url(); ?>'+img+'">');
      $('#categoriaid').val(id);
      $('#categoria').val(name);
    }
    function editarmar(id,name,img){
      $('.previewmar').html('<img src="<?php echo base_url(); ?>'+img+'">');
      $('#marcasid').val(id);
      $('#marcas').val(name);
    }
    function marcadelete(id){
      $.ajax({
        type:'POST',
        url:'Categoria/marcadell',
        data:{id:id},
        async:false,
        success:function(data){
          toastr.success('Eliminado Correctamente','Hecho!');
          $('#trven_'+id).remove();
        }
      });
    }   
</script>