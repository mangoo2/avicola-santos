<?php 
    $id = 0;
    $Fecha = '';
    $horaa = '';
    $horac = '';
    $cantidad = '';
    $nombre = '';
    $status = '';
    $user = '';
    foreach ($sturno->result() as $row) {
        if ($row->status=='abierto') {
            $id = $row->id;
            $Fecha = $row->fecha;
            $horaa = $row->horaa;
            $horac = $row->horac;
            $cantidad = $row->cantidad;
            $nombre = $row->nombre;
            $status = $row->status;
            $user = $row->user;
        }
        
    }
    $fechahoy =date('Y').'-'.date('m'). '-'.date('d'); 
?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/print.css" media="print">
<input type="hidden" name="idturno" id="idturno" value="<?php echo $id;?>">
<input id="txtFin" name="txtFin"  size="16" type="text" value="<?php echo $fechahoy; ?>" hidden/>
<input id="txtInicio" name="txtInicio" size="16" type="text" value="<?php echo $Fecha; ?>" <?php if($status=='abierto'){  }else{ echo 'readonly';} ?> hidden/>
<div class="row">
    <div class="col-md-10">
      <h2>Turno </h2>
    </div>
    <div class="col-md-2">
        <select class="form-control" id="personalbodega" onchange="seleccionar()">
          <option value="1" <?php if (isset($_GET['bod'])) { if ($_GET['bod']==1) { echo "selected"; } }?>  >Matriz</option>
        </select>
    </div>
</div>
<!--Statistics cards Ends-->
<!--Line with Area Chart 1 Starts-->
<div class="row">
<div class="col-sm-12">
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">turno</h4>
        </div>
        <div class="card-body">
            <div class="card-block">
                <!--------//////////////-------->
                <div class="row">
                    <div class="col-md-12 inputbusquedas">
                        <div class="form-group">
                            <label class="control-label col-md-2">Abrir Turno</label>
                            <div class="col-md-2">
                                <input type="number" min="0"class="form-control"id="cantidadt" name="cantidadt" value="<?php echo $cantidad; ?>" <?php if($status=='abierto'){ echo 'disabled="true"'; } ?> >
                            </div>
                            <label class="control-label col-md-2">Nombre del turno</label>
                            <div class="col-md-2">
                                <input type="text" class="form-control" id="nombredelturno" name="nombredelturno" value="<?php echo $nombre; ?>" <?php if($status=='abierto'){ echo 'disabled="true"'; } ?>>
                            </div>
                            <div class="col-md-4">
                                <button type="button" class="btn btn-raised gradient-purple-bliss white" style="background: rgb(255 189 1) !important; background: #2e58a6;" id="btnabrirt" <?php if($status=='abierto'){ echo 'disabled="true"'; } ?> >Abrir Turno</button>
                                <button type="button" class="btn btn-raised gradient-purple-bliss white" style="background: rgb(255 189 1) !important; background: #2e58a6;" id="btncerrar" <?php if($status=='abierto'){  }else{ echo 'disabled="true"';} ?> >Inspeccionar</button>
                                <button type="button" class="btn btn-raised gradient-purple-bliss white" style="background: rgb(255 189 1) !important; background: #2e58a6;" id="cerrarturno" <?php if($status=='abierto'){  }else{ echo 'disabled="true"';} ?> >Cerrar Turno</button>
                                <a id="btnImprimir" onclick="imprimir();"><button type="button" class="btn btn-raised gradient-purple-bliss white"  style="background: rgb(255 189 1) !important;background: #2e58a6;" ><i class="fa fa-print"></i></button></a>
                            </div>  
                        </div>
                    </div>
                    <div class="col-md-12">
                        <br><br>
                    </div>
                    <div class="col-md-12 inputbusquedas">
                        <div class="form-group">
                            <label class="control-label col-md-2">Hora de inicio</label>
                            <div class="col-md-2"><input type="text" class="form-control"id="horainicial" name="horainicial" value="<?php echo $horaa; ?>" readonly></div>
                        </div>
                    </div>
                    <div class="col-md-12" id="imprimir">
                        <div class="col-md-12">
                            <br><br>

                            <div class="col-md-6">
                                <!--<p style="font-size: 20px">
                                    <span class="col-md-6 text-warning">SUBTOTAL:</span>
                                    <span class="col-md-4" >
                                        <span class="text-warning">$</span>
                                        <span id="dSubtotal">0</span>
                                    </span>
                                </p>
                                <p style="font-size: 20px">
                                    <span class="col-md-6 text-warning">DESCUENTO:</span>
                                    <span class="col-md-4" >
                                        <span class="text-warning">$</span>
                                        <span id="ddescuento">0</span>
                                    </span>
                                </p>-->
                                <p style="font-size: 20px">
                                    <span class="col-md-6 text-warning">TOTAL:</span>
                                    <span class="col-md-4" >
                                        <span class="text-warning">$</span>
                                        <span id="dTotal3">0.00</span>
                                    </span>
                                </p>
                                <p style="font-size: 20px">
                                    <span class="col-md-6 text-warning">ABONOS:</span>
                                    <span class="col-md-4" >
                                        <span class="text-warning">$</span>
                                        <span id="abonos">0.00</span>
                                    </span>
                                </p>
                                <p style="font-size: 20px">
                                    <span class="col-md-6 text-warning">GASTOS:</span>
                                    <span class="col-md-4" >
                                        <span class="text-warning">$</span>
                                        <span id="gastos">0.00</span>
                                    </span>
                                </p>
                                <p style="font-size: 20px">
                                    <span class="col-md-6 text-warning">EN CAJA:</span>
                                    <span class="col-md-4" >
                                        <span class="text-warning">$</span>
                                        <span id="val2">0.00</span>
                                    </span>
                                </p>
                                <p style="font-size: 20px">
                                    <span class="col-md-6 text-warning">TOTAL EN CAJA:</span>
                                    <span class="col-md-4" >
                                        <span class="text-warning">$</span>
                                        <span id="stotal2">0.00</span>
                                    </span>
                                </p>



                                <!--
                                <label style="font-size: 20px" class="col-md-6 text-warning">Compras:</label> 
                                <label style="font-size: 20px" class="col-md-1 text-warning">$</label>
                                <label style="font-size: 20px" class="col-md-4" id="caja"><label id="comps">0.00</label> </label><br>-->
                                <script src="http://code.jquery.com/jquery-latest.js"></script>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <br><br>
                        </div>
                        <div class="col-md-12" id="tbCorte">
                          
                        </div>
                        
                    </div>
                    
                    
                </div>
               
                
                <!--------//////////////-------->
            </div>
        </div>
    </div>
</div>
</div>