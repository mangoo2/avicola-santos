<style type="text/css">
  #personalbodega{
    display: none;
  }
</style>
<div class="row">
                <div class="col-md-10">
                  <h2>Lista Turnos</h2>
                  
                </div>
                <div class="col-md-2">
                  <select class="form-control" id="personalbodega" onchange="seleccionar()">
                    <option value="1" <?php if (isset($_GET['bod'])) { if ($_GET['bod']==1) { echo "selected"; } }?>  >Matriz</option>
                    <option value="2" <?php if (isset($_GET['bod'])) { if ($_GET['bod']==2) { echo "selected"; } }?> >Sucursal 1</option>
                    <option value="3" <?php if (isset($_GET['bod'])) { if ($_GET['bod']==3) { echo "selected"; } }?> >Sucursal 2</option>
                  </select>
              </div>
              </div>
              <!--Statistics cards Ends-->

              <!--Line with Area Chart 1 Starts-->
              <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Listado de Turnos</h4>
                        </div>
                        <div class="card-body">
                            <div class="card-block">
                                <!--------//////////////-------->
                                <table class="table table-striped" id="data-tables">
                                      <thead>
                                        <tr>
                                          <th>Fecha</th>
                                          <th>Hora inicio</th>
                                          <th>Hora Cierre</th>
                                          <th>Nombre</th>
                                          <th>Estatus</th>
                                          <th></th>
                                        </tr>
                                      </thead>
                                      <tbody>

                                        <?php foreach ($lturnos->result() as $item){ ?>
                                         <tr id="trven_<?php echo $item->id; ?>">
                                                  <td><?php echo $item->fecha; ?></td>
                                                  <td><?php echo $item->horaa; ?></td>
                                                  <td><?php echo $item->horac; ?></td>
                                                  <td><?php echo $item->nombre; ?></td>
                                                  <td><?php echo $item->status; ?></td>
                                                  <td>
                                                    <button class="btn btn-raised gradient-blackberry white" onclick="consultarturn(<?php echo $item->id; ?>)">
                                                      <i class="fa fa-book"></i>
                                                    </button>
                                                  </td>
                                          </tr>
                                          
                                        <?php } ?>
                                            
                                      </tbody>
                                    </table>
                                    <div class="col-md-12">
                                      <div class="col-md-9">
                                        
                                      </div>
                                      <div class="col-md-3">
                                        <?php echo $this->pagination->create_links() ?>
                                      </div>
                                      
                                    </div>
                                    
                                    
                        <!--------//////////////-------->
                            </div>
                        </div>
                    </div>
                </div>
              </div>
<!------------------------------------------------>
<script type="text/javascript">
  function consultarturn(id){
    $("#iframeri").modal();
    $.ajax({
      type:'POST',
      url:'<?php echo base_url(); ?>Ventas/consultarturno',
      data:{id:id,bod:$('#personalbodega option:selected').val()},
      async:false,
      success:function(data){
        //var array = $.parseJSON(data);
        $('#tbCorte').html(data);
        
      }
    });
    
  }
  function seleccionar(){
      var bode=$('#personalbodega option:selected').val();
      location.href="<?php echo base_url(); ?>ListaTurnos?bod="+bode;
  }
</script>

<div class="modal fade text-left" id="iframeri" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Ventas del Turno</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Productos vendidos en el turno</p>
                <div class="row" align="center" >
                  <div class="col-md-12" id="tbCorte">
                  </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>