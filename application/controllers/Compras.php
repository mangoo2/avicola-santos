<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Compras extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloProveedor');
        $this->load->model('ModeloProductos');
        $this->load->model('ModeloVentas');
        $this->load->model('ModeloCatalogos');
    }
	public function index(){
		$this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('compras/compras');
        $this->load->view('templates/footer');
        $this->load->view('compras/jscompras');
	}
    public function searchpro(){
        $usu = $this->input->get('search');
        $results=$this->ModeloProveedor->proveedorallsearch($usu);
        echo json_encode($results->result());
    }
    
    
    public function addproducto(){
        $cant = $this->input->post('cant');
        $prod = $this->input->post('prod');
        $prec = $this->input->post('prec');
        $prodt = $this->input->post('prodt');
        //$personalv=$this->ModeloProductos->getproducto($prod);
        //foreach ($personalv->result() as $item){
        //      $id = $item->productoid;
        //      $precio = $item->preciocompra;
        //}
        $array = array("id"=>$prod,
                        "cant"=>$cant,
                        "producto"=>$prodt,
                        "precio"=>$prec
                    );
            echo json_encode($array);
    }
    function ingresarcompra(){
        $uss = $this->input->post('uss');
        $prov = $this->input->post('prov');
        $total = $this->input->post('total');
        $id=$this->ModeloVentas->ingresarcompra($uss,$prov,$total);
        echo $id;
    }
    function ingresarcomprapro(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        $sub=0; $sumac=0; $precioc=0; $prom_ant=0;
        for ($i=0;$i<count($DATA);$i++) { 
            $idcompra = $DATA[$i]->idcompra;
            $producto = $DATA[$i]->producto;
            $cant_ant = $DATA[$i]->cant_ant;
            $cantidad = $DATA[$i]->cantidad;
            $kilos = $DATA[$i]->kilos;
            $precio = $DATA[$i]->precio;
            
            $ultimo_p=$this->getUltimoCosto($producto);
            //log_message('error', ' ultimo_p de compra: '.$ultimo_p);
            //log_message('error', ' cant_ant : '.$cant_ant);
            $prom_ant= ($ultimo_p*$cant_ant);

            /*log_message('error', ' precio : '.$precio);
            log_message('error', ' cantidad : '.$cantidad);
            
            log_message('error', ' ultimo_p : '.$ultimo_p);
            log_message('error', ' cant_ant : '.$cant_ant);
            log_message('error', ' prom_ant : '.$prom_ant);*/
            if($kilos==0){
                $prom_act= ($precio*$cantidad);
            }else{
                //$prom_act= ($precio*$kilos);
                $prom_act= ($precio*$cantidad);
            }
            //log_message('error', ' prom_act : '.$prom_act);
            $sumaproms=$prom_ant+$prom_act;
            $sumastocks=$cant_ant+$cantidad;
            //log_message('error', ' sumaproms : '.$sumaproms);
            $precioc_act=round($sumaproms/$sumastocks,2);   
            //log_message('error', ' precioc_act : '.$precioc_act);
            $this->ModeloVentas->updatePrecioCompra($precioc_act,$producto);
            //$this->ModeloVentas->updatePrecioCompra2($precioc_act,$producto);

            $this->ModeloVentas->ingresarcomprad($idcompra,$producto,$cantidad,$kilos,$precio); //guarda el la compra*/
        }
    }

    function getUltimoCosto($id){
        $precioc=0; $sub=0; $cont=0; $sumac=0;
        //$getid=$this->ModeloCatalogos->getselectwhere_n_consulta("sproductosub",array("productoaddId"=>$id));
        $getid=$this->ModeloCatalogos->getselectwhere_n_consulta("sproducto",array("productoaddId"=>$id));
        if(isset($getid)){
            /*$get=$this->ModeloVentas->getLast(array("id_producto"=>$getid[0]->subId));
            if($get->num_rows()>0) {
                $cont++;
                $get=$get->row();
                $precioc=$get->precio_compra;
            } */
            $cont++;
            $precioc=$getid[0]->precompra;
            if($cont==0){
               //$getcc=$this->ModeloCatalogos->getselectwhere_n_consulta("sproductosub",array("productoaddId"=>$id));
               $getid=$this->ModeloCatalogos->getselectwhere_n_consulta("sproducto",array("productoaddId"=>$id));

               $precioc=$getcc[0]->precompra;
            }
        }
        return round($precioc,2);
    }

    function getPromedio(){
        $id = $this->input->post('id');
        $precioc=0; $sub=0; $cont=0; $sumac=0;
        $getid=$this->ModeloCatalogos->getselectwhere_n_consulta("sproductosub",array("productoaddId"=>$id));
        if(isset($getid[0]->subId)){
            /*$get=$this->ModeloCatalogos->getselectwhere_n_consulta("compra_detalle",array("id_producto"=>$getid[0]->subId));
            foreach ($get as $p) {
                $cont++;
                if($p->kilos==0){
                    $sub=$sub+($p->cantidad*$p->precio_compra);
                    $sumac=$sumac+$p->cantidad;
                }else{
                    $sub=$sub+($p->kilos*$p->precio_compra);
                    $sumac=$sumac+$p->kilos;
                }
            }
            
            if($cont==0){
               $getcc=$this->ModeloCatalogos->getselectwhere_n_consulta("sproducto",array("productoaddId"=>$id));
               $precioc=$getcc[0]->precompra;
            }else{
               $precioc=$sub/$sumac; 
            }*/
            $getcc=$this->ModeloCatalogos->getselectwhere_n_consulta("sproducto",array("productoaddId"=>$id));
            $precioc=$getcc[0]->precompra;
        }
        echo round($precioc,2);
    }

}