<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mermas extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloProductos');
        date_default_timezone_set('America/Mexico_City');
        $this->fecha = date('Y-m-d G:i:s');
        $this->usuarioid_tz=$this->session->userdata("usuarioid_tz");
    }
	public function index(){
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('mermas/mermas');
        $this->load->view('templates/footer');
        $this->load->view('mermas/jsmermas');
	}

    public function get_data_list() {
        $params = $this->input->post();
        $getdata = $this->ModeloProductos->getlistMermas($params);
        $totaldata= $this->ModeloProductos->getTotalMermas($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
   
    
}
