<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->model('ModeloSession');
        $this->load->helper('url');
    }
	public function index()
	{
        $this->load->view('login2/header');
        $this->load->view('login2/index');
       
        $this->load->view('login2/footer');
        $this->load->view('login2/pages-logintpl');
            
	}   
    public function session(){
        $usu = $this->input->post('usu');
        $pass = $this->input->post('passw');
        $respuesta = $this->ModeloSession->login($usu,$pass);
        echo $respuesta;
    }   
    public function exitlogin() {
        $this->load->view('login2/exit');
    }
}
