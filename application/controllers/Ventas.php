<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ventas extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloClientes');
        $this->load->model('ModeloVentas');
        $this->load->model('ModeloProductos');
        if (isset($_SESSION['bodega_tz'])) {
            $this->bodega=$_SESSION['bodega_tz'];
        }else{
            $this->bodega=0;
        }
    }
    public function index(){
        $data['bodegauser']=$this->bodega;
        $data['sturno']=$this->ModeloVentas->turnosactivo($this->bodega);
        $data['clientedefault']=$this->ModeloVentas->clientepordefecto();
        $data['categoriasll']=$this->ModeloCatalogos->categorias_all();
        $data['metodopagos']=$this->ModeloCatalogos->metodopagos();
        //$data['marcasll']=$this->ModeloCatalogos->marcashuevo_all();
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        //$this->load->view('ventas/ventas',$data);
        $this->load->view('ventas/ventas2',$data);
        $this->load->view('templates/footer');
        $this->load->view('ventas/jsventas');
        unset($_SESSION['pro']);
        $_SESSION['pro']=array();
        unset($_SESSION['can']);
        $_SESSION['can']=array();
        unset($_SESSION['kilos']);
        $_SESSION['kilos']=array();
        unset($_SESSION['precio']);
        $_SESSION['precio']=array();
    }
    public function searchcli(){
        $usu = $this->input->get('search');
        $results=$this->ModeloClientes->clientesallsearch($usu);
        echo json_encode($results->result());
    }
    public function searchproducto(){
        $pro = $this->input->get('search');
        $results=$this->ModeloProductos->productoallsearch($pro);
        echo json_encode($results->result());
    }
    public function addproducto(){
        $cant = $this->input->post('cant');
        $kilos=$this->input->post('kilos');
        $prod = $this->input->post('proc');
        $producton=$this->input->post('producton');
        $marcan=$this->input->post('marcan');
        $categorian=$this->input->post('categorian');
        $productoss=$this->ModeloProductos->getsproductosub($prod);
        //$oProducto = new Producto();
        foreach ($productoss->result() as $item){
              $id = $item->subId;
              if ($this->bodega==2) {
                    $precio = $item->precio2;
                    $precio_mm = $item->precio_mm2;
                    $cantidad_mm = $item->cantidad_mm2;
                    $precio_m = $item->precio_m2;
                    $cantidad_m = $item->cantidad_m2;
              }elseif ($this->bodega==3) {
                    $precio = $item->precio3;
                    $precio_mm = $item->precio_mm3;
                    $cantidad_mm = $item->cantidad_mm3;
                    $precio_m = $item->precio_m3;
                    $cantidad_m = $item->cantidad_m3;
              }else{
                    $precio = $item->precio;
                    $precio_mm = $item->precio_mm;
                    $cantidad_mm = $item->cantidad_mm;
                    $precio_m = $item->precio_m;
                    $cantidad_m = $item->cantidad_m;
              }
              $oProducto=array("id"=>$id,"producton"=>$producton,"marcan"=>$marcan,"categorian"=>$categorian,'precio'=>$precio,'mediomayoreo'=>$precio_mm,'canmediomayoreo'=>$cantidad_mm,'mayoreo'=>$precio_m,'canmayoreo'=>$cantidad_m);

        }
        if(in_array($oProducto, $_SESSION['pro'])){  // si el producto ya se encuentra en la lista de compra suma las cantidades
            $idx = array_search($oProducto, $_SESSION['pro']);
            $_SESSION['can'][$idx]+=$cant;
            $_SESSION['kilos'][$idx]+=$kilos;
        }
        else{ //sino lo agrega
            array_push($_SESSION['pro'],$oProducto);
            array_push($_SESSION['can'],$cant);
            array_push($_SESSION['kilos'],$kilos);
        }
        //======================================================================
        $count = 0;
        $n =array_sum($_SESSION['can']);
        foreach ($_SESSION['pro'] as $fila){
            $Cantidad=$_SESSION['can'][$count];
            $kiloss=$_SESSION['kilos'][$count]; 
            $id=$fila['id'];
            if ($Cantidad>=$fila['canmayoreo']) {
                $precio=$fila['mayoreo'];
            }elseif ($Cantidad>=$fila['canmediomayoreo'] && $Cantidad<$fila['canmayoreo']) {
                $precio=$fila['mediomayoreo'];
            }else{
                $precio=$fila['precio'];
            }
            if ($_SESSION['precio'][$id]>0) {
                $precio = $_SESSION['precio'][$id];
            }

            if ($kiloss>0) {
                $cantotal=$kiloss*$precio;
            }else{
                $cantotal=$Cantidad*$precio;
            }
            
            
            ?>
            <tr class="producto_<?php echo $count;?>" style="font-size: 14px;">                                        
                <td>
                    <input type="hidden" name="vsproid" id="vsproid" value="<?php echo $fila['id'];?>">
                    <?php echo $fila['producton'];?>
                </td>
                <td>
                    <?php echo $fila['marcan'];?>
                </td>
                <td>
                    <?php echo $fila['categorian'];?>
                </td>                                        
                <td>
                    <input type="number" class="vscanti_<?php echo $fila['id'];?>" name="vscanti" id="vscanti" value="<?php echo $Cantidad;?>" readonly style="background: transparent;border: 0px; width: 60px;">
                </td>
                <td>
                    <div class="input-group">
                        <input type="number" name="vskilos" id="vskilos" value="<?php echo $kiloss;?>" readonly style="background: transparent;border: 0px; width: 45px; <?php   if ($kiloss==0) { echo' color:transparent;'; };?>"><?php   if ($kiloss>0) { echo' Kilos'; };?>
                    </div>
                    
                    
                </td>                                        
                <td>$ <input type="text" name="vsprecio" id="vsprecio" class="vsprecio vsprecio_<?php echo $fila['id'];?>" value="<?php echo $precio;?>" readonly style="background: transparent;border: 0px;width: 60px;" oninput="recalcula(<?php echo $fila['id'];?>)"></td>                                        
                <td>$ <input type="text" class="vstotal" name="vstotal" id="vstotal" value="<?php echo $cantotal;?>" readonly style="background: transparent;border: 0px; width: 61px;"></td>                                        
                <td>                                            
                    <a class="danger" data-original-title="Eliminar" title="Eliminar" onclick="deletepro(<?php echo $count;?>)">
                        <i class="ft-trash font-medium-3"></i>
                    </a>
                </td>
            </tr>
        <?php
        $count++;
        }
    }

    public function precio_prod(){
        $id = $this->input->post('id');
        $precio = $this->input->post('precio');
        $_SESSION['precio'][$id]=$precio;
    }

    function deleteproducto(){
        $idd = $this->input->post('idd');
        unset($_SESSION['pro'][$idd]);
        $_SESSION['pro']=array_values($_SESSION['pro']);
        unset($_SESSION['can'][$idd]);
        $_SESSION['can'] = array_values($_SESSION['can']); 
        unset($_SESSION['kilos'][$idd]);
        $_SESSION['kilos'] = array_values($_SESSION['kilos']);
        unset($_SESSION['precio'][$idd]);
        $_SESSION['precio'] = array_values($_SESSION['precio']); 
    }
    public function productoclear(){
        unset($_SESSION['pro']);
        $_SESSION['pro']=array();
        unset($_SESSION['can']);
        $_SESSION['can']=array();
        unset($_SESSION['kilos']);
        $_SESSION['kilos']=array();
        unset($_SESSION['precio']);
        $_SESSION['precio']=array();
        echo "limpiado";
    }
    function searchcliente(){
        $search = $this->input->post('search');
        $clientes=$this->ModeloCatalogos->searchcliente($search);
        foreach ($clientes->result() as $item){ ?>
            <li class="list-group-item" data-dismiss="modal" onclick="addcliente(<?php echo $item->ClientesId;?>,'<?php echo $item->Nom;?>')"><?php echo $item->Nom;?></li>
        <?php }
    }   
    function ingresarventa(){
        $uss = $this->input->post('uss');
        $cli = $this->input->post('cli');
        $total = $this->input->post('total');
        $fac = $this->input->post('fac');
        $des = $this->input->post('des');
        $metodo = $this->input->post('metodo');
        /// Diferenciar cuando es domingo 
        /// alias = 1: Dia domingo
        /// alias = 0: Es de Lunes a sabado 
        $folion=1; 
        $fecha = date("l");
        if($fecha == "Sunday"){ // 
            $alias=1;
            $idventa_alias=$this->ModeloCatalogos->get_ventas_alias(1);
            $folion=$idventa_alias+1;    
        }else{
            $alias=0;
            $idventa_alias=$this->ModeloCatalogos->get_ventas_alias(0);
            $folion=$idventa_alias+1;    
        } 
        $id=$this->ModeloVentas->ingresarventa($uss,$cli,$total,$fac,$des,$metodo,$alias,$folion);
        echo $id;
    }
    function ingresarventapro(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        for ($i=0;$i<count($DATA);$i++) { 
            $idventa = $DATA[$i]->idventa;
            $producto = $DATA[$i]->producto;
            $cantidad = $DATA[$i]->cantidad;
            $kilos = $DATA[$i]->kilos;
            $precio = $DATA[$i]->precio;
            $this->ModeloVentas->ingresarventad($idventa,$producto,$cantidad,$kilos,$precio);
            //$this->ModeloVentas->ingresarventad2($idventa,$producto,$cantidad,$kilos,$precio);
        }
    }
    function abrirturno(){
        $cantidad = $this->input->post('cantidad');
        $nombre = $this->input->post('nombre');
        $bodega = $this->input->post('bodega');
        $fecha =date('Y').'-'.date('m'). '-'.date('d'); 
        $horaa =date('H:i:s');
        $this->ModeloVentas->abrirturno($cantidad,$nombre,$fecha,$horaa,$bodega);
    }
    function cerrarturno(){
            $id = $this->input->post('id');
            $fecha =date('Y').'-'.date('m'). '-'.date('d'); 
            $horaa =date('H:i:s');
            $this->ModeloVentas->cerrarturno($id,$horaa);
    }   
    function consultarturno(){
        $id = $this->input->post('id');
        $bodega = $this->input->post('bod');
        $vturno=$this->ModeloVentas->consultarturno($id);
        foreach ($vturno->result() as $item){
            $fecha= $item->fecha;
            $fechac= $item->fechacierre;
            $horaa= $item->horaa;
            $horac= $item->horac;
        }
        if ($horac=='00:00:00') {
            $horac =date('H:i:s');
            $fechac =date('Y-m-d');
        }
        $totalventas=$this->ModeloVentas->consultartotalturno($fecha,$horaa,$horac,$fechac,$bodega);
        echo "<p><b>Total: ".number_format($totalventas,2,'.',',')."</b></p>";
        //$totalpreciocompra=$this->ModeloVentas->consultartotalturno2($fecha,$horaa,$horac,$fechac,$bodega);
        //$obed =$totalventas-$totalpreciocompra;
        //echo "<p><b>Utilidad: ".number_format($obed,2,'.',',')."</b></p>";
        $productos =$this->ModeloVentas->consultartotalturnopro($fecha,$horaa,$horac,$fechac,$bodega);
        echo "<div class='panel-body table-responsive' id='table'>
                            <table class='table table-striped table-bordered table-hover' id='data-tables2'>
                            <thead class='vd_bg-green vd_white'>
                                <tr>
                                    <th>Producto</th>
                                    <th>Cantidad</th>
                                    <th></th>
                                    <th>Precio</th>
                                    
                                </tr>
                            </thead>
                            <tbody>";
        foreach ($productos->result() as $item){
            if ($item->kilos>0) {
                $kilosl='Kilos: '.$item->kilos;
            }else{
                $kilosl='';
            }
            echo "<tr><td>".$item->producto."</td><td>".$item->cantidad."</td><td>".$kilosl."</td><td> $".$item->precio."</td></tr>";
        }
        echo "</tbody> </table></div>";
        echo "<div style='    position: absolute; float: left; top: 0; left: 800px; background: white; width: 250px; z-index: 20;'>
                            <table class='table table-striped table-bordered table-hover' id='data-tables'>
                            <thead>
                                <tr>
                                    <td colspan='4' style='text-align: center;'>Productos mas vendido</td>
                                </tr>
                            </thead>
                            <tbody>";
        $productosmas =$this->ModeloVentas->consultartotalturnopromas($fecha,$horaa,$horac,$fechac,$bodega);
        foreach ($productosmas->result() as $item){
            echo "<tr><td colspan='2' style='text-align: center;'>".$item->total." </td><td colspan='2' style='text-align: center;'>".$item->producto." </td></tr>";
        }
        echo "</tbody> </table></div>";
    }
    function cargarmarcas(){
        $id=$this->input->post('id');
        $marcasll=$this->ModeloCatalogos->marcashuevo_all($id);
        foreach ($marcasll->result() as $item){
            if($item->imgm==''){
                $imgmar=base_url().'public/img/ops.png';
                $porcentajeimg='75%';
            }else{
                $imgmar=base_url().'public/img/marcas/'.$item->imgm;
                $porcentajeimg='135%';
            }
            ?>
                <div class="col-lg-3 col-md-3 col-sm-12 mb-2 classproductosub" onclick="marcasselec(<?php echo $item->marcaid; ?>,'<?php echo $item->marca; ?>','<?php echo $imgmar; ?>')">
                    <div class="card card-inverse bg-info text-center" style="background-color: rgb(255 189 1) !important; height: 300.906px;">
                        <div class="card-body">
                            <div class="card-img overlap" style="background: url('<?php echo $imgmar; ?>');height: 220px;background-size: <?php echo $porcentajeimg;?>;background-position: center; background-repeat: no-repeat; ">
                                <!--<img src="<?php echo $imgmar; ?>" alt="element 06" width="225" class="mb-1">-->
                            </div>
                            <div class="card-block">
                                <h4 class="card-title"><?php echo $item->marca; ?></h4>
                                <!--<p class="card-text">Donut toffee candy brownie soufflé macaroon.</p>-->
                            </div>
                        </div>
                    </div>
                </div>

            <?php
        }
    }
    function cargarpresentacion(){
        $pro=$this->input->post('pro');
        $mar=$this->input->post('mar');
        $presentacion_all=$this->ModeloCatalogos->presentacion_all($pro,$mar);
        
        foreach ($presentacion_all->result() as $item) {
            
            if ($this->bodega==2) {
                $precio=$item->precio2;
            }elseif ($this->bodega==3) {
                $precio=$item->precio3;
            }else{
                $precio=$item->precio;
            }
            //log_message('error', 'bodegastock: '.$this->bodega);
            switch ($this->bodega) {
                case 3:
                    $stock=$item->stok3;
                    break;
                case 2:
                    $stock=$item->stok2;
                    break;
                default:
                    $stock=$item->stok;
                    break;
            }

            
            ?>
                <div class="col-lg-3 col-md-3 col-sm-12 mb-2 classproductotipo" 
                onclick="presentselec(<?php echo $item->subId; ?>,'<?php echo $item->presentacion; ?>',<?php echo $precio; ?>,<?php echo $stock; ?>)">
                            <div class="card card-inverse bg-info text-center" style="background-color: rgb(255 189 1) !important; height: 300.906px;">
                                <div class="card-body">
                                    
                                    <div class="card-block">
                                        <h4 class="card-title"><?php echo $item->presentacion; ?></h4>
                                        <!--<p class="card-text">Donut toffee candy brownie soufflé macaroon.</p>-->
                                    </div>
                                </div>
                            </div>
                        </div>
            <?php
        }
    }
    function productoverificarstok(){
        $proc=$this->input->post('proc');
        $cant=$this->input->post('cant');
        $producto=$this->ModeloProductos->getsproductosub($proc);
        foreach ($producto->result() as $item) {
            //log_message('error', 'bodegastock: '.$this->bodega);
            $stock=$item->stok;
            /*switch ($this->bodega) {
                case 3:
                    $stock=$item->stok3;
                    break;
                case 2:
                    $stock=$item->stok2;
                    break;
                default:
                    $stock=$item->stok;
                    break;
            }*/
        }
        if ($stock>=$cant) {
            $cantn=$cant;
            $cantstatus=1;
        }else{
            $cantn=$stock;
            $cantstatus=0;
        }
        $array = array("canti"=>$cantn,"estatus"=>$cantstatus);
            echo json_encode($array);
    }
    function datosfactura(){
        $idventa=$this->input->post('idventa');
        $datos=$this->input->post('datos');
        echo $this->ModeloVentas->datosfactura($idventa,$datos);
    }

    public function conaulta_update_ventas_1()
    {   
        $resul=$this->ModeloCatalogos->getselect('ventas');
        foreach ($resul as $x){
            $arrayupdate = array('idventa_alias' => $x->id_venta);
            $arraywhere = array('id_venta' => $x->id_venta);
            $this->ModeloCatalogos->updateCatalogo('ventas',$arrayupdate,$arraywhere);
        }
    }

    public function actualizamontoventa()
    {   
        $arr =array(); $arr2=array(); $calc_total=0; $idant=0;
        $getventasd=$this->ModeloVentas->getAllventasd();
        foreach ($getventasd->result() as $rowEmp){
            
            if ($rowEmp->kilos>0) {
                $rowimporte=$rowEmp->kilos*$rowEmp->precio;
            }else{
                $rowimporte=$rowEmp->cantidad*$rowEmp->precio;
            }
            //$calc_total=$calc_total+$rowimporte;
            if($idant==$rowEmp->id_venta){
                $calc_total=$calc_total+$rowimporte;
            }else{
                $calc_total=$rowimporte;
            }

            echo "id_venta: ".$rowEmp->id_venta;
            echo "calc_total: ".$calc_total."<br>";

            $this->ModeloCatalogos->updateCatalogo('ventas',array("monto_total"=>$calc_total),array("id_venta"=>$rowEmp->id_venta));
            $idant=$rowEmp->id_venta;
        }

    }

}