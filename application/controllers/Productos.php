<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Productos extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloProductos');
        $this->load->model('ModeloCatalogos');
        date_default_timezone_set('America/Mexico_City');
        $this->fecha = date('Y-m-d G:i:s');
        $this->usuarioid_tz=$this->session->userdata("usuarioid_tz");
    }
	public function index(){
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        //$this->load->view('Personal/Personal',$data);
        $this->load->view('productos/productos');
        $this->load->view('templates/footer');
        $this->load->view('productos/jsproducto');
	}
    public function productosadd(){
        $data["categorias"] = $this->ModeloProductos->categorias();
        $data["marcaall"] = $this->ModeloCatalogos->marcas_all();
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('productos/productoadd',$data);
        $this->load->view('templates/footer');
        $this->load->view('productos/jsproducto');
    }
    function productoadd(){
        $id = $this->input->post('id');
        $prod = $this->input->post('prod');
        $marca = $this->input->post('marca');
        $compra = $this->input->post('compra');
    
        if ($id>0) {
            $this->ModeloProductos->productosupdate($id,$prod,$marca,$compra);
            echo $id;
        }else{
            $idd=$this->ModeloProductos->productosinsert($prod,$marca,$compra);
            echo $idd;
        }
    }
    function productoaddetalle(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        for ($i=0;$i<count($DATA);$i++) { 
            $idpro = $DATA[$i]->idpro;
            $idp = $DATA[$i]->ppresentacionid;
            $presentacion = $DATA[$i]->presentacion;
            $tipo = $DATA[$i]->tipo;

            $cantidad = $DATA[$i]->cantidad;
            $precio = $DATA[$i]->precio;
            $apartirmm = $DATA[$i]->apartirmm;
            $prepreciomm = $DATA[$i]->prepreciomm;
            $apartirm = $DATA[$i]->apartirm;
            $prepreciom = $DATA[$i]->prepreciom;  
            $cantidad2 = $DATA[$i]->cantidad2;
            $cantidad3 = $DATA[$i]->cantidad3;          
            if ($idp==0) {
               echo $this->ModeloProductos->productoaddetalleinser($idpro,$presentacion,$cantidad,$precio,$apartirmm,$prepreciomm,$apartirm,$prepreciom,$tipo,$cantidad2,$cantidad3);
            }else{
                $this->ModeloProductos->productoaddetalleupdate($idp,$idpro,$presentacion,$cantidad,$precio,$apartirmm,$prepreciomm,$apartirm,$prepreciom,$tipo,$cantidad2,$cantidad3);
            }
        }
    }
    public function deleteproductos(){
        $id = $this->input->post('id');
        $this->ModeloProductos->productosdelete($id); 
    }
    function deleterow(){
        $id = $this->input->post('id');
        $row=$this->ModeloProductos->deleterowv($id);
        if ($row==0) {
            $this->ModeloProductos->deleterow($id);
        }
        echo $row;
    }
    
    public function searchpres(){
        $usu = $this->input->get('search');
        $results=$this->ModeloProductos->presentallsearch($usu);
        echo json_encode($results->result());
    }
    public function mermaadd(){
        //$data = $this->input->post();
        $id = $this->input->post('id');
        $marca = $this->input->post('marca'); //tipo merma
        $press = $this->input->post('press'); //presentacion
        $cant = $this->input->post('cant');
        $bodega = $this->input->post('bodega');
        $motivo = $this->input->post('motivo');

        //unset($data["marca"]);
        $data["id_producto"]=$id;
        $data["tipo_merma"]=$marca;
        $data["presentacion"]=$press;
        $data["bodega"]=$bodega;
        $data["cantidad"]=$cant;
        $data["motivo"]=$motivo;
        $data["fecha_reg"]=$this->fecha;
        $data["id_usuario"]=$this->usuarioid_tz;
        $this->ModeloProductos->tabla_inserta("mermas",$data);
        echo $this->ModeloProductos->addmerma($id,$marca,$press,$cant,$bodega);
    }
    public function traspasos(){
        $id = $this->input->post('id');
        $origen = $this->input->post('origen');
        $destino = $this->input->post('destino');
        $cant = $this->input->post('cant');
        $this->ModeloProductos->traspasos3($id,$origen,$cant);//disminulle
        $this->ModeloProductos->traspasos2($id,$destino,$cant);//aumenta
    }
    public function getlistproductos() {
        $params = $this->input->post();
        $getdata = $this->ModeloProductos->getlistprodutos($params);
        $totaldata= $this->ModeloProductos->getlistprodutost($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
   
    
}
