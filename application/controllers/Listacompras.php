<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Listacompras extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloVentas');
        $this->load->model('ModeloCompras');
        $this->load->model('ModeloCatalogos');
    }
	public function index(){
            /*
            $pages=15; //Número de registros mostrados por páginas
            $this->load->library('pagination'); //Cargamos la librería de paginación
            $config['base_url'] = base_url().'Listacompras/view/'; // parametro base de la aplicación, si tenemos un .htaccess nos evitamos el index.php
            $config['total_rows'] = $this->ModeloVentas->filaslcompras();//calcula el número de filas
            $config['per_page'] = $pages; //Número de registros mostrados por páginas  
            $config['num_links'] = 10; //Número de links mostrados en la paginación
            $config['first_link'] = 'Primera';//primer link
            $config['last_link'] = 'Última';//último link
            $config["uri_segment"] = 3;//el segmento de la paginación
            $config['next_link'] = 'Siguiente';//siguiente link
            $config['prev_link'] = 'Anterior';//anterior link
            $this->pagination->initialize($config); //inicializamos la paginación 
            $pagex = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $data["lcompras"] = $this->ModeloVentas->total_paginadoslcompras($pagex,$config['per_page']);
            */
            $this->load->view('templates/header');
            $this->load->view('templates/navbar');
            //$this->load->view('Personal/Personal',$data);
            $this->load->view('compras/lcompras');
            $this->load->view('templates/footer');
            $this->load->view('compras/jslcompras');
	}
    function consultar(){
        $inicio = $this->input->post('fechain');
        $fin = $this->input->post('fechafin');
        $clcompras = $this->ModeloVentas->lcomprasconsultar($inicio,$fin);
        foreach ($clcompras->result() as $item) { 
                          if ($item->kilos>0) {
                            $kiloscantidad=$item->kilos;
                          } else{
                            $kiloscantidad=$item->cantidad;
                          }

                        ?>
                       <tr id="trcli_<?php echo $item->id_detalle_compra; ?>">
                          <td><?php echo $item->id_detalle_compra; ?></td>
                          <td><?php echo $item->reg; ?></td>
                          <td><?php echo $item->producto; ?></td>
                          <td><?php echo $item->razon_social; ?></td>
                          <td><?php echo $item->cantidad; ?></td>
                          <td><?php if ($item->kilos>0) {
                                      echo $item->kilos.' Kilos';
                                    } 
                          ?></td>
                          <td>$ <?php echo number_format($item->precio_compra,2,'.',',') ; ?></td>
                          <td>$ <?php echo number_format($kiloscantidad*$item->precio_compra,2,'.',',') ; ?></td>
                        </tr>
                      <?php }
    }
  public function getlistcompras() {
        $params = $this->input->post();
        $getdata = $this->ModeloCompras->getlistcompras($params);
        $totaldata= $this->ModeloCompras->getlistcomprast($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
  }
  function comprasdetalle(){
    $params = $this->input->post();
    $compra=$params['compra'];
    $compraresult = $this->ModeloCompras->comprasdetalle($compra);
    $html='<table class="table">
            <thead>
              <tr>
                <th>Producto</th>
                <th>Cantidad</th>
                <th></th>
                <th>Precio</th>
              </tr>
            <thead>
            <tbody>
        ';
    foreach ($compraresult->result() as $item) {
        if($item->kilos>0){
          $kilos=$item->kilos." Kilos";
        }else{
          $kilos="";
        }
        $html.='<tr>
                <td>'.$item->producto.'</td>
                <td>'.$item->cantidad.'</td>
                <td>'.$kilos.'</td>
                <td>'.$item->precio_compra.'</td>
              </tr>';
    }
      $html.='</tbody></table>';
      echo $html;
  }
  function cancelarcompra(){
    $params = $this->input->post();
    $compra=$params['compra'];

    $this->ModeloCatalogos->updateCatalogo('compras',array('cancelado'=>0),array('id_compra'=>$compra));

    $resultcompra=$this->ModeloCatalogos->getselectwheren('compra_detalle',array('id_compra'=>$compra));

    foreach ($resultcompra->result() as $item) {
      $producto=$item->id_producto;
      $this->ModeloCompras->regresarproductos($item->id_producto,$item->cantidad,$item->kilos);
    }
  }
}
