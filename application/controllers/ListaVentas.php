<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ListaVentas extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloVentas');
        $this->load->model('ModeloCatalogos');
        date_default_timezone_set('America/Mexico_City');
        $this->fecha = date('Y-m-d G:i:s');
        $this->usuarioid_tz=$this->session->userdata("usuarioid_tz");
    }
    public function index(){
            if (isset($_GET['bod'])) {
                $bodega=$_GET['bod'];
            }else{
                $bodega=0;
            }
            $pages=10; //Número de registros mostrados por páginas
            //$this->load->library('pagination'); //Cargamos la librería de paginación
            //$config['base_url'] = base_url().'ListaVentas/view/'; // parametro base de la aplicación, si tenemos un .htaccess nos evitamos el index.php
            $config['total_rows'] = $this->ModeloVentas->filas($bodega);//calcula el número de filas
            $config['per_page'] = $pages; //Número de registros mostrados por páginas  
            $config['num_links'] = 3; //Número de links mostrados en la paginación
            $config['first_link'] = 'Primera';//primer link
            $config['last_link'] = 'Última';//último link
            $config["uri_segment"] = 3;//el segmento de la paginación
            $config['next_link'] = 'Siguiente';//siguiente link
            $config['prev_link'] = 'Anterior';//anterior link
            $config['reuse_query_string'] = TRUE;// para mantener los parametros del get
            //$this->pagination->initialize($config); //inicializamos la paginación 
            //$pagex = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            //$data["ventas"] = $this->ModeloVentas->total_paginados($pagex,$config['per_page'],$bodega);
            
            $this->load->view('templates/header');
            $this->load->view('templates/navbar');
            //$this->load->view('Personal/Personal',$data);
            $this->load->view('ventas/listaventas');
            $this->load->view('templates/footer');
            $this->load->view('ventas/listaventasjs');
    }
    function cancalarventa(){
        $id = $this->input->post('id');
        $this->ModeloVentas->cancalarventa($id);
        $rowresult=$this->ModeloCatalogos->getselectwheren('ventas',array('id_venta'=>$id));
        $bodega=1;
        foreach ($rowresult->result() as $item) {
            $bodega=$item->bodega;
        }
        $resultado=$this->ModeloVentas->ventadetalles($id);
        foreach ($resultado->result() as $item){
            $this->ModeloVentas->regresarpro($bodega,$item->id_producto,$item->cantidad);
        }
    }
    function buscarvent(){
        $buscar = $this->input->post('buscar');
        $bode = $this->input->post('bode');
        $resultado=$this->ModeloVentas->ventassearch($buscar,$bode);
        foreach ($resultado->result() as $item){ ?>
            <tr id="trven_<?php echo $item->id_venta; ?>">
                  <td><?php echo $item->id_venta; ?></td>
                  <td><?php echo $item->reg; ?></td>
                  <td><?php echo $item->vendedor; ?></td>
                  <td>$ <?php echo number_format($item->monto_total,2,'.',',') ; ?></td>
                  <td><?php echo $item->metodo; ?></td>
                  <td><?php echo $item->cliente; ?></td>
                  <td><?php if($item->cancelado==1){ echo '<span class="badge badge-danger">Cancelado</span>';} ?></td>
                  <td>
                    <button class="btn btn-raised gradient-blackberry white sidebar-shadow" onclick="ticket(<?php echo $item->id_venta; ?>)" title="Ticket" data-toggle="tooltip" data-placement="top">
                      <i class="fa fa-book"></i>
                    </button>
                    <button class="btn btn-raised gradient-flickr white sidebar-shadow" onclick="cancelar(<?php echo $item->id_venta; ?>,<?php echo $item->monto_total; ?>)" title="Cancelar" data-toggle="tooltip" data-placement="top" <?php if($item->cancelado==1){ echo 'disabled';} ?>>
                      <i class="fa fa-times"></i>
                    </button>
                  </td>
          </tr>
        <?php }
    }
    
    public function getlistventas() {
        $params = $this->input->post();
        $getdata = $this->ModeloVentas->getlistventas($params);
        $totaldata= $this->ModeloVentas->getlistventast($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    
    function itemsventa(){
        $params = $this->input->post();
        $idventa = $params['idventa'];
        
        $getventas=$this->ModeloVentas->getventas($idventa);
        foreach ($getventas->result() as $item){
          $monto_total = $item->monto_total;
          $descuento = $item->descuento;
        }
        $getventasd=$this->ModeloVentas->getventasd($idventa);

        $subtotal=0;
        $rowimporte=0;

        $html='<table  style="margin-top:0;margin-bottom:0;" class="table" id="itemsventas">
          <thead>
            <tr>
                <th width="10%" style="" align="center">Cant.</th>
                <th width="37%" style="" align="center">Articulo</th>
                <th width="15%" style="" align="left"></th>
                <th width="18%" style="" align="left">Pre. Uni.</th>
                <th width="20%" style="" align="left">Importe</th>
            </tr>
          </thead>
          <tbody>';
        foreach ($getventasd->result() as $rowEmp){
          if ($rowEmp->kilos>0) {
            $rowkilos = $rowEmp->kilos.' kg';
            $kilossub=$rowkilos;
          }else{
            $rowkilos='';
            $kilossub=1;
          }
          if ($rowEmp->kilos>0) {
            $rowimporte=$rowEmp->kilos*$rowEmp->precio;
            $rowcantidad=$rowEmp->kilos;
          }else{
            $rowimporte=$rowEmp->cantidad*$rowEmp->precio;
            $rowcantidad=$rowEmp->cantidad;
          }
            $html .= '
                        <tr>
                            <td width="10%" align="center">
                            <input type="hidden" value="'.$rowEmp->id_detalle_venta.'" id="idvd">
                            <input type="hidden" value="'.$rowcantidad.'" class="cantidad_'.$rowEmp->id_detalle_venta.'">
                            '.$rowEmp->cantidad.'</td>
                            <td width="37%" align="center">'.$rowEmp->categoria.' '.$rowEmp->marca.' '.$rowEmp->presentacion.'</td>
                            <td width="15%" align="left">'.$rowkilos.'</td>
                            <td width="18%" align="center">
                                <input type="number" 
                                        value="'.$rowEmp->precio.'" 
                                        class="precio_'.$rowEmp->id_detalle_venta.' form-control"
                                        id="preciovd"
                                        onchange="calcularmontos('.$rowEmp->id_detalle_venta.')"

                                        >
                            </td>
                            <td width="20%" align="center" class="importes_'.$rowEmp->id_detalle_venta.' importesall">'.round($rowimporte,2).'</td>
                        </tr>
                        ';
                        //$subtotal=$subtotal+($rowEmp->cantidad*$kilossub*$rowEmp->total);
                        $subtotal=$subtotal+$rowEmp->total;
        }
        $html.='</tbody><tfoot>';
        //if ($descuento>0) {
          $descuentol=$descuento*100;
          $html .='<tr>
                            <th style="font-size: 7" align="center" colspan="3"></th>
                            <th style="font-size: 7"  align="center">Descuento</th>
                            <th style="font-size: 6"  align="right" class="descuentoventa" data-descuento="'.$descuento.'">'.$descuentol.' %</th>
                        </tr>';
        //}
        $html .= '
                        <tr>
                            <th align="center"></th>
                            <th align="center"></th>
                            <th align="center"></th>
                            <th align="center"><b>Total</b></th>
                            <th colspan="2" align="center" class="totalventa">
                                <input type="hidden" value="'.$monto_total.'" class="totalventa">
                                <span class="totalvental">$ '.number_format($monto_total,2,'.',',').'</span>
                            </th>
                            
                        </tr>
                        </tfoot>
                  </table>';
        echo $html;
    }
    function editarventa(){
        $data = $this->input->post();
        $arrayproductos = $data['arrayproductos'];
        $total = $data['total'];
        $idventa = $data['idventa'];
        unset($data['arrayproductos']);

        $DATAc = json_decode($arrayproductos);       
        for ($i=0;$i<count($DATAc);$i++) {
            $this->ModeloCatalogos->updateCatalogo('venta_detalle',array('precio'=>$DATAc[$i]->precio),array('id_detalle_venta'=>$DATAc[$i]->id));
        }
        $this->ModeloCatalogos->updateCatalogo('ventas',array('monto_total'=>$total),array('id_venta'=>$idventa));
    }
    
    public function getPagos() {
        $id = $this->input->post("id");
        $html=""; $tot_pay=0;
        $getv=$this->ModeloCatalogos->getselectwheren('ventas',array('id_venta'=>$id));
        $getv=$getv->row();
        $monto_venta=$getv->monto_total;
        $pays=$this->ModeloCatalogos->getselectwheren('pagos_ventas',array('id_venta'=>$id,"estatus"=>1));
        foreach ($pays->result() as $p) {
            $html.="<tr>
                    <td>".$p->folio."</td>
                    <td>".$p->monto."</td>
                    <td>".$p->fecha."</td>
                    <td><button type='button' onclick='delepay(".$p->id.")' class='btn btn-warning'><i class='fa fa-minus' aria-hidden='true'></i></button></td>
                </tr>";
            $tot_pay=$tot_pay+$p->monto;
        }
        echo json_encode(array("html"=>$html,"monto_venta"=>$monto_venta,"tot_pay"=>$tot_pay));
    }

    function submitPago(){
        $data = $this->input->post();
        $data["fecha_reg"]=$this->fecha;
        $data["id_usuario"]=$this->usuarioid_tz;
        $id_reg = $this->ModeloCatalogos->tabla_inserta('pagos_ventas', $data); 
        echo $data["id_venta"];
    }

    public function deletePay(){
        $id = $this->input->post('id');
        $this->ModeloCatalogos->updateCatalogo_value(array("estatus"=>0),array('id'=>$id),'pagos_ventas');
    }

}
