<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gastos extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloCatalogos');
        date_default_timezone_set('America/Mexico_City');
        $this->fecha = date('Y-m-d G:i:s');
        $this->usuarioid_tz=$this->session->userdata("usuarioid_tz");
    }
	public function index(){
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('gastos/gastos');
        $this->load->view('templates/footer');
        $this->load->view('gastos/jsgastos');
	}

    function submitGasto(){
        $data = $this->input->post();
        $id = $data['id'];
        unset($data['id']);
        $id_reg = 0;

        if($id>0) {
          $this->ModeloCatalogos->updateCatalogo_value($data,array('id'=>$id),'gastos');
          $id_reg = $id;
        }else{
            $data["fecha_reg"]=$this->fecha;
            $data["id_usuario"]=$this->usuarioid_tz;
            $id_reg = $this->ModeloCatalogos->tabla_inserta('gastos', $data);
        }
        echo $id_reg;
    }

    public function delete(){
        $id = $this->input->post('id');
        $this->ModeloCatalogos->updateCatalogo_value(array("estatus"=>0),array('id'=>$id),'gastos');
    }

    public function getData() {
        $datas = $this->ModeloCatalogos->getGastos();
        $json_data = array("data" => $datas);
        echo json_encode($json_data);
    }
   
    
}
