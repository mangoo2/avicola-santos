<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloProductos');
        if (isset($_SESSION['bodega_tz'])) {
            $this->bodega=$_SESSION['bodega_tz'];
        }else{
            $this->bodega=0;
        }
    }
	public function index(){
        $data['total_rows'] = $this->ModeloProductos->filas();
        $data['totalexistencia'] = $this->ModeloProductos->totalproductosenexistencia();
        $data['totalproductopreciocompra'] = $this->ModeloProductos->totalproductopreciocompra();
        $data['totalproductoporpreciocompra'] = $this->ModeloProductos->totalproductoporpreciocompra();
        $data['getdescuento'] = $this->ModeloCatalogos->getdescuento(1);
        $data['getdescuento2'] = $this->ModeloCatalogos->getdescuento(2);
        $data['getdescuento3'] = $this->ModeloCatalogos->getdescuento(3);
		$this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('inicio',$data);
        $this->load->view('templates/footer');
        unset($_SESSION['pro']);
        $_SESSION['pro']=array();
        unset($_SESSION['can']);
        $_SESSION['can']=array();
	}
    function notas(){
        $user = $this->input->post('user');
        $nota = $this->input->post('nota');
        $this->ModeloCatalogos->updatenota($user,$nota);
    }
    function descuento(){
        $des = $this->input->post('des');
        $bod = $this->input->post('bod');
        $this->ModeloCatalogos->getdescuentoup($des,$bod);
    }
    function descuentov(){
        echo $this->ModeloCatalogos->getdescuento($this->bodega);
    }

    public function verificarPass(){
        $pass = $this->input->post('pass');
        $respuesta = $this->ModeloSession->validaPassAdmin($pass);
        echo $respuesta;
    }   

}