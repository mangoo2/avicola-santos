var stock_ant=0;
$(document).ready(function() {	
	$('#cproveedor').select2({
		width: 'resolve',
		minimumInputLength: 3,
		minimumResultsForSearch: 10,
		placeholder: 'Buscar un proveedor',
	  	ajax: {
	    	url: 'Compras/searchpro',
	    	dataType: "json",
	    	data: function (params) {
	      	var query = {
	        	search: params.term,
	        	type: 'public'
	      	}
	      	return query;
	    },
	    processResults: function(data){
	    	var itemspro = [];
	    	data.forEach(function(element) {
                itemspro.push({
                    id: element.id_proveedor,
                    text: element.razon_social
                });
            });
            return {
                results: itemspro
            };	    	
	    },  
	  }
	});
	$('#vproducto').select2({
		width: 'resolve',
		minimumInputLength: 3,
		minimumResultsForSearch: 10,
		placeholder: 'Buscar un Producto',
	  	ajax: {
	    	url: 'Ventas/searchproducto',
	    	dataType: "json",
	    	data: function (params) {
	      	var query = {
	        	search: params.term,
	        	type: 'public'
	      	}
	      	return query;
	    },
	    processResults: function(data){
	    	console.log(data);
	    	var clientes=data;
	    	var itemscli = [];
	    	data.forEach(function(element) {
                itemscli.push({
                    id: element.productoaddId,
                    text: element.categoria+' '+element.marca,
                    idprocat: element.categoriaId,
                    stok:element.stok
                });
            });
            return {
                results: itemscli
            };	    	
	    },  
	  }
	}).on('select2:select', function (e) {
        var data = e.params.data;
        $('#ckilos').val(0);	        
        if (data.idprocat==1) {
        	$("#ckilos").prop( "readonly", false );
        }else{
        	$("#ckilos").prop( "readonly", true );
        }
        getPromedioCompras(data.id);
        stock_ant = data.stok;
	});
	$('#ingresaventa').click(function(event) {
		if ($('#cproveedor').val()!=null) {
                $.ajax({
                    type:'POST',
                    url: 'Compras/ingresarcompra',
                    data: {
                    	uss: $('#ssessius').val(),
                        prov: $('#cproveedor').val(),
                        total: $('#vtotal').val()
                    },
                    async: false,
                    statusCode:{
                        404: function(data){
                            toastr.error('Error!', 'No Se encuentra el archivo');
                        },
                        500: function(){
                            toastr.error('Error', '500');
                        }
                    },
                    success:function(data){
                    	var idcompra=data;
                    	var DATA  = [];
            			var TABLA   = $("#productosv tbody > tr");
            				TABLA.each(function(){         
				                item = {};
				                item ["idcompra"] = idcompra;
				                item ["cant_ant"]   = $(this).find("input[id*='cant_ant']").val();
				                item ["producto"]   = $(this).find("input[id*='vsproid']").val();
				                item ["cantidad"]  = $(this).find("input[id*='vscanti']").val();
				                item ["kilos"]  = $(this).find("input[id*='vskilos']").val();
				                item ["precio"]  = $(this).find("input[id*='vsprecio']").val();
				                DATA.push(item);
				            });
            				INFO  = new FormData();
            				aInfo   = JSON.stringify(DATA);
            				INFO.append('data', aInfo);
				            $.ajax({
				                data: INFO,
				                type: 'POST',
				                url : 'Compras/ingresarcomprapro',
				                processData: false, 
				                contentType: false,
				                success: function(data){
				                }
				            });
				            toastr.success('Hecho!', 'Guardado Correctamente');
				            limpiar();
				            $('#vcantidad').val('')
							$('#vproducto').html('');
							$('#cprecio').val('');
						    $("#vproducto").val(0).change();
						    $('#vtotal').val(0);
                    }
                });
        }else{
        	toastr.error('Error', 'Seleccione el proveedor');
        }
            
    });     
});

function getPromedioCompras(id){
	//console.log("id: "+id);
	$.ajax({
        type:'POST',
        url: 'Compras/getPromedio',
        data: {
            id: id
        },
        async: false,
        success:function(data){
            $('#cprecio').val(data);
        }
    });
}

function addproducto(){
	if ($('#vcantidad').val()>0) {
		$.ajax({
	        type:'POST',
	        url: 'Compras/addproducto',
	        data: {
	            cant: $('#vcantidad').val(),
	            prod: $('#vproducto').val(),
	            prodt: $('#vproducto').text(),
	            prec: $('#cprecio').val()
	            },
	            async: false,
	            statusCode:{
	                404: function(data){
	                    toastr.error('Error!', 'No Se encuentra el archivo');
	                },
	                500: function(){
	                    toastr.error('Error', '500');
	                }
	            },
	            success:function(data){
	                var array = $.parseJSON(data);
	                console.log(array);
	                var skilos=$('#ckilos').val();
	                if (skilos==0) {
	                	var displaykilo='display: none;';
	                	var selecttotal = Number((parseFloat(array.cant)*parseFloat(array.precio)).toFixed(2));
	                }else{
	                	var displaykilo='';
	                	var selecttotal = Number((parseFloat(skilos)*parseFloat(array.precio)).toFixed(2));
	                }
	                var producto='<tr class="producto_'+array.id+'">';
	                	producto+='<td><input type="hidden" id="cant_ant" value="'+stock_ant+'"><input type="hidden" name="vsproid" id="vsproid" value="'+array.id+'">'+array.id+'</td>';
	                	producto+='<td><input type="number" name="vscanti" id="vscanti" value="'+array.cant+'" readonly style="background: transparent;border: 0px;width: 80px;"></td>';
	                	producto+='<td>'+array.producto+'</td> ';
	                	producto+='<td>$ <input type="text" name="vsprecio" id="vsprecio" value="'+array.precio+'" readonly style="background: transparent;border: 0px;width: 100px;"></td>';

	                	producto+='<td>';
		                	producto+='<div class="input-group" style="'+displaykilo+'">';
			                	producto+='<input type="text" name="vskilos" id="vskilos" value="'+skilos+'" readonly style="background: transparent;border: 0px;width: 45px; ">Kilos';
		                	producto+='</div>';
	                	producto+='</td>';

	                
	                
	                	producto+='<td>$ <input type="text" class="vstotal" name="vstotal" id="vstotal" value="'+selecttotal+'" readonly style="background: transparent;border: 0px;width: 100px;"></td>';
	                	producto+='<td><a class="danger" data-original-title="Eliminar" title="Eliminar" onclick="deletepro('+array.id+')"><i class="ft-trash font-medium-3"></i></a></td>';
	                	producto+=' </tr>';
	                $('#class_productos').append(producto);
	            }
	        });
		$('#vcantidad').val('')
		$('#vproducto').html('');
		$('#cprecio').val('');
	    $("#vproducto").val(0).change();
	}
    calculartotal();
}
function calculartotal(){
	var addtp = 0;
	$(".vstotal").each(function() {
        var vstotal = $(this).val();
        addtp += Number(vstotal);
    });
    
    $('#vtotal').val(Number(addtp).toFixed(2));
}
function limpiar(){
	$('#class_productos').html('');
}
function deletepro(id){
	$('.producto_'+id).remove();
}