var base_url = $('#base_url_g').val();
$(document).ready(function() {
	loadtable();
});
function loadtable(){
    table = $('#table').DataTable({
        destroy:true,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+"Mermas/get_data_list",
            type: "post",
        },
        "columns": [
            {"data": 'id'},
            {"data": 'categoria'},
            {"data": 'marca'},
            {"data": 'cantidad'},
            {"data": null,
                "render" : function ( url, type, row) {
                    var html='';
                    if(row.bodega==1){
                        html='Matriz';
                    }else if(row.bodega==1){
                        html='Almacén 1';
                    }else if(row.bodega==2){
                        html='Almacén 2';
                    } 
                    return html;
                }
            }, 
            {"data": 'motivo'},
            {"data": 'Usuario'},
            {"data": 'fecha_reg'},           
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[10,25, 50, 100], [10,25, 50,100]],        
    });
}
