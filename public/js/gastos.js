var base_url = $('#base_url_g').val();
$(document).ready(function() {
    loadtable();

    $("#addGasto").on("click",function(){
        $("#modal_edit").modal();
    });
    $("#savegasto").on("click",function(){
        add_form();
    });
});

function add_form(){
    var form_register = $('#form_gasto');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);

    var $validator1 = form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            concepto: {
                required: true
            },
            monto: {
                required: true
            },
            fecha:{
                required: true
            }
        },
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")) {
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        },
        invalidHandler: function(event, validator) { //display error alert on form submit              
            success_register.fadeOut(500);
            error_register.fadeIn(500);
            scrollTo(form_register, -100);
        },
        highlight: function(element) { // hightlight error inputs
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');
        },
        unhighlight: function(element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },
        success: function(label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    var $valid = $("#form_gasto").valid();
    if ($valid) {
        var datos = form_register.serialize();
        $.ajax({
            type: 'POST',
            url: base_url+'Gastos/submitGasto',
            data: datos,
            statusCode: {
                404: function(data){
                    swal("Error!", "No Se encuentra el archivo", "error");
                },
                500: function(){
                    swal("Error!", "500", "error");
                }
            },
            beforeSend: function(){
                $("#savegasto").attr("disabled",true);
             },
            success: function(data){
                swal("Éxito", "Guardado correctamente", "success");
                $("#savegasto").attr("disabled",false);
                $("#modal_edit").modal("hide");
                loadtable();
            }
        });           
    }
}

function loadtable(){
    tabla=$("#table").DataTable({
        destroy:true,
        "ajax": {
            "url": base_url+"Gastos/getData",
            type: "post",
        },
        "columns": [
            {"data":"id"},
            {"data":"concepto"},
            {"data": "monto",type: 'formatted-num',
                "render" : function ( url, type, row) {
                    var totals=0;
                    totals = new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(row.monto);
                    return totals;
                }
            }, 
            {"data":"fecha"},
            {"data":"Usuario"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                    var html='<a title="Eliminar" onclick="delete_data('+row.id+')"><i class="fa fa-trash-o" style="font-size: 20px; cursor: pointer;"></i></a> ';
                        html+='<button data-concep="'+row.concepto+'" class="btn row_'+row.id+'" type="button" onclick="editar('+row.id+','+row.monto+',\''+row.fecha+'\')" title="Editar" ><i class="fa fa-edit" style="font-size: 20px;"></i></button> ';
                    return html;
                }
            },
        ],
        "order": [[ 0, "asc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
    });
}

function editar(id,monto,fecha){
    $("#modal_edit").modal();
    var concep=$(".row_"+id+"").data("concep");
    $("#id").val(id);
    $("#monto").val(monto);
    $("#fecha").val(fecha);
    $("#concepto").val(concep);
}

function delete_data(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: '¡Atención!',
        content: '¿Está seguro de eliminar este registro?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Gastos/delete",
                    data: {
                        id:id
                    },
                    success:function(response){  
                        loadtable();
                        swal("Éxito", "Se ha eliminado correctamente", "success");
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}
