var base_url = $('#base_url_g').val();
var table;
var idventa;

$(document).ready(function() {
	table=$('#data-tables-ventas').DataTable();
	loadtable();
	$('#sieditar').click(function(event) {
		var totalventa = $('.totalventa').val();
		var DATAr  = [];
        var TABLA   = $("#itemsventas tbody > tr");
            TABLA.each(function(){         
            item = {};
            item ["id"] = $(this).find("input[id*='idvd']").val();
            item ["precio"]   = $(this).find("input[id*='preciovd']").val();
            DATAr.push(item);
        });
        INFO  = new FormData();
        arrayproductos   = JSON.stringify(DATAr);
    	
    	var datos = 'arrayproductos='+arrayproductos+'&total='+totalventa+'&idventa='+idventa;

		
		$.ajax({
            type:'POST',
            url: base_url+'ListaVentas/editarventa',
            data: datos,
            statusCode:{
                404: function(data){ toastr.error('Error!', 'No Se encuentra el archivo');},
                500: function(){ toastr.error('Error', '500');}
            },
            success:function(data){
                loadtable();
                toastr.success('Actualización correcta','Hecho!');
                $('#modaledit').modal('hide');
            },
            error: function(response){
            	toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema','Error!');
        	}
        });
	});
	$('#sicancelar').click(function(){
      params = {};
      params.id = $('#hddIdVenta').val();
      $.ajax({
        type:'POST',
        url:base_url+'ListaVentas/cancalarventa',
        data:params,
        async:false,
        success:function(data){
          toastr.success('Cancelado Correctamente','Hecho!');
          //$('#trven_'+params.id).remove();
          loadtable();
        }
      });
    });

    $('#savepay').click(function(){
        setpay();
    });

});
function loadtable(){
    table.destroy();
    var dia=$('#tipo_dia option:selected').val();
    table = $('#data-tables-ventas').DataTable({
        stateSave: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+"index.php/ListaVentas/getlistventas",
            type: "post",
            "data": {
                'ro':'0',
                'dia':dia
            },

        },
        "columns": [
            {"data": 'id_venta'},
            {"data": 'reg'},
            {"data": 'vendedor'},
            {"data": null,
                    "render" : function ( url, type, full) {
                        
                        var rw=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(full.monto_total);
                        return rw;
                    }
            },
            {"data": 'metodo'},
            {"data": 'cliente'},
            {"data": null,
            		"render" : function ( url, type, full) {
            			var html='';
            			if(full.cancelado==1){
            				html='<span class="badge badge-danger">Cancelado</span>';
            			}
            			return html;
            		}
        	},
            {"data": null,
            	"render" : function ( url, type, full) {
            		var html='';
    				html+='<div class="div_bottms">';
    				html+='<button class="btn btn-raised gradient-blackberry\
    				 white" onclick="ticket('+full.id_venta+')" title="Ticket" data-toggle="tooltip" data-placement="top">\
                                              <i class="fa fa-book"></i>\
                                            </button> ';
                       if(full.cancelado==0){ 
                            /*
                            html+='<button class="btn btn-raised gradient-blackberry white"\
                            	 	onclick="editventa('+full.id_venta+')" title="Editar" data-toggle="tooltip" data-placement="top">\
                                                    <i class="fa fa-pencil"></i>\
                                                </button> ';
                            */
                        }
                        html+='<button class="btn btn-raised gradient-flickr white" \
                        		onclick="cancelar('+full.id_venta+','+full.monto_total+')" \
                        		title="Cancelar" data-toggle="tooltip" data-placement="top"';
                        		if(full.cancelado==1){
                        			html+='disabled';
                        		} 
                        		html+='>\
                                        <i class="fa fa-times"></i>\
                                        </button> ';
                        if(full.met_pay==5){ //credito
                            html+='<button data-cli="'+full.cliente+'" class="cli_'+full.id_venta+' btn btn-success white" onclick="getpay('+full.id_venta+','+full.monto_total+','+full.cancelado+')" title="Asignar Pago" data-toggle="tooltip" data-placement="top">\
                                  <i class="fa fa-money"></i>\
                                </button> ';
                        }
                    html+='</div>';
            		return html;
            	}
        	},  
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[10,25, 50, 100], [10,25, 50,100]],
        "columnDefs": [ {
			"targets": 2,
			"orderable": true
			} ],
		dom: 'Bfrtip',
        buttons: [
        	'pageLength',
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ]
        
    });
}

function getpay(id,monto=0,cancelado){
    if(monto>0){
        $('#modalpays').modal();
    }
    
    $('#id_venta').val(id);
    $('#idtxt').html(id);
    var monto_p=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(monto);
    if(monto>0){
        $('#montotxt').html(monto_p);
    }
    $.ajax({
        type:'POST',
        url: base_url+'ListaVentas/getPagos',
        data: {
            id: id
        },
        async: false,
        success:function(data){
            //console.log(data);
            var array=$.parseJSON(data);
            //console.log("monto_venta: "+array.monto_venta);
            $('#body_pays').html(array.html);
            if(cancelado==1){
                $("#savepay").attr("disabled",true);
            }else{
                $("#savepay").attr("disabled",false);
            }
            $('#montotxt').html(new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(array.monto_venta));
            $('#pagostxt').html(new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(array.tot_pay));
            resta = parseFloat(array.monto_venta).toFixed(2) - parseFloat(array.tot_pay).toFixed(2);
            $('#totalv').val(array.monto_venta);
            $('#totalp').val(array.tot_pay);
            $('#resta').val(resta);
            $('#restatxt').html(new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(resta));
        }
    });
}

function setpay(){
    var form_register = $('#form_pay');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);

    var $validator1 = form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            folio: {
                required: true
            },
            monto: {
                required: true
            },
            fecha:{
                required: true
            }
        },
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")) {
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        },
        invalidHandler: function(event, validator) { //display error alert on form submit              
            success_register.fadeOut(500);
            error_register.fadeIn(500);
            scrollTo(form_register, -100);
        },
        highlight: function(element) { // hightlight error inputs
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');
        },
        unhighlight: function(element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },
        success: function(label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    var $valid = $("#form_pay").valid();
    if ($valid) {
        pago_tot = parseFloat($("#totalp").val()).toFixed(2);
        pago_realiza = parseFloat($("#monto").val()).toFixed(2);
        totalv = parseFloat($("#totalv").val()).toFixed(2);
        tot_act=Number(pago_tot)+Number(pago_realiza);
        /*console.log("pago_tot: "+pago_tot);
        console.log("pago_realiza: "+pago_realiza);
        console.log("tot_act: "+tot_act);
        console.log("totalv: "+totalv);*/
        if(tot_act<=totalv){
            var datos = form_register.serialize();
            $.ajax({
                type: 'POST',
                url: base_url+'ListaVentas/submitPago',
                data: datos,
                statusCode: {
                    404: function(data){
                        swal("Error!", "No Se encuentra el archivo", "error");
                    },
                    500: function(){
                        swal("Error!", "500", "error");
                    }
                },
                beforeSend: function(){
                    $("#savepay").attr("disabled",true);
                 },
                success: function(data){
                    swal("Éxito", "Guardado correctamente", "success");
                    $("#savepay").attr("disabled",false);
                    //$("#modalpays").modal("hide");
                    $("#form_pay").trigger("reset");
                    getpay(data,0);
                }
            });
        }else{
            swal("Error", "El tota de los pagos es mayor al monto de la venta", "warning");
        }         
    }
}

function delepay(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: '¡Atención!',
        content: '¿Está seguro de eliminar este pago?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"ListaVentas/deletePay",
                    data: {
                        id:id
                    },
                    success:function(response){  
                        swal("Éxito", "Se ha eliminado correctamente", "success");
                        getpay(id,0);
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}

function editventa(id){
    idventa=id;
    $('#modaledit').modal();
     $.ajax({
            type:'POST',
            url: base_url+'ListaVentas/itemsventa',
            data: {
                idventa: idventa
            },
            async: false,
            statusCode:{
                404: function(data){ toastr.error('Error!', 'No Se encuentra el archivo');},
                500: function(){ toastr.error('Error', '500');}
            },
            success:function(data){
                $('.itemsventasedit').html(data);
            }
        });
}
function calcularmontos(idventad) {
    var cantidad = $('.cantidad_'+idventad).val();
    var precio = $('.precio_'+idventad).val();
    var totalimport = parseFloat(cantidad)*parseFloat(precio);
    //console.log(cantidad+' * '+precio);
    $('.importes_'+idventad).html(totalimport.toFixed(2));

        var addtp = 0;
        $(".importesall").each(function() {
            var vstotal = $(this).html();
            addtp += Number(vstotal);
        });

    var descuento = $('.descuentoventa').data('descuento');
    var descuentot = parseFloat(addtp)*parseFloat(descuento);
    var total = addtp-descuentot;
    total=total.toFixed(2);
    $('.totalventa').val(total);
    $('.totalvental').html(new Intl.NumberFormat('es-MX',{style: "currency", currency: "MXN"}).format(total));
}