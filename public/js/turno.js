$(document).ready(function() {	
	$('#btnabrirt').click(function(){
			$.ajax({
				type:'POST',
				url:'Ventas/abrirturno',
				data:{
					cantidad: $('#cantidadt').val(),
					nombre: $('#nombredelturno').val(),
					bodega: $('#personalbodega option:selected').val()
				},
				async:false,
				success:function(data){
					toastr.success('Hecho!', 'Turno Abierto');
					$('#modalturno').modal('hide');
					
				}
			});
	});
	$('#cerrarturno').click(function(){
			$.ajax({
				type:'POST',
				url:'Ventas/cerrarturno',
				data:{
					id: $('#idturno').val()
				},
				async:false,
				success:function(data){
					toastr.success('Turno cerrado', 'Hecho!');
					document.getElementById("cantidadt").disabled = false;
					document.getElementById("nombredelturno").disabled = false;
					document.getElementById("btnabrirt").disabled = false;
					document.getElementById("btncerrar").disabled = true;
					document.getElementById("cerrarturno").disabled = true;
					$('#cantidadt').val('');
					$('#nombredelturno').val('');
					$('#horainicial').val('');
					//alert(data);
				}
			});	
	});
	$('#btncerrar').click(function(){
		params = {};
		params.fecha1 = $('#txtInicio').val();
		params.fecha2 = $('#txtFin').val();
		params.bodega = $('#personalbodega option:selected').val();
		if(params.fecha1 != '' && params.fecha2 != ''){
			$.ajax({
				type:'POST',
				url:'Corte_caja/corte',
				data:params,
				async:false,
				success:function(data){
					//console.log(data);
					var array = $.parseJSON(data);
					$('#tbCorte').html(array.tabla);
					$('#ddescuento').html(array.descuento);
					/*$('#cRealizadas').html(array.cRealizadas);
					$('#cContado').html(array.cContado);
					$('#cCredito').html(array.cCredito);
					$('#cTerminal').html(array.cTerminal);
					$('#cCheque').html(array.cCheque);
					$('#cTransferencia').html(array.cTransferencia);
					$('#cPagos').html(array.cPagos);
					$('#dContado').html('$ '+array.dContado);
					$('#dCredito').html('$ '+array.dCredito);
					$('#dTerminal').html('$ '+array.dTerminal);
					$('#dCheque').html('$ '+array.dCheque);
					$('#dTransferencia').html('$ '+array.dTransferencia);
					$('#dPagos').html('$ '+array.dPagos);*/
					$('#dTotal3').html(new Intl.NumberFormat('es-MX').format(array.super_tot2));
                    //$('#dImpuestos').html(''+array.dImpuestos);
                    $('#dSubtotal').html(''+array.dSubtotal);
                    $('#abonos').html(array.totabonos);
                    $('#gastos').html(array.tot_gasto);
                    var cantidadt=$('#cantidadt').val();
                    $('#val2').html(cantidadt);
                    //console.log("super_tot2: "+array.super_tot2);
                    var caja=parseFloat(cantidadt)+parseFloat(array.totabonos2)+parseFloat(array.super_tot2);
                    $('#stotal2').html(new Intl.NumberFormat('es-MX').format(caja-array.tot_gasto2));
                    $('#sample_4').DataTable({
				        dom: 'Bfrtip',
				        buttons: [          
				            {
				                extend: 'excel',
				                title: 'Corte de turno'
				            },
				            {
				                extend: 'pdf',
				                title: 'Corte de turno'
				            },
				        'pageLength'
				        ]
				    });
				}
			});
		}
		else{
			toastr.error('No existen fechas validas', 'Error');
			
		}
	});
});
function imprimir(){
      window.print();
}
function seleccionar(){
    var bode=$('#personalbodega option:selected').val();
    location.href="Turno?bod="+bode;
}