-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 27-06-2024 a las 19:16:50
-- Versión del servidor: 10.4.10-MariaDB
-- Versión de PHP: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `avicola_santos`
--

DELIMITER $$
--
-- Procedimientos
--
DROP PROCEDURE IF EXISTS `SP_ADD_DATOSFACTURA`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_ADD_DATOSFACTURA` (IN `_IDVENTA` INT, IN `_TEXT` TEXT)  NO SQL
UPDATE ventas SET datosfactura=_TEXT WHERE id_venta=_IDVENTA$$

DROP PROCEDURE IF EXISTS `SP_ADD_PERSONAL`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_ADD_PERSONAL` (IN `_NOM` VARCHAR(120), IN `_SEX` INT(1))  NO SQL
INSERT INTO personal (nombre,sexo)VALUES (_NOM,_SEX)$$

DROP PROCEDURE IF EXISTS `SP_DEL_PERSONAL`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_DEL_PERSONAL` (IN `_ID` INT)  NO SQL
UPDATE personal SET estatus=0 where personalId=_ID$$

DROP PROCEDURE IF EXISTS `SP_GET_ALL_ESTADOS`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_GET_ALL_ESTADOS` ()  NO SQL
SELECT * FROM Estado$$

DROP PROCEDURE IF EXISTS `SP_GET_ALL_PERFILES`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_GET_ALL_PERFILES` ()  NO SQL
SELECT *  FROM Perfiles$$

DROP PROCEDURE IF EXISTS `SP_GET_ALL_PERSONAL`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_GET_ALL_PERSONAL` (IN `_BODE` INT(1))  NO SQL
BEGIN
  CASE _BODE
      WHEN 0 THEN
      SELECT per.personalId,per.nombre,per.apellidos,usu.Usuario 
        FROM personal as per 
        LEFT JOIN usuarios as usu on usu.personalId=per.personalId
        WHERE per.tipo=1 AND per.estatus=1;
     ELSE
        SELECT per.personalId,per.nombre,per.apellidos,usu.Usuario 
        FROM personal as per 
        LEFT JOIN usuarios as usu on usu.personalId=per.personalId
        WHERE per.tipo=1 AND per.estatus=1 AND per.bodega=_BODE;
      END CASE;
END$$

DROP PROCEDURE IF EXISTS `SP_GET_DETALLEVENTA`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_GET_DETALLEVENTA` (IN `_ID` INT)  NO SQL
SELECT cat.categoria,mar.marca,pre.presentacion,pre.unidad,vdll.cantidad,vdll.kilos,vdll.precio,(vdll.cantidad*vdll.precio) as total
FROM venta_detalle as vdll
INNER JOIN sproductosub as sprob on sprob.subId=vdll.id_producto
INNER JOIN sproducto as pro on pro.productoaddId=sprob.productoaddId
INNER JOIN categoria as cat on cat.categoriaId=pro.productoId
INNER JOIN marca as mar on mar.marcaid=pro.MarcaId
INNER JOIN presentaciones as pre ON pre.presentacionId=sprob.PresentacionId
WHERE vdll.id_venta=_ID$$

DROP PROCEDURE IF EXISTS `SP_GET_MENUS`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_GET_MENUS` (IN `PERFIL` INT)  NO SQL
SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, Perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.PerfilId=PERFIL$$

DROP PROCEDURE IF EXISTS `SP_GET_PERSONAL`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_GET_PERSONAL` (IN `_ID` INT)  NO SQL
SELECT *  FROM personal where personalId=_ID$$

DROP PROCEDURE IF EXISTS `SP_GET_PRODUCTOMAS_TURNO`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_GET_PRODUCTOMAS_TURNO` (IN `_FINICIO` DATETIME, IN `_FFIN` DATETIME, IN `_BODEGA` INT(1))  NO SQL
SELECT concat(cat.categoria,' ',mar.marca,' ',pre.presentacion)as producto, sum(vd.cantidad) as total
from venta_detalle as vd
inner join sproductosub as p on vd.id_producto=p.subId
inner join presentaciones as pre on pre.presentacionId=p.PresentacionId
inner join sproducto as pro on pro.productoaddId=p.productoaddId
inner join categoria as cat on cat.categoriaId=pro.productoId
inner join marca as mar on mar.marcaid=pro.MarcaId
inner join ventas as v on vd.id_venta=v.id_venta
where v.cancelado=0 and v.reg>=_FINICIO and v.reg<=_FFIN AND v.bodega=_BODEGA GROUP BY producto ORDER BY `total` DESC LIMIT 1$$

DROP PROCEDURE IF EXISTS `SP_GET_PRODUCTOS_TURNO`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_GET_PRODUCTOS_TURNO` (IN `_FINICIO` DATETIME, IN `_FFIN` DATETIME, IN `_BODEGA` INT(1))  NO SQL
SELECT concat(cat.categoria,' ',mar.marca,' ',pre.presentacion)as producto,vd.cantidad, vd.precio
from venta_detalle as vd
inner join sproductosub as p on vd.id_producto=p.subId
inner join presentaciones as pre on pre.presentacionId=p.PresentacionId
inner join sproducto as pro on pro.productoaddId=p.productoaddId
inner join categoria as cat on cat.categoriaId=pro.productoId
inner join marca as mar on mar.marcaid=pro.MarcaId
inner join ventas as v on vd.id_venta=v.id_venta
where v.cancelado=0 and v.reg>=_FINICIO and v.reg<=_FFIN AND v.bodega=_BODEGA$$

DROP PROCEDURE IF EXISTS `SP_GET_SEARCHPRODUCTO`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_GET_SEARCHPRODUCTO` (IN `_PRO` TEXT)  NO SQL
SELECT * FROM(
    SELECT spro.productoaddId,pro.categoria,pro.img, mar.marca,pro.categoriaId,spros.stok,spros.stok2,spros.stok3
                FROM sproducto as spro
                left join sproductosub as spros on spros.productoaddId=spro.productoaddId and spros.tipo=1
                inner JOIN categoria as pro on pro.categoriaId=spro.productoId
                inner join marca as mar on mar.marcaid=spro.MarcaId
                where spro.activo=1
) as prod
WHERE prod.categoria LIKE concat("%",_PRO,"%") or prod.marca LIKE concat("%",_PRO,"%")$$

DROP PROCEDURE IF EXISTS `SP_GET_SESSION`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_GET_SESSION` (IN `USUA` VARCHAR(30))  NO SQL
SELECT usu.UsuarioID,per.personalId,per.nombre,per.bodega, usu.perfilId, usu.contrasena FROM usuarios as usu,personal as per where per.personalId = usu.personalId and usu.Usuario = USUA$$

DROP PROCEDURE IF EXISTS `SP_GET_TOTAL_CREDITOSVENCIDOS`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_GET_TOTAL_CREDITOSVENCIDOS` ()  NO SQL
SELECT COUNT(*) as total
FROM ventas as ven
inner JOIN clientes as cli on cli.ClientesId=ven.id_cliente and cli.ClientesId!=45
WHERE ADDDATE(DATE_FORMAT(ven.reg,'%Y-%m-%d'),interval cli.dcredito day)=DATE_FORMAT(now(),'%Y-%m-%d')$$

DROP PROCEDURE IF EXISTS `SP_GET_TOTAL_CREDITOSVENCIDOS_DLL`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_GET_TOTAL_CREDITOSVENCIDOS_DLL` ()  NO SQL
SELECT ven.id_venta,ven.reg,ven.monto_total,cli.ClientesId,cli.Nom,cli.dcredito
FROM ventas as ven
inner JOIN clientes as cli on cli.ClientesId=ven.id_cliente and cli.ClientesId!=45
WHERE ADDDATE(DATE_FORMAT(ven.reg,'%Y-%m-%d'),interval cli.dcredito day)=DATE_FORMAT(now(),'%Y-%m-%d')$$

DROP PROCEDURE IF EXISTS `SP_GET_USUARIOS`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_GET_USUARIOS` ()  NO SQL
SELECT usu.UsuarioID,usu.Usuario,per.nombre as perfil FROM usuarios as usu, Perfiles as per WHERE usu.perfilId=per.perfilId$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

DROP TABLE IF EXISTS `categoria`;
CREATE TABLE IF NOT EXISTS `categoria` (
  `categoriaId` int(11) NOT NULL AUTO_INCREMENT,
  `categoria` varchar(200) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `img` text NOT NULL,
  `activo` int(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`categoriaId`)
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`categoriaId`, `categoria`, `img`, `activo`) VALUES
(1, 'HUEVO', '180803-164558catHUEVO.jpg', 1),
(2, 'Aceite', '190709-112507catrkJWiPIwe_930x525.jpg	', 0),
(3, 'SOPAS INSTANTANEAS', '190709-135112catdescarga (3).jpg', 0),
(4, 'SALSAS ', '190709-135659catdescarga (12).jpg', 0),
(5, 'ACEITES ', '190712-112644catimages (1).jpg', 1),
(6, 'COMIDA PARA PERRO ', '190712-112934catimages (3).jpg', 1),
(7, 'COMIDA PARA GATO ', '190712-113014catdescarga (22).jpg', 1),
(8, 'DETERGENTE EN POLVO ', '190712-113318catimages (4).jpg', 1),
(9, 'DESINFECTANTES ', '190712-113345catdescarga (23).jpg', 1),
(10, 'SALSAS ', '190712-113438catimages (5).jpg', 1),
(11, 'AZUCAR ', '190712-114144catimages (6).jpg', 1),
(12, 'SAL', '190712-114230catdescarga (25).jpg', 1),
(13, 'ARROZ ', '190712-114359catimages (7).jpg', 1),
(14, 'PASTAS', '190712-114413catimages (8).jpg', 1),
(15, 'CERILLOS ', '190712-114508catdescarga (26).jpg', 1),
(16, 'HARINA ', '190712-114608catdescarga (27).jpg', 1),
(17, 'VELADORAS ', '190712-114713catdescarga (28).jpg', 1),
(18, 'FRIJOL ', '190712-114830catdescarga (29).jpg', 1),
(19, 'PAPEL HIG.', '190712-114854catdescarga (30).jpg', 1),
(20, 'LECHE ', '190712-115104catimages (9).jpg', 1),
(21, 'JABÓN EN BARRA ', '190712-115333catdescarga (32).jpg', 1),
(22, 'SAZONADORES ', '190712-115549catdescarga (33).jpg', 1),
(23, 'CAFE ', '190712-115632catdescarga (34).jpg', 1),
(24, 'MAIZ ', '190712-115743catdescarga (35).jpg', 0),
(25, 'GALLETAS', '190712-120003catdescarga (36).jpg', 1),
(26, 'MAYONESA', '190712-120141catdescarga (4).jpg', 1),
(27, 'CHILES EN LATA ', '190712-120449catimages (10).jpg', 1),
(28, 'CHOCOLATE ', '190816-143545cattumblr_mmen7sSkvv1rocz98o1_400.jpg', 1),
(29, 'SHAMPOO ', '190712-120729catdescarga (38).jpg', 0),
(30, 'LECHE CONDENSADA ', '190712-121144catimages (12).jpg', 1),
(31, 'GELATINA EN POLVO ', '190712-121343catdescarga (39).jpg', 0),
(32, 'ATUN EN LATA ', '190712-121434catdescarga (40).jpg', 1),
(33, 'SERVILLETAS', '190712-121540catimages (13).jpg', 1),
(34, 'CHOCOLATE EN POLVO ', '190712-121751catdescarga (41).jpg', 0),
(35, 'FRUTA EN ALMIBAR EN LATA ', '190712-122403catdescarga (43).jpg', 1),
(36, 'SABORES PARA AGUA ', '190712-122507catimages (14).jpg', 1),
(37, 'MEDICAMENTO EFERVESCENTE ', '190712-122916catdescarga (44).jpg', 0),
(38, 'PEGAMENTO', '190712-123039catdescarga (45).jpg', 0),
(39, 'SOPA INSTANTANEA ', '190712-123412catdescarga (46).jpg', 1),
(40, 'PAÑALES ', '190712-123523catimages (15).jpg', 0),
(41, 'MAIZENA ', '190712-123627catdescarga (11).jpg', 0),
(42, 'JUGOS ', '190714-101750catdescarga (11).jpg', 0),
(43, 'PALOMITAS ', '190712-124052catdescarga (49).jpg', 0),
(44, 'CHILE EN POLVO ', '190712-124156catdescarga (50).jpg', 0),
(45, 'TÉ', '190712-124454catimages (16).jpg', 0),
(46, 'BIDON MARAVILLA ', '190712-135108catdescarga (1).jpg', 0),
(47, 'IMPERIAL 5LT. PZA', '190712-140043catdescarga.png', 0),
(48, 'PERRON CACHORRO ', '190712-141937catdescarga (10).jpg', 0),
(49, 'CAMPEON CACHORRO ', '190712-143458catdescarga (14).jpg', 0),
(50, 'DET. BLANCA NIEVES 010/001 KGS', '190712-150818catdescarga (29).jpg', 0),
(51, 'VOGUE 559', '190712-164159catdescarga (56).jpg', 0),
(52, 'FRIJOLES ENLATADOS ', '190712-174322catdescarga (77).jpg', 1),
(53, 'SARDINA ', '190714-095901catdescarga (8).jpg', 1),
(54, 'SUAVITEL VARIOS AROMAS', '190810-153757catsuavitel varios.jpg', 0),
(55, 'SUAVITEL VARIOS AROMAS ', '190810-154626catsuavitel varios.jpg', 0),
(56, 'SUAVITEL VARIOS AROMAS', '190810-154920catsuavitel varios.jpg', 0),
(57, 'JABÓN LIQUIDO ', '190810-155045catsuavitel varios.jpg', 0),
(58, 'SUAVITEL C AROMA DE SOL', '190810-155734catsuavitel varios.jpg', 0),
(59, 'FABULOSO VARIOS AROMAS', '190810-155930catfabuloso varios.jpg', 0),
(60, 'VINAGRES ', '190827-124923catvinagre-para-eliminar-las-liendres.jpg', 0),
(61, 'LA CONCEPCION ', '', 0),
(62, 'HUEVO', '201012-110926catsANFANDILA.jpg', 0),
(63, 'MAIZENA ', '201218-122319catdescarga.jpg', 1),
(64, 'CATEGORIA PRUEBA MANGOO', '210811-133355catSoftware a la medida (16).png', 1),
(65, 'JUGOS EMBOTELLADOS', '220210-141924catJUMEX 450 ML.jfif', 0),
(66, 'CIGARROS', '220819-160702catdownload.jpg', 1),
(67, 'PALOMITAS', '220823-092847catimages.jpg', 0),
(68, 'JABON', '220823-094104catimages.jpg', 1),
(69, 'TOSTADAS', '220912-110548catdownload.jpg', 0),
(70, 'VINAGRE ', '221111-125949catimages.jpg', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

DROP TABLE IF EXISTS `clientes`;
CREATE TABLE IF NOT EXISTS `clientes` (
  `ClientesId` int(11) NOT NULL AUTO_INCREMENT,
  `Nom` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `Calle` varchar(45) DEFAULT NULL,
  `noExterior` varchar(45) DEFAULT NULL,
  `Colonia` varchar(45) DEFAULT NULL,
  `Localidad` varchar(45) DEFAULT NULL,
  `Municipio` varchar(45) DEFAULT NULL,
  `Estado` varchar(45) DEFAULT NULL,
  `Pais` varchar(45) DEFAULT NULL,
  `CodigoPostal` varchar(45) DEFAULT NULL,
  `Correo` varchar(100) NOT NULL,
  `noInterior` varchar(45) NOT NULL,
  `nombrec` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `correoc` varchar(45) NOT NULL,
  `telefonoc` varchar(30) NOT NULL,
  `extencionc` varchar(20) NOT NULL,
  `nextelc` varchar(30) NOT NULL,
  `descripcionc` text NOT NULL,
  `dcredito` int(11) NOT NULL,
  `activo` int(1) NOT NULL DEFAULT 1 COMMENT '1 activo 0 eliminado',
  PRIMARY KEY (`ClientesId`)
) ENGINE=InnoDB AUTO_INCREMENT=130 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`ClientesId`, `Nom`, `Calle`, `noExterior`, `Colonia`, `Localidad`, `Municipio`, `Estado`, `Pais`, `CodigoPostal`, `Correo`, `noInterior`, `nombrec`, `correoc`, `telefonoc`, `extencionc`, `nextelc`, `descripcionc`, `dcredito`, `activo`) VALUES
(45, 'PUBLICO EN GENERAL', '', '', '', '', '', '', 'Mexico', '', '', '', '', '', '', '', '', '', 0, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compras`
--

DROP TABLE IF EXISTS `compras`;
CREATE TABLE IF NOT EXISTS `compras` (
  `id_compra` int(11) NOT NULL AUTO_INCREMENT,
  `id_proveedor` int(11) NOT NULL,
  `monto_total` double NOT NULL,
  `reg` timestamp NOT NULL DEFAULT current_timestamp(),
  `cancelado` tinyint(1) NOT NULL DEFAULT 1 COMMENT '1 activo 0 cancelado',
  PRIMARY KEY (`id_compra`),
  KEY `constraint_fk_04` (`id_proveedor`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `compras`
--

INSERT INTO `compras` (`id_compra`, `id_proveedor`, `monto_total`, `reg`, `cancelado`) VALUES
(1, 1, 9100, '2023-10-20 20:50:42', 1),
(2, 1, 905, '2023-10-20 20:56:36', 1),
(3, 1, 3365, '2023-10-20 21:17:07', 1),
(4, 1, 3701.5, '2023-10-20 21:17:37', 1),
(5, 1, 670, '2023-10-20 21:18:10', 1),
(6, 1, 6175, '2023-10-20 21:24:44', 1),
(7, 1, 22717.5, '2023-10-20 21:25:44', 1),
(8, 1, 600, '2023-10-20 21:33:42', 1),
(9, 1, 1000, '2023-11-09 00:51:29', 1),
(10, 1, 525, '2023-11-09 00:54:28', 1),
(11, 1, 2300, '2023-11-17 21:47:22', 1),
(12, 1, 1800, '2023-11-17 21:48:56', 1),
(13, 1, 1300, '2023-11-27 16:59:19', 1),
(14, 1, 1350, '2023-11-27 17:17:54', 1),
(15, 1, 1400, '2023-11-27 17:20:11', 1),
(16, 1, 150, '2023-11-27 17:25:10', 1),
(17, 1, 140, '2023-11-27 17:27:07', 1),
(18, 1, 155, '2023-11-27 17:28:46', 1),
(19, 1, 155, '2023-11-27 17:31:55', 1),
(20, 1, 155, '2023-11-27 17:34:16', 1),
(21, 1, 155, '2023-11-27 17:36:28', 1),
(22, 1, 775, '2023-11-27 17:37:57', 1),
(23, 1, 775, '2023-11-27 17:39:32', 1),
(24, 1, 640, '2023-11-27 17:40:18', 1),
(25, 1, 1650, '2023-11-27 17:41:42', 1),
(26, 1, 1750, '2023-11-27 17:43:20', 0),
(27, 1, 1600, '2023-11-28 20:13:18', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compra_detalle`
--

DROP TABLE IF EXISTS `compra_detalle`;
CREATE TABLE IF NOT EXISTS `compra_detalle` (
  `id_detalle_compra` int(11) NOT NULL AUTO_INCREMENT,
  `id_compra` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `cantidad` float NOT NULL,
  `kilos` float NOT NULL COMMENT 'solo en el caso del huevo',
  `precio_compra` double NOT NULL,
  PRIMARY KEY (`id_detalle_compra`),
  KEY `constraint_fk_08` (`id_compra`),
  KEY `constraint_fk_09` (`id_producto`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `compra_detalle`
--

INSERT INTO `compra_detalle` (`id_detalle_compra`, `id_compra`, `id_producto`, `cantidad`, `kilos`, `precio_compra`) VALUES
(1, 1, 84, 10, 0, 910),
(2, 2, 84, 1, 0, 905),
(3, 3, 107, 10, 0, 336.5),
(4, 4, 100, 11, 0, 336.5),
(5, 5, 100, 2, 0, 335),
(6, 6, 720, 10, 250, 24.7),
(7, 7, 720, 50, 975, 23.3),
(8, 8, 720, 3, 60, 10),
(9, 9, 848, 10, 0, 100),
(10, 10, 848, 5, 0, 105),
(11, 11, 848, 20, 0, 115),
(12, 12, 848, 15, 0, 120),
(13, 13, 848, 10, 0, 130),
(14, 14, 848, 10, 0, 135),
(15, 15, 848, 10, 0, 135.56),
(16, 16, 848, 1, 0, 150),
(17, 17, 848, 1, 0, 140),
(18, 23, 848, 5, 0, 155),
(19, 24, 848, 4, 0, 160),
(20, 25, 848, 10, 0, 165),
(21, 26, 848, 10, 0, 175),
(22, 27, 848, 10, 0, 160);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `descuentos`
--

DROP TABLE IF EXISTS `descuentos`;
CREATE TABLE IF NOT EXISTS `descuentos` (
  `descuentoId` int(11) NOT NULL AUTO_INCREMENT,
  `activado` int(1) NOT NULL,
  `bodega` int(1) NOT NULL,
  `reg` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`descuentoId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `descuentos`
--

INSERT INTO `descuentos` (`descuentoId`, `activado`, `bodega`, `reg`) VALUES
(1, 1, 1, '2018-06-29 16:44:08'),
(2, 0, 2, '2018-06-29 16:44:08'),
(3, 0, 3, '2018-06-29 16:44:08');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado`
--

DROP TABLE IF EXISTS `estado`;
CREATE TABLE IF NOT EXISTS `estado` (
  `EstadoId` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(50) DEFAULT NULL,
  `Alias` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`EstadoId`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `estado`
--

INSERT INTO `estado` (`EstadoId`, `Nombre`, `Alias`) VALUES
(1, 'AGUASCALIENTES', 'AS'),
(2, 'BAJA CALIFORNIA', 'BC'),
(3, 'BAJA CALIFORNIA SUR', 'BS'),
(4, 'CAMPECHE', 'CC'),
(5, 'COAHUILA', 'CL'),
(6, 'COLIMA', 'CM'),
(7, 'CHIAPAS', 'CS'),
(8, 'CHIHUAHUA', 'CH'),
(9, 'DISTRITO FEDERAL', 'DF'),
(10, 'DURANGO', 'DG'),
(11, 'GUANAJUATO', 'GT'),
(12, 'GUERRERO', 'GR'),
(13, 'HIDALGO', 'HG'),
(14, 'JALISCO', 'JC'),
(15, 'ESTADO DE MEXICO', 'MC'),
(16, 'MICHOACAN', 'MN'),
(17, 'MORELOS', ''),
(18, 'NAYARIT', 'NT'),
(19, 'NUEVO LEON', 'NL'),
(20, 'OAXACA', 'OC'),
(21, 'PUEBLA', 'PL'),
(22, 'QUERETARO', 'QT'),
(23, 'QUINTANA ROO', 'QR'),
(24, 'SAN LUIS POTOSI', 'SP'),
(25, 'SINALOA', 'SL'),
(26, 'SONORA', 'SR'),
(27, 'TABASCO', 'TC'),
(28, 'TAMAULIPAS', 'TS'),
(29, 'TLAXCALA', 'TL'),
(30, 'VERACRUZ', 'VZ'),
(31, 'YUCATAN', 'YN'),
(32, 'ZACATECAS', 'ZS');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gastos`
--

DROP TABLE IF EXISTS `gastos`;
CREATE TABLE IF NOT EXISTS `gastos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `monto` decimal(8,2) NOT NULL,
  `concepto` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `fecha` date NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `fecha_reg` datetime NOT NULL,
  `estatus` varchar(1) COLLATE utf8_spanish_ci NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `marca`
--

DROP TABLE IF EXISTS `marca`;
CREATE TABLE IF NOT EXISTS `marca` (
  `marcaid` int(11) NOT NULL AUTO_INCREMENT,
  `marca` varchar(120) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `imgm` text NOT NULL,
  `activo` int(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`marcaid`)
) ENGINE=InnoDB AUTO_INCREMENT=498 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `marca`
--

INSERT INTO `marca` (`marcaid`, `marca`, `imgm`, `activo`) VALUES
(1, 'Capullo', '190709-112608marcapullo.jpg', 0),
(2, 'Calvario', '190709-125026marcal.jpg', 0),
(3, 'SALSA BOTANERA LT', '190709-135752mardescarga (12).jpg', 0),
(4, 'GUADALUPE', '190709-141622marHUEVO LUPE.png', 0),
(5, 'CAMARON PIQUIN', '', 0),
(6, 'CAMARON HABANERO', '', 0),
(7, 'CAMARON SOLO', '', 0),
(8, 'POLLO', '', 0),
(9, 'GUADALUPE ', '190712-124853mardescarga.png', 1),
(10, 'CALVARIO ', '190714-113446mardescarga (16).jpg', 1),
(11, 'MAMA GALLINA ', '190712-131528mardescarga (2).png', 1),
(12, 'CALVARIO EXTRA ', '190714-113500mardescarga (16).jpg', 1),
(13, 'ALVAREZ ', '', 0),
(14, 'ALVAREZ ', '', 0),
(15, 'ALVAREZ ', '', 0),
(16, 'CHAPARRAL', '', 0),
(17, 'CHAPARRAL', '', 0),
(18, 'CHAPARRAL BLANCO', '190712-133258mardescarga (52).jpg', 1),
(19, 'ALVAREZ ', '190712-133551mardescarga (1).png', 1),
(20, 'AVANDARO', '190823-130433mar20190823_122538 (2).jpg', 1),
(21, 'ALVAREZ EXTRA ', '190712-134042mardescarga (1).png', 1),
(22, 'LA CARRETA', '191216-144442mar20191216_143412.jpg', 1),
(23, 'CHAPARRAL SEMISUCIO', '190712-134259mardescarga (52).jpg', 1),
(24, 'CHAPARRAL SUCIO ', '190712-134337mardescarga (52).jpg', 1),
(25, 'MARAVILLA LT ', '190712-134834mardescarga.jpg', 1),
(26, 'MARAVILLA  24/500 ml', '190712-134922mardescarga.jpg', 1),
(27, 'BIDON MARAVILLA ', '190712-135236mardescarga (1).jpg', 1),
(28, 'IMPERIAL 19LT', '190712-135746mardescarga.png', 1),
(29, 'IMPERIAL CAJA 5LT', '190712-135932mardescarga.png', 1),
(30, 'IMPERIAL 5LT. PZA', '190712-140131mardescarga.png', 1),
(31, 'ACEITE AVE 900ML', '190712-140234mardescarga (2).jpg', 1),
(32, 'PODER CANINO ', '190712-140622mardescarga (3).jpg', 1),
(33, 'PEDIGREE ADULTO ', '190712-140652mardescarga (4).jpg', 1),
(34, 'PEDIGREE CACHORRO ', '190712-140723mardescarga (5).jpg', 1),
(35, 'PAL PERRO 25KG', '190712-140757mardescarga (6).jpg', 1),
(36, 'DOG CHOW AMARILLO ', '190712-140942mardescarga (7).jpg', 1),
(37, 'DOG CHOW ROJO ', '190712-141058mardescarga (7).jpg', 1),
(38, 'DOG CHOW CACHORRO ', '190712-141157mardescarga (8).jpg', 1),
(39, 'MEGACAN ', '190712-141701mardescarga (12).jpg', 1),
(40, 'NOGACAN ADULTO', '190712-141816marCRO.jpg', 1),
(41, 'PERRON ADULTO ', '190712-141912mardescarga (11).jpg', 1),
(42, 'PERRON CACHORRO 20KG', '190712-142050mardescarga (10).jpg', 1),
(43, 'CAMPEON ADULTO ', '190712-143441mardescarga (13).jpg', 1),
(44, 'CAMPEON CACHORRO ', '190712-143541mardescarga (14).jpg', 1),
(45, 'DON CAN ADULTO 25KG', '190712-143727mardescarga (15).jpg', 1),
(46, 'DYNO ADULTO 25KG', '190712-143804mardescarga (16).jpg', 1),
(47, 'SUPER CAN FEROZ ', '190712-144315mardescarga (17).jpg', 1),
(48, 'BERISCAN ADULTO ', '190712-144345mardescarga (18).jpg', 1),
(49, 'BERISCAN CACHORRO ', '190712-144418mardescarga (19).jpg', 1),
(50, 'PRO-CAN ADULTO ', '190715-153950marPRO-CAN-ADULTOS-RAZAS-MEDIANAS-Y-GRANDES-RECETA-ORIGINAL.jpg', 1),
(51, 'NOGOCAN CACHORRO 15KG', '190712-144725mardescarga (21).jpg', 1),
(52, 'CANPRO', '190712-144908mardescarga (22).jpg', 1),
(53, 'GATINA 20KGS', '190712-145311mardescarga (24).jpg', 1),
(54, 'PAL GATO ', '190712-145341mardescarga (25).jpg', 1),
(55, 'WHISKAS', '190712-145406mardescarga (26).jpg', 1),
(56, 'MININO ', '190712-145433mardescarga (27).jpg', 1),
(57, 'DET.ROMA 040/250 GRS', '190712-145837mardescarga (28).jpg', 1),
(58, 'DET.ROMA 10 KGS', '190712-145932mardescarga (28).jpg', 1),
(59, 'DET.ROMA 020/500 GRS', '190712-150011mardescarga (28).jpg', 1),
(60, 'DET. BLANCA NIEVES 500', '190712-150138mardescarga (29).jpg', 0),
(61, 'DET. BLANCA NIEVES 10 KGS', '190712-150240mardescarga (29).jpg', 0),
(62, 'DET.ROMA 010/001 KGS', '190712-150555mardescarga (28).jpg', 1),
(63, 'DET. BLANCA NIEVES 500', '190712-150736mardescarga (29).jpg', 1),
(64, 'DET. BLANCA NIEVES 010/001 KGS', '190712-150918mardescarga (29).jpg', 1),
(65, 'DET. BLANCA NIEVES 10 KGS', '190712-151011mardescarga (29).jpg', 1),
(66, 'DET. BLANCA NIEVES C/40/250 GRS', '190712-151057mardescarga (29).jpg', 1),
(67, 'DET. FOCA 1 KG', '190712-151213mardescarga (30).jpg', 1),
(68, 'DET. FOCA 250 GRS', '190712-151257mardescarga (30).jpg', 1),
(69, 'DET. FOCA 500 GRS', '190712-151333mardescarga (30).jpg', 1),
(70, 'DET. ARIEL 500 GRS', '190712-151541mardescarga (31).jpg', 1),
(71, 'DET. ARIEL 850 GRS', '190712-151613mardescarga (31).jpg', 1),
(72, 'ACEITE 123 1lt', '190712-151752mardescarga (32).jpg', 1),
(73, 'ACEITE NUTRIOLI ', '190712-151928mardescarga (33).jpg', 1),
(74, 'ACEITE CAPULLO ', '190712-151959mardescarga (34).jpg', 1),
(75, 'CLARASOL 12/1', '190712-152133mardescarga (35).jpg', 1),
(76, 'CLARASOL 24/500', '190712-152203mardescarga (35).jpg', 1),
(77, 'CLORALEX C/12/950', '190712-152316mardescarga (36).jpg', 0),
(78, 'CLORALEX C/20/500', '190712-152406mardescarga (36).jpg', 1),
(79, 'BOTANERA 3.8LT CAJA', '190712-152723marimages.jpg', 0),
(80, 'BOTANERA 3.8LT CAJA', '190712-152829marimages.jpg', 0),
(81, 'BOTANERA 3.8LT CAJA', '190715-153358mar0073854501061L.jpg', 1),
(82, 'BOTANERA 3.8LT PZ', '190715-153433mar0073854501061L.jpg', 1),
(83, 'VALENTINA 24/370', '190712-153048mardescarga (37).jpg', 1),
(84, 'VALENTINA 12/1', '190712-153122mardescarga (37).jpg', 1),
(85, 'AZUCAR ', '190712-153513mardescarga (40).jpg', 1),
(86, 'SAL FINA 20KG', '190729-103209mardescarga (1).png', 1),
(87, 'SAL FINA 50 KG', '190712-154059mardescarga (1).png', 1),
(88, 'SAL FINA 18 KG', '190712-154124mardescarga (1).png', 1),
(89, 'SAL ROCHE ', '190729-103128marpasted image 95x1672.jpg', 1),
(90, 'ARROZ SOS ', '190712-154558mardescarga (41).jpg', 1),
(91, 'ARROZ SAMAN 25KG', '190712-154720marimages (1).jpg', 1),
(92, 'ARROZ TIA CATA ', '190712-154741mardescarga (42).jpg', 1),
(93, 'SAN DIEGO EXTRA VERDE 10 KG', '190712-154831mardescarga (3).png', 0),
(94, 'SAN DIEGO EXTRA MEXICO ', '190712-154915mardescarga (3).png', 0),
(95, 'ARROZ LA COSECHA 10 KGS', '190712-155146marimages (2).jpg', 0),
(96, 'ARROZ PIONERA ', '190712-155210mardescarga (43).jpg', 1),
(97, 'ARROZ AGUILA VERDE 25 KG', '190712-155430mardescarga (44).jpg', 0),
(98, 'CERILLOS JAGUAR CAJA ', '190712-161218mardescarga (45).jpg', 1),
(99, 'CERILLOS JAGUAR PZ ', '190712-161245mardescarga (45).jpg', 1),
(100, 'CERILLOS CLASICOS CAJA / 10 PAQUETES', '190712-161304mardescarga (46).jpg', 1),
(101, 'CERILLOS CLASICOS PZ ', '190712-161331mardescarga (46).jpg', 1),
(102, 'HARINA CUSPIDE 44 KG', '190712-161554mardescarga (47).jpg', 1),
(103, 'HARINA OMEGA 44KG', '190712-161639mardescarga (48).jpg', 1),
(104, 'HARINA 10 KGS', '190712-161746mardescarga (49).jpg', 1),
(105, 'VELADORA SUPER LIMONERO C/20', '190712-162009mardescarga (50).jpg', 1),
(106, 'VELADORA ARAMO REFACCION C/40', '190712-162134mardescarga (51).jpg', 0),
(107, 'FRIJOL NEGRO ', '190712-162425mardescarga (52).jpg', 1),
(108, 'FRIJOL MICHIGAN ', '190712-162549mardescarga (53).jpg', 1),
(109, 'FRIJOL SINALOA ', '190712-163048marimages (3).jpg', 0),
(110, 'HIG. SUNNY ', '190712-163458mardescarga (55).jpg', 1),
(111, 'HIG. VOGUE 400', '190712-163641mardescarga (56).jpg', 1),
(112, ' HIG. PETALO ULTRA 400 C/12 ', '190712-164039mardescarga (58).jpg', 1),
(113, 'HIG. PETALO ULTRA 300', '190712-164059mardescarga (58).jpg', 1),
(114, 'HIG. PETALO ULTRA 400 C/10 ', '190712-164136mardescarga (58).jpg', 1),
(115, 'HIG. REGIO RINDE+ 400H', '190811-074506marpapel-higienico-regio-rinde-mas-4-rollos-D_NQ_NP_870378-MLM31230581823_062019-F.jpg', 1),
(116, 'HIG. VOGUE 600', '190716-130041mar20711.png', 1),
(117, 'ALPURA ROJA ', '190729-101157marlogo-alpura.jpg', 1),
(118, 'ALPURA DESLACTOSADA', '190729-101235marlogo-alpura.jpg', 1),
(119, 'MILECHE ', '190729-101446mar274469.jpg', 1),
(120, 'NUTRILECHE ', '190729-101515mar300_300.png', 1),
(121, 'TAMARIZ ', '190729-102342marP2-20.jpg', 1),
(122, 'TAMARIZ DESLACTOSADA ', '190729-102402mar604498701731-00-CH515Wx515H.jpg', 1),
(123, 'CLARISSA ', '190712-165442mardescarga (5).png', 1),
(124, 'LALA ENTERA ', '190712-165600mardescarga (62).jpg', 1),
(125, 'LALA DESLACTOSADA ', '190712-165745mardescarga (62).jpg', 1),
(126, 'VITALECHE ', '190712-165850mardescarga (63).jpg', 1),
(127, 'LECHE DELITE ', '190729-101657mar750301518102-00-CH515Wx515H.jpg', 1),
(128, 'JABON PALMOLIVE ', '190712-170143mardescarga (65).jpg', 1),
(129, 'JABON CORONA 25PZ/400GRS', '190712-170259mardescarga (66).jpg', 0),
(130, 'JABON CORONA ROSA 25PZ/400GRS', '190712-170353mardescarga (67).jpg', 0),
(131, 'RIKO POLLO  PZ', '190712-170558mardescarga (68).jpg', 1),
(132, 'KNORR SUIZA PZ', '190712-170626marimages.png', 1),
(133, 'CAFE LEGAL ', '190712-171220mardescarga (6).png', 1),
(134, 'NESCAFE EN SOBRE ', '190712-171255mardescarga (69).jpg', 1),
(135, 'NESCAFE CLASICO 16/42 GRS', '190712-171335mardescarga (70).jpg', 1),
(136, 'GALLETAS ANIMALITOS ', '190712-171555mardescarga (71).jpg', 1),
(137, 'McCORMICK 390GRS', '190712-171907mardescarga (72).jpg', 1),
(138, 'McCORMICK 190GRS', '190712-171945mardescarga (72).jpg', 1),
(139, 'McCORMICK 2.8K', '190712-172022mardescarga (72).jpg', 0),
(140, 'McCORMICK 105GRS', '190712-172056mardescarga (72).jpg', 1),
(141, 'McCORMICK INSTITUCIONAL CAJA', '190716-144311marcaja-mayonesa-mccormick-4-piezas-de-28-kilos-D_NQ_NP_746240-MLM26921111383_022018-Q.jpg', 1),
(142, 'McCORMICK INSTITUCIONAL PZ ', '190712-172217mardescarga (72).jpg', 0),
(143, 'LA MORENA CHIPOTLES 40/100GRS', '190712-172650mardescarga (73).jpg', 1),
(144, 'LA MORENA CHIPOTLES 48/210GRS', '190712-172741mardescarga (73).jpg', 1),
(145, 'LA MORENA CHIPOTLES 40PZ-100GRS', '190712-172828mardescarga (73).jpg', 0),
(146, 'LA MORENA CHIPOTLE 48/210GRS', '190712-172903mardescarga (73).jpg', 1),
(147, 'LA MORENA JALAPEÑOS 3K', '190712-172959mardescarga (73).jpg', 0),
(148, 'LA MORENA JALAPEÑOS 24/380GRS', '190712-173041mardescarga (73).jpg', 1),
(149, 'LA MORENA JALAPEÑOS 48PZ-210GRS', '190712-173146mardescarga (73).jpg', 1),
(150, 'LA MORENA JALAPEÑOS 48PZ-200GRS', '190712-173238mardescarga (73).jpg', 1),
(151, 'LA MORENA JALAPEÑOS 2.8GRS CAJA', '190712-173319mardescarga (73).jpg', 1),
(152, 'LA MORENA RAJAS VDES 40/100 GR', '190712-173427mardescarga (73).jpg', 0),
(153, 'LA MORENA RAJAS ROJAS 48PZ-210GRS', '190712-173509mardescarga (73).jpg', 1),
(154, 'LA MORENA SERRANOS 40PZ-100GRS', '190712-173556mardescarga (73).jpg', 0),
(155, 'LA MORENA RAJAS VERDES 2.8GRS', '190712-173625mardescarga (73).jpg', 1),
(156, 'LA MORENA RAJAS ROJAS 48PZ-210GRS', '190712-173658mardescarga (73).jpg', 1),
(157, 'LA MORENA RAJAS VERDES 48PZ-210GRS', '190712-173837mardescarga (73).jpg', 1),
(158, 'KNORR TOMATE PZ', '190712-173953mardescarga (74).jpg', 1),
(159, 'CHOCOLATE ABUELITA ', '190712-174101mardescarga (75).jpg', 1),
(160, 'SHAMPOO PALMOLIVE ', '190712-174159mardescarga (76).jpg', 0),
(161, 'LECHERA CAJA ', '190712-174532mardescarga (78).jpg', 1),
(162, 'CARNATION CLAVEL CAJA', '190712-174653mardescarga (79).jpg', 1),
(163, 'SAYES AGUA ', '190712-175134mardescarga (7).png', 0),
(164, 'SAYES LECHE ', '190712-175152mardescarga (7).png', 0),
(165, 'ATUN DOLORES DE AGUA C/48/140', '190712-175407mardescarga (8).png', 0),
(166, 'ATUN DOLORES DE ACEITE  C/48/140', '190712-175429mardescarga (8).png', 0),
(167, 'ITALPASTA ', '190712-175825mardescarga (81).jpg', 1),
(168, 'SERV. ADORABLE  C/12/500', '190714-074347mardescarga (82).jpg', 0),
(169, 'SERV. LYS C/12/500', '190714-074538marimages (4).jpg', 0),
(170, 'SERV. PETALO LARGA C/12/420 CJ ', '210326-152933marSERPET03_2_65e3.png', 1),
(171, 'SERV. PREMIER C/12/500', '190714-075756mardescarga (84).jpg', 0),
(172, 'SERV. MAXIMA RECT C/12/500', '190714-075907mardescarga (85).jpg', 0),
(173, 'SERV. ADORABLE 125H', '190714-080209mardescarga (86).jpg', 0),
(174, 'CHOCOMILK ', '190714-080344mardescarga (87).jpg', 0),
(175, 'DON AGUSTIN DURAZNO ', '190714-080625mardescarga (88).jpg', 1),
(176, 'PIÑA DON AGUSTIN', '201219-122310mardescarga.jpg', 1),
(177, 'TANG ', '190714-080903mardescarga (90).jpg', 1),
(178, 'ALKA-SELTZER', '190714-081045mardescarga (91).jpg', 1),
(179, 'SAL DE UVAS ', '190714-081215mardescarga (92).jpg', 1),
(180, 'KOLA-LOKA', '190714-081326mardescarga (93).jpg', 1),
(181, 'MARUCHAN HABANERO Y PIQUIN', '190714-081427mardescarga (94).jpg', 1),
(182, 'PAÑAL ABSORSEC GRANDE ', '190729-101109marhigiene-cuidado-bebe-panales-D_NP_651766-MLM27571018569_062018-Q.jpg', 0),
(183, 'PAÑAL ABSORSEC JUMBO ', '190714-081800mardescarga (95).jpg', 0),
(184, 'PAÑAL ABSORSEC ULTRA MEDIANO', '190714-081939mardescarga (96).jpg', 0),
(185, 'MAIZENA ', '190714-082047mardescarga (97).jpg', 0),
(186, 'MAIZENA ', '190714-082200mardescarga (98).jpg', 0),
(187, 'BOING C/24/500', '190714-082401marimages.jpg', 0),
(188, 'BOING C/27/250', '190714-082433marimages.jpg', 0),
(189, 'PALOMITAS ', '190714-082546mardescarga.jpg', 1),
(190, 'TAJIN CLASICO ', '190714-082640mardescarga (2).jpg', 0),
(191, 'TAJIN CLASICO ', '190714-082727mardescarga (3).jpg', 0),
(192, 'TÉ McCORMICK', '190714-093951mardescarga (4).jpg', 0),
(193, 'LA SIERRA REFRITOS BAYO 24/440', '190714-095040mardescarga (5).jpg', 1),
(194, 'LA SIERRA REFRITOS NEGRO 24/440', '190714-100144mardescarga (10).jpg', 1),
(195, 'LA SIERRA REFRITOS BAYO 12/580', '190714-095247mardescarga (5).jpg', 1),
(196, 'LA SIERRA REFRITOS NEGRO 12/580', '190714-100220mardescarga (10).jpg', 1),
(197, 'LA SIERRA FRIJOL ENTERO BAYO ', '190714-095651mardescarga (6).jpg', 1),
(198, 'LA SIERRA FRIJOL ENTERO NEGRO ', '190714-095739mardescarga (7).jpg', 1),
(199, 'SARDINA CALMEX', '190714-100022mardescarga (9).jpg', 1),
(200, 'CHAPARRAL FRAGIL', '190714-105603mardescarga (12).jpg', 1),
(201, 'CONOS DE HUEVO', '190714-110614mardescarga (13).jpg', 1),
(202, 'YEMA ', '190714-111218mardescarga (14).jpg', 1),
(203, 'CASCADO ', '190714-111704mardescarga (15).jpg', 1),
(204, 'CALVARIO FRAGIL ', '190714-112424mardescarga (16).jpg', 1),
(205, 'SAN ANGEL ', '190714-112604mardescarga (17).jpg', 1),
(206, 'GREDT', '190714-112846mardescarga (17).jpg', 0),
(207, 'CALVARIO CHICO', '190714-113720mardescarga (16).jpg', 1),
(208, 'SAN ANGEL FRAGIL ', '190714-113741mardescarga (17).jpg', 1),
(209, 'GREDT CHICO ', '190714-113829mardescarga (17).jpg', 0),
(210, 'ACEITE IMPERIAL 900ML', '190714-120842mardescarga (18).jpg', 1),
(211, 'BOTINERA C/12/1.5LT CAJA ', '190715-151203mar0073854501061L.jpg', 1),
(212, 'BOTANERA 1.5LT PZ ', '190715-151251mar0073854501061L.jpg', 1),
(213, 'JABON ZOTE 400GR', '190716-133927marUnknown.jpeg', 1),
(214, 'MORO JABON 350G', '190716-134110mar1176.jpg', 0),
(215, 'JABON corona c/25-400', '200710-140241marJABON CORONA AMARILLO.jpg', 1),
(216, 'CAFE DOLCA EXHIBIDOR', '190716-140228mar7501000912605-00-CH515Wx515H.jpeg', 0),
(217, 'MAIZ SINALOA ', '190716-141623marimages.png', 0),
(218, 'McCORMICK INSTITUCIONAL PZ', '190716-144223marcaja-mayonesa-mccormick-4-piezas-de-28-kilos-D_NQ_NP_746240-MLM26921111383_022018-Q.jpg', 1),
(219, 'LA MORENA JALAPEÑOS 3K PZ', '190716-150442mar190712-172959mardescarga (73).jpg', 1),
(220, 'LA MORENA RAJAS VERDES 2.8GRS PZ', '190717-133030mar190712-172650mardescarga (73).jpg', 1),
(221, 'CHOCOLATE ABUELITA PIEZA ', '190717-135302mar190712-174101mardescarga (75).jpg', 1),
(222, 'CARNATION CLAVEL PZ ', '190717-140130mar190712-174653mardescarga (79).jpg', 0),
(223, 'ATUN DOLORES ACEITE C/36/133', '190717-141440mar190712-175429mardescarga (8).png.jpeg', 1),
(224, 'ATUN DOLORES DE AGUA C/36/133', '190717-141608mar190712-175429mardescarga (8).png.jpeg', 1),
(225, 'SERVILLETAS. MAX RECT C/48', '190717-161747mar190714-075907mardescarga (85).jpg', 0),
(226, 'SERV. PETALO BLANCA C/24/2/100', '190717-162642mar190714-075509mardescarga (83).jpg', 0),
(227, 'SERV.ADORABLE C/12/500 PZ', '190717-163917mar190714-074347mardescarga (82).jpg', 0),
(228, 'SERV. LYS C/12/500 PZ', '190717-165632mar190714-074538marimages (4).jpg', 0),
(229, 'SERV. PETALO BCA C/12/420 PZ', '190717-170444mar190714-075509mardescarga (83) (1).jpg', 1),
(230, 'SERV.PREMIER C/12/500 PZ', '190717-170718mar190714-075756mardescarga (84).jpg', 0),
(231, 'SERV.MAXIMA RECT C/12/500 PZ', '190717-170822mar190714-075907mardescarga (85) (1).jpg', 0),
(232, 'PAÑAL ABSORSEC GRANDE PZ ', '190729-101029marhigiene-cuidado-bebe-panales-D_NP_651766-MLM27571018569_062018-Q.jpg', 0),
(233, 'PAÑAL ABSORSEC JUMBO PZ', '190717-182349mar190714-081800mardescarga (95).jpg', 0),
(234, 'PAÑAL ABSORSEC ULTRA MEDIANO PZ', '190717-182459mar190714-081939mardescarga (96).jpg', 0),
(235, 'CLARISSA ', '190802-225531margrupo_maulec_logo_clarissa.png', 1),
(236, 'GUADALUPE SUCIO ', '190802-231538marUnknown.png', 1),
(237, 'ESCUDO ', '190803-001114mar719062.jpg', 1),
(238, 'GUADALUPE ESPECIAL ', '190803-134258mardescarga.png', 1),
(239, 'DON CAN ADULTO 20KG', '190803-134759mar190712-143727mardescarga (15).jpg', 1),
(240, 'RAJAS VERDES 2.8 GR CJ', '190806-104124mar190712-173427mardescarga (73).jpg', 1),
(241, 'LA MORENA JALAPEÑOS 2.8GRS PZ', '190806-104857mar190712-173427mardescarga (73).jpg', 1),
(242, 'FABULOSO VARIOS AROMAS', '190810-153737marfabuloso varios.jpg', 0),
(243, 'SUAVITEL C PRIMAVERAL', '190813-125318mar7509546067513-190x190.jpg', 0),
(244, 'SUAVITEL C AROMA DE SOL', '190813-125105mar7509546067520-515x515.jpg', 0),
(245, 'SUAVITEL C ACQUA', '190813-125008mar7509546067506-300x300.jpg', 0),
(246, 'SUAVITEL ', '190813-123924marsuavizante-de-telas-suavitel-anochecer-800-ml-D_NQ_NP_910815-MLM30576106925_052019-Q.jpg', 1),
(247, 'FABULOSO ', '190813-124104mar1205.jpg', 1),
(248, 'FABULOSO MAR FRESCO', '190813-124216mar1210.jpg', 0),
(249, 'FABULOSO ENERGIA NARANJA', '190813-124326marlimpiador-liquido-fabuloso-energia-naranja-multiusos-1-lt-D_NQ_NP_908311-MLM30350796830_052019-F.jpg', 0),
(250, 'FABULOSO ALTERNATIVA CLOR', '190813-124439mar11140.jpg', 0),
(251, 'GALLETAS MARIAS 20/160GR', '190813-124805marcaja-galletas-maria-gamesa-de-850-grs-con-12-piezas-D_NQ_NP_647118-MLM29832142179_042019-F.jpg', 1),
(252, 'GALLETAS ANIMALITOS GRANEL 5 KG', '190813-122919mar190712-171555mardescarga (71).jpg', 1),
(253, 'HARINA DE MAIZ SAN BLAS 10/1 ', '190813-130417mar0001671_harina-de-trigo-101kg-san-blas_510.jpeg', 0),
(254, 'HARINA HOT CAKES 10/1', '190813-132713marimages.jpg', 0),
(255, 'GALLETAS ANIMALITO 5 KG', '190812-110430margalletas animalito.jpg', 0),
(256, 'GALLETAS ANIMALITO 5 KG', '', 0),
(257, 'HARINA DE TRIGO SAN BLAS  10/1', '190813-130953marHARIAN TRIGO.png', 1),
(258, 'CARNATION CLAVEL PIEZAS', '190816-012909mar190712-174653mardescarga (79).jpg', 1),
(259, 'LECHERA PIEZAS ', '190816-013050mar190712-174532mardescarga (78).jpg', 1),
(260, 'CHOCOLATE IBARRA CAJA', '190816-120842marCHOCOLATE IBARRA.jpg', 0),
(261, 'CHOCOLATE IBARRA PIEZA', '190816-120933marCHOCOLATE IBARRA.jpg', 0),
(262, 'GUADALUPE SUCIO', '190817-162621marHUEVO GUADALUPE.jpg', 0),
(263, 'GUADALUPE EXTRA', '190819-092931mar190712-124853mardescarga.png', 1),
(264, 'DET. ARIEL 250 GRS	', '190820-145933mar190712-151541mardescarga (31).jpg', 1),
(265, 'LECHE BOREAL ', '190823-124910mar7501020560695-00-CH515Wx515H.jpg', 1),
(266, 'VINAGRE BLANCO CLEMENTE JACQUES', '190827-130246mar1.jpg', 0),
(267, 'LA MORENA CHILES JALAPEÑOS 12PZ-800GRS', '190903-102517mar190712-172650mardescarga (73).jpg', 1),
(268, 'LA MORENA CHILES JALAPEÑOS 24PZ-380GRS', '190903-102557mar190712-172650mardescarga (73).jpg', 1),
(269, 'ROMA VEGETAL C/25 400 GR', '190911-150955marroma-jabon-de-lavanderia-100-vegetal-caja-25-pz-de-400g-D_NQ_NP_926147-MLM27406631894_052018-F.jpg', 0),
(270, 'JABON CORONA C/50 200 GR', '190911-151242mar190712-170353mardescarga (67).jpg', 0),
(271, 'JABON ZOTE AZUL C/25 400GR', '190912-143548marUnknown.jpeg', 0),
(272, 'JABON ZOTE BCO C/50 200 GR', '190912-143739marUnknown.jpeg', 0),
(273, 'JABON ZOTE BCO C/60 100GR', '190912-143805marUnknown.jpeg', 0),
(274, 'JABON ZOTE ROSA C/60 100 GR', '190912-143901marUnknown.jpeg', 0),
(275, 'JABON DAROMA BCO C/25 400 GR', '190912-144022marUnknown-1.jpeg', 0),
(276, 'JABON CARISMA C/25 400 GR', '190912-144118marUnknown.jpeg', 0),
(277, 'JABON MURANO PEPINO C/40 150 GR', '190912-144242marUnknown.jpeg', 0),
(278, 'JABON MURANO MANDARINA C/40 150 GR', '190912-144327marUnknown.jpeg', 0),
(279, 'JABON MURANO VERBENA C/40 150GR', '190912-144415marUnknown.jpeg', 0),
(280, 'JABON MURANO SELVA NEGRA C/40 150GR', '190912-144459marUnknown.jpeg', 0),
(281, 'JABON MURANO LIMON C/CHIA 40 150GR', '190912-144640marUnknown.jpeg', 0),
(282, 'JABON MURANO FRAMBUESA/CHIA 40 150GR', '190912-144722marUnknown.jpeg', 0),
(283, 'JABON MADI VEGETAL C/40 150GR', '190912-144848marUnknown.jpeg', 0),
(284, 'JABON TERSSO C/50 120 GR', '190912-144926marUnknown.jpeg', 0),
(285, 'DET PURO SOL 10/1 KG', '190912-145011marUnknown.jpeg', 0),
(286, 'DET PURO SOL 20/500GR', '190912-145126marUnknown.jpeg', 0),
(287, 'DET PURO SOL 40/250GR', '190912-145146marUnknown.jpeg', 0),
(288, 'DET BRILOZA C/10 1KG', '190912-145233marUnknown.jpeg', 0),
(289, 'DET BRILOZA C/20 500GR', '190912-145252marUnknown.jpeg', 0),
(290, 'JABON LIQ FOCA C/12 LT', '190912-145432marUnknown.jpeg', 0),
(291, 'JABON LIQ BLANCA NIEVES C/12 LT', '190912-145520marUnknown.jpeg', 0),
(292, 'JABON LIQ BRILOZA C/12 730 ML', '190912-145349marUnknown.jpeg', 0),
(293, 'JABON LIQ ROMA C/12 LT', '190912-145606marUnknown.jpeg', 0),
(294, 'DET CARISMA C/10 1 KG', '190912-145709marUnknown.jpeg', 0),
(295, 'DET CARISMA C/20 500 GR', '190912-145729marUnknown.jpeg', 0),
(296, 'ACEITE 123 500ml', '190917-094737mar190712-151752mardescarga (32).jpg', 1),
(297, 'JABON ZOTE BCO C/25 400GR', '190917-100936mar190912-143739marUnknown.jpeg', 0),
(298, 'JABON TERSSO C/30 200GR', '190917-101152mar190912-144926marUnknown.jpeg', 0),
(299, 'JABON ZOTE ROSA 50/200GR', '191015-130802mar190716-133927marUnknown.jpeg', 0),
(300, 'CERILLOS EL PIPILA', '191028-161122marimages.jpg', 0),
(301, 'CHAPETES ', '191028-160455mar20191028_152814 (2).jpg', 1),
(302, 'CHAPETES ', '', 0),
(303, 'ROCKO', '191028-160118mardescarga (1).jpg', 1),
(304, 'MARUCHAN CAMARON ', '191119-122459mar645.jpg', 0),
(305, 'HUEVO CHAPARRAL EXTRA', '191216-143255mar190712-133258mardescarga (52).jpg', 1),
(306, 'LA ESPERANZA ', '200123-154809mardescarga.jpg', 1),
(307, 'AVICOLA ALDRETE ', '', 0),
(308, 'LA CONCEPCION ', '200605-115746marIMG_1591375916437.jpg', 1),
(309, 'RANCHO GRANDE ', '200201-142258marimages.jpg', 1),
(310, 'RANCHO GRANDE EXTRA ', '200201-143728marimages.jpg', 0),
(311, 'LA CONCEPCION EXTRA', '200605-120113marIMG_1591375916437.jpg', 1),
(312, 'LECHE TAMARIZ 1.5 LT', '200309-142621mardescarga.jpg', 0),
(313, 'SERVILLETA PRUEBA C/12 ', '200320-073731marcedaiztapalapa-com-servilletas-prueba-500-piezas-bolsa-200px.jpg', 1),
(314, 'SERVILLETA PRUEBA C/12 PIEZA', '200320-074755marcedaiztapalapa-com-servilletas-prueba-500-piezas-bolsa-200px.jpg', 1),
(315, 'PINOL', '200325-222624marUnknown.jpeg', 1),
(316, 'CHAPARRAL ROJO', '200821-143828mardescarga (1).jpg', 1),
(317, 'ARROZ SCHETTINO 10 KG', '200605-114403mardescarga.jpg', 0),
(318, 'CONOS CASCADO', '210813-103836marcono cascado.jpg', 1),
(319, 'AVANDARO PALOMERO', '200821-144237mar190823-130433mar20190823_122538 (2).jpg', 1),
(320, 'CONCEPCION EXTRA', '200605-115855marIMG_1591375916437.jpg', 0),
(321, 'CERILLOS MAYA', '200605-114504mardescarga (1).jpg', 1),
(322, 'AVANDARO SUCIO', '200821-144146mar190823-130433mar20190823_122538 (2).jpg', 1),
(323, 'CONCEPCION FRAGIL', '200821-144004mar200605-115746marIMG_1591375916437.jpg', 1),
(324, 'HACHI 25KG', '200618-123605marIMG_1592501492046 (1).jpg', 0),
(325, 'RAJAS VERDES 24/380', '200821-143336mardescarga.jpg', 1),
(326, 'VITA LECHE', '200710-140039marVITALECHE.jpg', 1),
(327, 'FLOR DEL SURESTE', '200710-140103marFLOR DE LECHE.png', 1),
(328, '', '', 0),
(329, 'SALSA DE CHIPOTLE ', '200828-124528mardescarga (2).jpg', 1),
(330, 'CERILLOS MANOLA', '200905-093757marMANOLA.jpg', 1),
(331, 'CANPRO-MAX', '200908-102643mardescarga.jfif', 1),
(332, 'LA MORENA RAJAS ROJAS 48-200GRS', '200909-141212mar2ag6PTGB.jpg', 0),
(333, 'TITAN', '200914-115346mar20200914_112901.jpg', 0),
(334, 'TITAN', '', 0),
(335, 'KATTOS', '200914-132210marimages.jpg', 1),
(336, 'BOOB', '200914-132402mardescarga (3).jpg', 1),
(337, 'TITAN ', '200914-132522marimages (1).jpg', 1),
(338, 'SAL LA FINA BOTE 1KG ', '200928-123209marSAL.jpg', 1),
(339, 'LA MORENA JALAPEÑOS 48PZ-200GRS', '200929-164013mardescarga.jpg', 0),
(340, 'LA MORENA RAJAS VERDES 48PZ-200GRS', '200929-164110mardescarga.jpg', 0),
(341, 'LA MORENA SERRANOS VERDES 40/140 ', '201007-161458mardescarga.jpg', 1),
(342, 'SANFANDILA', '201012-111027marsANFANDILA.jpg', 1),
(343, 'SANFANDILA', '', 0),
(344, 'PERRON CACHORRO 15 KG', '201031-131740marPERRON CACHORRO.JPG', 1),
(345, 'GALLETAS SURTIDO RICO', '', 0),
(346, 'GALLETAS SURTIDO RICO', '201127-125820marsurtido rico.jpg', 1),
(347, 'HIG ELITE COLOR', '201127-130823marELITE.jpg', 1),
(348, 'ACEITE DE OLIVO 1LT', '201201-110248marimages.jpg', 1),
(349, 'ACEITE DE OLIVO 45ML', '201201-110342marimages.jpg', 1),
(350, 'LA MORENA RAJAS VERDES 40/100GRS', '201213-104635mardescarga.jpg', 1),
(351, 'FLAN SAYES', '201217-100147marFLAN SAYES.jfif', 0),
(352, 'ARROZ EL AGUILA', '201217-171931mararroz aguila.jpeg', 1),
(353, 'MAIZENA', '201217-171946marmaizena.jfif', 1),
(354, 'LECHE GANATA', '201217-172003marganata.jfif', 1),
(355, 'SUAVITEL', '201217-172020marsuavitel.jfif', 0),
(356, 'SERVILLETA PETALO C/24/2/100 CHICA', '210326-153016marCHICA.png', 1),
(357, 'GALLETA SURTIDO RICO CJ', '201221-112833marsurtido rico.jpg', 1),
(358, 'DURAZNOS ALMIBAR PZ', '201221-113127mardescarga.jpg', 1),
(359, 'PIÑA TROPICAL HARVEST', '210323-140242marpiña tropical.jpg', 1),
(360, 'PALOMITAS CHILE LIMON', '201226-173935mardescarga.jfif', 1),
(361, 'LECHE LIO', '201230-163955mardescarga.png', 1),
(362, 'ATUN CALMEX ACEITE C/48/133', '210106-131410marATUN CALMEX.jpg', 0),
(363, 'ATUN CALMEX AGUA C/48/133', '210106-131430marATUN CALMEX.jpg', 0),
(364, 'ATUN CALMEX ACEITE C/48/133', '210107-104342mardownload.jpg', 1),
(365, 'ATUN CALMEX AGUA C/48/133', '210107-104406mardownload.jpg', 1),
(366, 'SARDINA DOLORES', '210109-173230marSARDINA DOLORES.jpg', 1),
(367, 'ACEITE CAPULLO 500 ML', '210114-114648mardownload.jpg', 1),
(368, 'ARCOIRIS C/10KG', '210112-150131marDETERGENTE ARCOIRISS.jfif', 1),
(369, 'PREMIO AZUL C/9 KG', '210112-150209marPREMIO AZUL.jfif', 1),
(370, 'VELADORA ARAMO ', '210122-125709mardescarga.jpg', 1),
(371, 'VITALPRO', '210122-130450mardescarga (1).jpg', 1),
(372, 'GATINA 15KGS', '210128-171322mardescarga.jpg', 1),
(373, 'BLANQUEADOR EL CHINITO', '210129-143210mardescarga.jpg', 1),
(374, 'VELADORA OCOTLAN', '210129-143347mardescarga (1).jpg', 1),
(375, 'ARROZ ITALRISO 1KG', '210129-173430mardescarga.jpg', 1),
(376, 'KNORR SUIZA CAJA', '210202-093104marKNOOR.png', 1),
(377, 'KNORR TOMATE CAJA', '210202-093330martomate.jpg', 1),
(378, 'RIKO POLLO CAJA', '210202-093511marrikopollo.jpg', 1),
(379, 'PIÑA EN REB SAN MARCOS 12/800', '210215-100807marPIÑA SAN MARCOS.jpg', 1),
(380, 'ACEITE DE OLIVA 5 LT PZ', '210219-101620marGALON ACEITE.jpg', 1),
(381, 'CLORALEX C/12/950', '210226-143924mar1...jpg', 1),
(382, 'TAMARIZ 1.5 LT', '210227-120823marTAMARIZ 1.JPG', 1),
(383, 'DURAZNO EN REB SAN MARCOS 12/820', '210311-105608marDURAZNO SAN MARCOS.JPG', 1),
(384, 'ACEITE DE OLIVO 5 LITROS', '210323-094822marGALON ACEITE.jpg', 0),
(385, 'NESCAFE CLASICO 12/120 GRS', '210323-141206marNESCAFE 120.jpg', 1),
(386, 'LA CONCEPCION PALOMERO', '210408-120740mar200605-115746marIMG_1591375916437.jpg', 1),
(387, 'RANCHO GRANDE PALOMERO', '210410-104450marRANCHO.JPG', 1),
(388, 'ACEITE GRAN TRADICIÓN 1 LT.', '210422-103643marGran tradición.jpeg', 1),
(389, 'GALLETAS ANIMALITOS PZ', '210513-131117mardescarga.jpg', 1),
(390, 'MARUCHAN PROMOCIÓN POLLO Y CAMARÓN', '210527-095524marmaruchan.jpg', 1),
(391, 'MARAVILLA 12/946 ML', '210612-165516marWhatsApp Image 2021-06-12 at 2.06.30 PM.jpeg', 1),
(392, ' PORTALES', '210707-102441marPORTALES.JPG', 0),
(393, 'LOS PORTALES', '210707-102512marPORTALES.JPG', 1),
(394, 'KAKI-CAN', '210721-142358markaki.jpg', 1),
(395, 'PORTALES SEMI-SUCIO', '210721-153829marPORTALES.JPG', 1),
(396, 'MARCA PRUEBA MANGOO', '210811-133441marSoftware a la medida (15).png', 1),
(397, 'GIGANTES EXTRA', '210904-155740margigantes.PNG', 0),
(398, 'CALVARIO EXTRA/SUCIO', '210909-100805marcalvario.jpg', 1),
(399, 'GREDT', '210928-130430marIMG_20210928_095154_resized_20210928_095457991.jpg', 1),
(400, 'MAIZENA SOBRE', '211002-155243mar0750100510680L_2e0bbd49-76c7-4d54-a9a2-8194c6987df0_1000x.jpg', 0),
(401, 'PORTALES SEMI-SUCIO', '211005-122509marPORTALS.jfif', 1),
(402, 'GREDT SUCIO', '211022-121351mar210928-130528marIMG_20210928_095154_resized_20210928_095457991.jpg', 1),
(403, 'GREDT EXTRA', '211118-123705mar210928-130528marIMG_20210928_095154_resized_20210928_095457991.jpg', 1),
(404, 'GREDT EXTRA', '211027-155754marGREDT.JPG', 0),
(405, 'ALDRETE', '211119-130615mar20211119_130117.jpg', 1),
(406, 'KOLIBRÍ', '211129-112240markolibri.jpg', 1),
(407, 'BELA  PZ', '220818-115414mardownload.jpg', 1),
(408, 'BELA 10/500 ', '220818-115353mardownload.jpg', 1),
(409, 'RIKOMATE', '220121-141431mar7506192508407-00-CH1200Wx1200H.jpg', 1),
(410, 'LEGAL 200 GRS CAJA', '220121-163523marLEGAL 200GRS.JPG', 1),
(411, 'LEGAL 400 GRS CAJA', '220121-164931mar220121-163523marLEGAL 200GRS.jpg', 1),
(412, 'LEGAL 400GRS PZ', '220121-165005mar220121-163523marLEGAL 200GRS.jpg', 1),
(413, 'LEGAL 200GRS PZ', '220121-165019mar220121-163523marLEGAL 200GRS.jpg', 1),
(414, 'JUMEX 450 ML VARIOS SABORES', '220210-141834marJUMEX 450 ML.jfif', 0),
(415, 'DOG CHOW CACH RAZAS GDES VERDE', '220210-142018marDOG CHOW CACHORRO RAZAS PEQUEÑAS.jpg', 1),
(416, 'CERILLO', '', 0),
(417, 'MARUCHAN HABANERO ', '220224-120947mardownload.jpg', 1),
(418, 'GLEZ', '220503-131546mardownload.png', 1),
(419, 'SERV. ECOKING', '220225-120042mardownload.jpg', 1),
(420, 'SERV. ECOKING PZ', '220225-121733mardownload.jpg', 1),
(421, 'ESPERANZA EXTRA', '220610-133516mardownload.jpg', 1),
(422, 'YEMA DORADA', '220317-143140mardownload.jpg', 1),
(423, 'REGIO RINDE + 400 16/4', '220407-134512mardownload.jpg', 1),
(424, 'RENACIMIENTO ', '220413-143103mardownload.jpg', 1),
(425, 'SALO', '', 1),
(426, 'GIGANTE EXTRA', '', 1),
(427, 'LA PAZ', '', 1),
(428, 'SAN JUAN- SEMI CRAFT', '', 1),
(429, 'VALAIS', '230125-125847mardownload.jpg', 1),
(430, 'GIGANTES ', '220715-115117mardownload.jpg', 1),
(431, 'GGL', '', 1),
(432, 'CAT-CHOW 20KGS', '220816-113433marimages.jpg', 1),
(433, 'CERILLOS FLAMA', '220816-113608mardownload.jpg', 1),
(434, 'NESCAFE 350GRS', '220816-115518mardownload.jpg', 1),
(435, 'CHIPOTLES 1.97 KGS ', '220816-143407mardownload.jpg', 1),
(436, 'CHIPOTLE 1.97 KG', '220817-143551mardownload.jpg', 1),
(437, 'ARROZ TIA CATA 25KGS', '220818-113909mardownload.jpg', 1),
(438, ' MARLBORO ROJO 20/10CJ PAQUETE', '220819-160016mardownload.jpg', 1),
(439, 'CHOCOMILK SOBRE', '220823-093105mardownload.jpg', 1),
(440, 'SAN JUAN-EXTRA', '', 1),
(441, 'SANTA CLARA ENTERA LT', '220824-113118marsanta clara.jpg', 1),
(442, 'SANTA CLARA DESLACTOSADA LT', '220825-124518mar371417_image.png', 1),
(443, 'GIGANTES SUCIO', '', 1),
(444, 'DELICIAS', '220912-111127mardownload.jpg', 1),
(445, 'CHAPETES GATO 15KGS', '220912-112213mardownload.jpg', 1),
(446, 'SAN JUAN LINEA', '', 1),
(447, 'FRIJOL MICHIGAN 25 KG', '220917-142843marMICHIGAN-1.jpg', 1),
(448, 'ARROZ BUENO 25KGS', '221001-164923mardownload.jpg', 1),
(449, 'MANTECA VEGETAL COLON 3.24 KG', '221015-121337marimagen colon.jpg', 1),
(450, 'MARAVILLA 800 ML', '221015-121818marMARAVILLA 800ML.jpg', 1),
(451, 'McCORMICK 2.8GRS CAJA', '221023-080840mar190712-120141catdescarga (4).jpg', 1),
(452, 'McCORMICK 2.8GRS PZ', '221023-080821mar190712-120141catdescarga (4).jpg', 1),
(453, 'OJAI', '', 1),
(454, 'TRIGON', '', 1),
(455, 'CLEMENTE JACQUES GALON PZ', '221111-130119mardownload-2 copia.jpg', 1),
(456, 'CLEMENTE JACQUES LT CAJA', '221111-130149mardownload-3.jpg', 1),
(457, 'BARRILITO BCO. 3.9LTS PZ', '221111-130235mardownload-1 copia 2.jpg', 1),
(458, 'BARRILITO BCO. 750ML CAJA', '221111-130311mardownload copia 2.jpg', 1),
(459, 'VALENTINA NEGRA 24/370ML', '221111-130401mardownload copia.jpg', 1),
(460, 'FABULOSO NARANJA LT', '221111-132907marimages.jpg', 0),
(461, 'FABULOSO LAVANDA LT', '221111-130440mardownload-1.jpg', 1),
(462, 'FABULOSO MAR FRESCO LT', '221111-130506mardownload-2.jpg', 1),
(463, 'BOTANERA 4LT PZ', '221111-131933mardownload-1 copia.jpg', 1),
(464, 'FABULOSO NARANJA LT ', '221111-133215mardownload.jpg', 1),
(465, 'MARAVILLA GALON 3.78LT CAJA ', '221114-170221mardownload.jpg', 1),
(466, 'MARAVILLA GALON 3.78LT PZ', '221114-170249mardownload.jpg', 1),
(467, 'BARRILITO BCO. 3.9LTS CAJA ', '221115-114703mar221111-130235mardownload-1 copia 2.jpg', 1),
(468, 'CLEMENTE JACQUES GALON CAJA', '221115-115203mar221111-130119mardownload-2 copia.jpg', 1),
(469, 'CAFE LEGAL 12GRS TIRA', '221117-113949mardownload.jpg', 1),
(470, 'HORCHATA CHONTAL ', '221118-113332mardownload.jpg', 1),
(471, 'CLEMENTE JACQUES LT PZ', '221123-161430mar221111-130149mardownload-3.jpg', 1),
(472, 'BARRILITO BCO. 750ML PZ', '221123-161525mar221111-130311mardownload copia 2.jpg', 1),
(473, 'CHAPETES 20 KG', '221126-124006marCHAPETES 20.jpg', 1),
(474, 'FINA 10KGS', '221214-152021mar190729-103209mardescarga (1).png', 1),
(475, 'PAPEL BELA ', '221229-154833marimages.jpg', 1),
(476, 'MAXIMA 250HJ', '221229-154859mardownload.jpg', 1),
(477, 'MAXIMA 250HJ PZ', '221229-155613mardownload.jpg', 1),
(478, 'PILONCILLO', '230131-151532mardownload.jpg', 1),
(479, 'MARAVILLA 946ML', '230207-110600mar0006679_aceite-vegetal-maravilla-12-botellas-de-1-l_510.jpg', 1),
(480, 'OSO BLANCO 20KGS', '230216-115348mardownload.jpg', 1),
(481, 'ACEITE AVE 3LT CAJA ', '230216-120728mardownload-1.jpg', 1),
(482, 'ACEITE AVE 3LT PZ', '230216-120852mardownload-1.jpg', 1),
(483, 'CHOCOMILK BOLSA 350GRS', '230216-155901mardownload.jpg', 1),
(484, 'SAL BAHIA 25 KG', '230314-090628mardownload.jpg', 1),
(485, 'TRIGON EXTRA', '', 1),
(486, 'TRIGON EXTRA', '', 0),
(487, 'BUENDIA', '', 1),
(488, 'HUEVO BUENDIA', '', 0),
(489, 'HUEVO BUEN DIA', '', 0),
(490, 'HUEVO BUEN DIA', '', 0),
(491, 'HUEVO BUEN DIA', '', 0),
(492, 'HUEVO BUEN DIA', '', 0),
(493, 'MAIZ', '', 1),
(494, 'BULTO DE MAIZ', '', 1),
(495, 'SAL MARFIL 50KGS', '', 1),
(496, 'SAL MARFIL 50KGS', '', 1),
(497, 'ARROZ ROJO TOMATE C/10', '', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu`
--

DROP TABLE IF EXISTS `menu`;
CREATE TABLE IF NOT EXISTS `menu` (
  `MenuId` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(45) DEFAULT NULL,
  `Icon` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`MenuId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `menu`
--

INSERT INTO `menu` (`MenuId`, `Nombre`, `Icon`) VALUES
(1, 'Catálogos', 'fa fa-book'),
(2, 'Operaciones', 'fa fa-folder-open'),
(3, 'Configuración\n', 'fa fa fa-cogs');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu_sub`
--

DROP TABLE IF EXISTS `menu_sub`;
CREATE TABLE IF NOT EXISTS `menu_sub` (
  `MenusubId` int(11) NOT NULL AUTO_INCREMENT,
  `MenuId` int(11) NOT NULL,
  `Nombre` varchar(50) DEFAULT NULL,
  `Pagina` varchar(120) DEFAULT NULL,
  `Icon` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`MenusubId`),
  KEY `fk_menu_sub_menu_idx` (`MenuId`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `menu_sub`
--

INSERT INTO `menu_sub` (`MenusubId`, `MenuId`, `Nombre`, `Pagina`, `Icon`) VALUES
(1, 1, 'Productos', 'Productos', 'fa fa-barcode'),
(2, 3, 'Categoria', 'Categoria', 'fa fa-cogs'),
(3, 1, 'Personal', 'Personal', 'fa fa-group'),
(4, 2, 'Ventas', 'Ventas', 'fa fa-shopping-cart'),
(5, 2, 'Compras', 'Compras', 'fa fa-cogs'),
(6, 1, 'Clientes', 'Clientes', 'fa fa-user'),
(7, 1, 'Proveedores', 'Proveedores', 'fa fa-truck'),
(8, 2, 'Corte de caja', 'Corte_caja', 'fa fa-cogs'),
(9, 2, 'Lista de ventas', 'ListaVentas', 'fa fa-cogs'),
(10, 2, 'Turno', 'Turno', 'fa fa-cogs'),
(11, 2, 'Lista de turnos', 'ListaTurnos', 'fa fa-cogs'),
(12, 2, 'Lista de compras', 'Listacompras', 'fa fa-shopping-cart'),
(13, 3, 'Config. de ticket', 'Config_ticket', 'fa fa-cogs'),
(14, 2, 'Lista Facturadas', 'Lista_facturadas', 'fa fa-cogs'),
(15, 2, 'Mermas', 'Mermas', 'fa fa-edit'),
(16, 2, 'Gastos', 'Gastos', 'fa fa-usd'),
(17, 2, 'Gastos', 'Gastos', 'fa fa-usd'),
(18, 2, 'Mermas', 'Mermas', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mermas`
--

DROP TABLE IF EXISTS `mermas`;
CREATE TABLE IF NOT EXISTS `mermas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_producto` int(11) NOT NULL,
  `tipo_merma` int(11) NOT NULL,
  `presentacion` int(11) NOT NULL,
  `bodega` int(11) NOT NULL,
  `cantidad` decimal(10,2) NOT NULL,
  `motivo` text COLLATE utf8_spanish_ci NOT NULL,
  `fecha_reg` datetime NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `estatus` varchar(1) COLLATE utf8_spanish_ci NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `metodopago`
--

DROP TABLE IF EXISTS `metodopago`;
CREATE TABLE IF NOT EXISTS `metodopago` (
  `metodoId` int(11) NOT NULL AUTO_INCREMENT,
  `metodo` varchar(100) NOT NULL,
  `activo` int(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`metodoId`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `metodopago`
--

INSERT INTO `metodopago` (`metodoId`, `metodo`, `activo`) VALUES
(1, 'Efectivo', 1),
(2, 'Tarjeta de débito / crédito', 1),
(3, 'Transferencia bancaria', 1),
(4, 'Cheque nominativo', 1),
(5, 'Crédito', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notas`
--

DROP TABLE IF EXISTS `notas`;
CREATE TABLE IF NOT EXISTS `notas` (
  `id_nota` int(1) NOT NULL AUTO_INCREMENT,
  `mensaje` text NOT NULL,
  `usuario` varchar(120) NOT NULL,
  `bodega` int(1) NOT NULL,
  `reg` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id_nota`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `notas`
--

INSERT INTO `notas` (`id_nota`, `mensaje`, `usuario`, `bodega`, `reg`) VALUES
(1, '<p>norberto lopez</p>\n', 'Eduardo', 1, '2019-01-07 15:33:11'),
(2, '<p>notas</p>\n', 'Administrador', 2, '2018-06-22 16:53:52'),
(3, '<p>notas</p>\n', 'Administrador', 3, '2018-06-22 16:53:52');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pagos_ventas`
--

DROP TABLE IF EXISTS `pagos_ventas`;
CREATE TABLE IF NOT EXISTS `pagos_ventas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_venta` int(11) NOT NULL,
  `folio` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `monto` decimal(6,2) NOT NULL,
  `fecha` date NOT NULL,
  `fecha_reg` datetime NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `estatus` varchar(1) COLLATE utf8_spanish_ci NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `venta_fk_pagos` (`id_venta`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `pagos_ventas`
--

INSERT INTO `pagos_ventas` (`id`, `id_venta`, `folio`, `monto`, `fecha`, `fecha_reg`, `id_usuario`, `estatus`) VALUES
(1, 3, 'PAGO01', '100.00', '2023-10-13', '2023-10-13 16:39:54', 1, '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `perfiles`
--

DROP TABLE IF EXISTS `perfiles`;
CREATE TABLE IF NOT EXISTS `perfiles` (
  `perfilId` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`perfilId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `perfiles`
--

INSERT INTO `perfiles` (`perfilId`, `nombre`) VALUES
(1, 'Administrador'),
(2, 'Personal'),
(3, 'Residentes');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `perfiles_detalles`
--

DROP TABLE IF EXISTS `perfiles_detalles`;
CREATE TABLE IF NOT EXISTS `perfiles_detalles` (
  `Perfil_detalleId` int(11) NOT NULL AUTO_INCREMENT,
  `perfilId` int(11) NOT NULL,
  `MenusubId` int(11) NOT NULL,
  PRIMARY KEY (`Perfil_detalleId`),
  KEY `fk_Perfiles_detalles_Perfiles1_idx` (`perfilId`),
  KEY `fk_Perfiles_detalles_menu_sub1_idx` (`MenusubId`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `perfiles_detalles`
--

INSERT INTO `perfiles_detalles` (`Perfil_detalleId`, `perfilId`, `MenusubId`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 4),
(5, 1, 5),
(6, 2, 4),
(7, 2, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personal`
--

DROP TABLE IF EXISTS `personal`;
CREATE TABLE IF NOT EXISTS `personal` (
  `personalId` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(300) NOT NULL,
  `apellidos` varchar(300) NOT NULL,
  `fechanacimiento` date NOT NULL,
  `sexo` int(1) NOT NULL,
  `domicilio` varchar(500) NOT NULL,
  `ciudad` varchar(120) NOT NULL,
  `estado` int(11) NOT NULL,
  `codigopostal` int(5) NOT NULL,
  `telefono` varchar(15) NOT NULL,
  `celular` varchar(15) NOT NULL,
  `correo` varchar(50) NOT NULL,
  `turno` int(1) NOT NULL,
  `fechaingreso` date NOT NULL,
  `fechabaja` date NOT NULL,
  `sueldo` decimal(10,2) NOT NULL,
  `bodega` int(1) NOT NULL,
  `tipo` int(1) NOT NULL DEFAULT 1 COMMENT '0 administrador 1 normal',
  `estatus` int(1) NOT NULL DEFAULT 1 COMMENT '1 visible 0 eliminado',
  PRIMARY KEY (`personalId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `personal`
--

INSERT INTO `personal` (`personalId`, `nombre`, `apellidos`, `fechanacimiento`, `sexo`, `domicilio`, `ciudad`, `estado`, `codigopostal`, `telefono`, `celular`, `correo`, `turno`, `fechaingreso`, `fechabaja`, `sueldo`, `bodega`, `tipo`, `estatus`) VALUES
(1, 'Administrador', 'B1', '0000-00-00', 0, '', '', 0, 0, '', '', '', 0, '0000-00-00', '0000-00-00', '0.00', 1, 0, 1),
(2, 'jesus', 'paq', '0000-00-00', 1, '', '', 1, 0, '', '', '', 1, '0000-00-00', '0000-00-00', '0.00', 1, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personal_menu`
--

DROP TABLE IF EXISTS `personal_menu`;
CREATE TABLE IF NOT EXISTS `personal_menu` (
  `personalmenuId` int(11) NOT NULL AUTO_INCREMENT,
  `personalId` int(11) NOT NULL,
  `MenuId` int(11) NOT NULL,
  PRIMARY KEY (`personalmenuId`),
  KEY `personal_fkpersona` (`personalId`),
  KEY `personal_fkmenu` (`MenuId`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `personal_menu`
--

INSERT INTO `personal_menu` (`personalmenuId`, `personalId`, `MenuId`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 4),
(5, 1, 5),
(6, 1, 6),
(7, 1, 7),
(8, 1, 8),
(9, 1, 9),
(10, 1, 10),
(11, 1, 11),
(12, 1, 12),
(13, 1, 13),
(14, 1, 14),
(15, 1, 16),
(16, 1, 15),
(17, 2, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `presentaciones`
--

DROP TABLE IF EXISTS `presentaciones`;
CREATE TABLE IF NOT EXISTS `presentaciones` (
  `presentacionId` int(11) NOT NULL AUTO_INCREMENT,
  `presentacion` varchar(100) NOT NULL,
  `unidad` float NOT NULL,
  `activo` int(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`presentacionId`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `presentaciones`
--

INSERT INTO `presentaciones` (`presentacionId`, `presentacion`, `unidad`, `activo`) VALUES
(1, 'CAJA', 1, 0),
(2, '1/2 CAJA', 0.5, 0),
(3, '12 CONOS', 0.0833333, 0),
(4, '10 CONOS', 0.1, 0),
(5, 'BULTO', 1, 0),
(6, 'CAJA 1LT', 1, 0),
(7, 'CAJA 1/2LT', 1, 0),
(8, 'BIDON 20 LT', 1, 0),
(9, 'CAJA DE 5 LT', 1, 0),
(10, '4 PZA DE GALON', 4, 0),
(11, 'CUBETA 20 LT', 1, 0),
(13, 'ADULTO', 1, 0),
(14, 'CACHORRO', 1, 0),
(15, 'ADULTO ROJO', 1, 0),
(16, 'BULTO 20KG', 1, 0),
(17, 'BULTO 50KG', 1, 0),
(18, 'CAJA 10 PZ', 1, 0),
(19, '6 PIEZA', 1, 0),
(20, 'BULTO 10 KG', 1, 0),
(21, 'BULTO 44 KG', 1, 0),
(22, 'CAJA 6/3', 1, 0),
(23, 'PIEZAS 3KG', 1, 0),
(24, 'CAJA 24/380', 1, 0),
(25, 'CAJA 40/200', 1, 0),
(26, '40/100', 1, 0),
(27, '48/200', 1, 0),
(28, 'CUBETA', 1, 0),
(29, 'BULTO 25 Kg', 1, 0),
(30, 'CAJA', 1, 1),
(31, '1 PIEZA CAJA', 0.0833333, 0),
(32, 'PIEZA', 1, 1),
(33, '1/2 CAJA', 0.5, 1),
(34, 'CONO', 1, 1),
(35, 'CUBETA ', 1, 1),
(36, 'BULTO', 1, 1),
(37, 'PZA 1/24', 0.0416, 1),
(38, 'PZA 1/12', 0.0833, 1),
(39, 'PZA 1/8', 0.125, 1),
(40, 'PZA 1/6', 0.1666, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

DROP TABLE IF EXISTS `productos`;
CREATE TABLE IF NOT EXISTS `productos` (
  `productoid` int(11) NOT NULL AUTO_INCREMENT,
  `codigo` varchar(20) NOT NULL,
  `productofiscal` int(1) NOT NULL COMMENT '0 no 1 si',
  `nombre` varchar(120) NOT NULL,
  `descripcion` text NOT NULL,
  `categoria` int(11) NOT NULL,
  `stock` int(11) NOT NULL,
  `preciocompra` decimal(10,2) NOT NULL,
  `porcentaje` int(11) NOT NULL,
  `precioventa` decimal(10,2) NOT NULL,
  `mediomayoreo` decimal(10,2) NOT NULL,
  `canmediomayoreo` int(11) NOT NULL,
  `mayoreo` decimal(10,2) NOT NULL,
  `canmayoreo` int(11) NOT NULL,
  `img` varchar(120) NOT NULL,
  `activo` int(1) NOT NULL DEFAULT 1 COMMENT '1 actual 0 eliminado',
  `reg` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`productoid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedores`
--

DROP TABLE IF EXISTS `proveedores`;
CREATE TABLE IF NOT EXISTS `proveedores` (
  `id_proveedor` int(11) NOT NULL AUTO_INCREMENT,
  `razon_social` varchar(50) NOT NULL,
  `domicilio` varchar(30) NOT NULL,
  `ciudad` varchar(25) NOT NULL,
  `cp` varchar(8) NOT NULL,
  `id_estado` int(11) NOT NULL,
  `telefono_local` varchar(10) NOT NULL,
  `telefono_celular` varchar(10) NOT NULL,
  `contacto` varchar(100) NOT NULL,
  `email_contacto` varchar(60) NOT NULL,
  `rfc` varchar(13) NOT NULL,
  `fax` varchar(20) NOT NULL,
  `obser` text NOT NULL,
  `activo` int(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id_proveedor`),
  KEY `constraint_fk_27` (`id_estado`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `proveedores`
--

INSERT INTO `proveedores` (`id_proveedor`, `razon_social`, `domicilio`, `ciudad`, `cp`, `id_estado`, `telefono_local`, `telefono_celular`, `contacto`, `email_contacto`, `rfc`, `fax`, `obser`, `activo`) VALUES
(1, 'PROVEE DE PRUEBA', '', '', '', 21, '', '', '', '', '', '', '', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sproducto`
--

DROP TABLE IF EXISTS `sproducto`;
CREATE TABLE IF NOT EXISTS `sproducto` (
  `productoaddId` int(11) NOT NULL AUTO_INCREMENT,
  `productoId` int(11) NOT NULL,
  `MarcaId` int(11) NOT NULL,
  `precompra` float NOT NULL,
  `activo` int(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`productoaddId`),
  KEY `producto_fk_marca` (`MarcaId`),
  KEY `producto_fk_categoria` (`productoId`)
) ENGINE=InnoDB AUTO_INCREMENT=557 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sproducto`
--

INSERT INTO `sproducto` (`productoaddId`, `productoId`, `MarcaId`, `precompra`, `activo`) VALUES
(1, 2, 1, 400, 0),
(2, 2, 1, 0, 0),
(3, 1, 2, 22, 0),
(4, 4, 3, 0, 0),
(5, 4, 3, 0, 0),
(6, 1, 4, 0, 0),
(7, 3, 5, 0, 0),
(8, 3, 6, 0, 0),
(9, 3, 7, 0, 0),
(10, 1, 18, 20, 0),
(11, 1, 23, 17.5, 0),
(12, 1, 24, 15.5, 0),
(13, 1, 10, 26.5, 0),
(14, 1, 12, 32, 0),
(15, 1, 19, 26, 0),
(16, 1, 21, 27.5, 0),
(17, 1, 11, 30.3, 0),
(18, 1, 20, 25.3, 0),
(19, 1, 22, 18.5, 0),
(20, 1, 9, 17.5, 0),
(21, 1, 200, 20, 0),
(22, 1, 201, 65, 1),
(23, 1, 202, 380, 1),
(24, 1, 203, 390, 1),
(25, 1, 204, 24.5, 0),
(26, 1, 206, 0, 0),
(27, 1, 205, 0, 0),
(28, 1, 207, 35.8, 0),
(29, 1, 209, 0, 0),
(30, 1, 208, 0, 0),
(31, 5, 25, 483.6, 0),
(32, 5, 26, 513.6, 1),
(33, 5, 27, 848, 1),
(34, 5, 28, 916, 0),
(35, 5, 29, 807, 1),
(36, 5, 30, 201.75, 1),
(37, 5, 210, 230.29, 0),
(38, 5, 74, 300, 0),
(39, 5, 73, 516.03, 1),
(40, 5, 72, 334.95, 0),
(41, 5, 31, 248, 0),
(42, 11, 85, 1520, 1),
(43, 6, 48, 584, 1),
(44, 6, 49, 0, 0),
(45, 6, 33, 753.9, 0),
(46, 6, 34, 690.5, 0),
(47, 6, 41, 447, 0),
(48, 6, 42, 488, 0),
(49, 6, 43, 683.9, 0),
(50, 6, 44, 615.8, 0),
(51, 6, 40, 410, 0),
(52, 6, 51, 359.8, 0),
(53, 6, 36, 970, 1),
(54, 6, 37, 905, 1),
(55, 6, 38, 950, 1),
(56, 6, 32, 515.71, 1),
(57, 6, 35, 471.5, 0),
(58, 6, 39, 0, 0),
(59, 6, 45, 285.5, 0),
(60, 6, 46, 360.9, 0),
(61, 6, 47, 260, 0),
(62, 6, 50, 285.5, 0),
(63, 6, 52, 323.9, 0),
(64, 7, 53, 540, 0),
(65, 7, 54, 395, 0),
(66, 7, 55, 831.1, 1),
(67, 7, 56, 430.8, 0),
(68, 8, 57, 340.1, 1),
(69, 8, 62, 335, 1),
(70, 8, 59, 340.17, 1),
(71, 8, 58, 331.7, 1),
(72, 8, 63, 337.85, 1),
(73, 8, 64, 336.5, 1),
(74, 8, 66, 337.85, 1),
(75, 8, 65, 346.71, 1),
(76, 8, 67, 292.7, 0),
(77, 8, 69, 292.7, 0),
(78, 8, 68, 305.6, 0),
(79, 8, 70, 322, 0),
(80, 8, 71, 246.87, 0),
(81, 9, 75, 121.85, 1),
(82, 9, 76, 121.1, 1),
(83, 9, 9, 125, 0),
(84, 9, 78, 97.84, 0),
(85, 10, 81, 270.3, 0),
(86, 10, 82, 46.58, 0),
(87, 10, 211, 232.52, 0),
(88, 10, 212, 19.38, 0),
(89, 10, 83, 335.8, 1),
(90, 10, 84, 221.9, 0),
(91, 12, 87, 330.5, 1),
(92, 12, 86, 293, 1),
(93, 12, 88, 150, 0),
(94, 12, 89, 153, 1),
(95, 13, 95, 248.65, 0),
(96, 13, 93, 110, 0),
(97, 13, 94, 275, 0),
(98, 13, 92, 157.79, 0),
(99, 13, 91, 500, 0),
(100, 13, 90, 279.72, 1),
(101, 13, 97, 270.45, 0),
(102, 13, 96, 350.78, 1),
(103, 14, 167, 119.4, 1),
(104, 15, 98, 343.8, 0),
(105, 15, 99, 34.1, 0),
(106, 15, 100, 1746.42, 1),
(107, 15, 101, 90, 1),
(108, 16, 104, 128.25, 1),
(109, 16, 102, 551.85, 1),
(110, 16, 103, 555.97, 1),
(111, 17, 105, 249.8, 0),
(112, 17, 106, 432, 0),
(113, 18, 108, 1538.16, 1),
(114, 18, 107, 430, 0),
(115, 18, 109, 440, 0),
(116, 19, 110, 185.5, 0),
(117, 19, 111, 492.58, 0),
(118, 19, 116, 298.5, 1),
(119, 19, 115, 395, 0),
(120, 19, 112, 258.75, 0),
(121, 19, 113, 212.2, 1),
(122, 20, 117, 245, 0),
(123, 20, 118, 221, 0),
(124, 20, 124, 239, 1),
(125, 20, 125, 239, 1),
(126, 20, 121, 195.85, 1),
(127, 20, 122, 140.43, 0),
(128, 20, 119, 135, 0),
(129, 20, 120, 208.35, 1),
(130, 20, 126, 126, 0),
(131, 20, 127, 109.88, 0),
(132, 21, 129, 339.33, 0),
(133, 21, 130, 339.32, 0),
(134, 21, 213, 352.8, 0),
(135, 21, 214, 152.83, 0),
(136, 21, 215, 340.3, 0),
(137, 21, 128, 40.53, 0),
(138, 22, 132, 166.53, 1),
(139, 22, 158, 71.87, 1),
(140, 22, 131, 60.28, 1),
(141, 23, 216, 112.55, 0),
(142, 23, 133, 326.68, 1),
(143, 23, 135, 450.45, 1),
(144, 23, 134, 100, 1),
(145, 24, 217, 270, 0),
(146, 25, 136, 169, 0),
(147, 26, 139, 550.98, 0),
(148, 26, 137, 345, 0),
(149, 26, 138, 572.9, 1),
(150, 26, 140, 383.7, 1),
(151, 26, 141, 1061, 1),
(152, 26, 218, 268.05, 1),
(153, 27, 143, 475.3, 1),
(154, 27, 144, 838, 0),
(155, 27, 145, 312.7, 0),
(156, 27, 146, 1019.91, 1),
(157, 27, 147, 316, 0),
(158, 27, 219, 48.11, 0),
(159, 27, 148, 417, 1),
(160, 27, 149, 514, 1),
(161, 27, 150, 339.83, 0),
(162, 27, 151, 411.81, 1),
(163, 27, 341, 279.13, 0),
(164, 27, 153, 437, 0),
(165, 27, 241, 68, 1),
(166, 27, 220, 103.33, 1),
(167, 27, 146, 361.2, 0),
(168, 27, 156, 361.2, 0),
(169, 27, 157, 599, 1),
(170, 28, 159, 1470.84, 1),
(171, 28, 221, 61.25, 1),
(172, 29, 160, 39.79, 0),
(173, 30, 161, 1144, 1),
(174, 30, 162, 896, 1),
(175, 30, 222, 65, 0),
(176, 31, 164, 396, 0),
(177, 31, 163, 432.8, 0),
(178, 32, 166, 692, 0),
(179, 32, 223, 661.5, 1),
(180, 32, 165, 679, 0),
(181, 32, 224, 625.2, 1),
(182, 33, 225, 362.25, 0),
(183, 33, 226, 405.83, 0),
(184, 33, 168, 286.83, 0),
(185, 33, 227, 270.5, 0),
(186, 33, 169, 374.5, 0),
(187, 33, 228, 374.5, 0),
(188, 33, 170, 419.37, 0),
(189, 33, 229, 34.83, 0),
(190, 33, 171, 411.33, 0),
(191, 33, 230, 0, 0),
(192, 33, 231, 30.12, 0),
(193, 33, 173, 174.5, 0),
(194, 34, 174, 70.12, 0),
(195, 35, 175, 451, 0),
(196, 35, 176, 445.5, 0),
(197, 36, 177, 29.75, 1),
(198, 37, 179, 111.7, 0),
(199, 37, 178, 174.08, 0),
(200, 38, 180, 129.3, 0),
(201, 39, 181, 78.8, 0),
(202, 39, 181, 120, 0),
(203, 41, 186, 338.75, 0),
(204, 42, 187, 153.3, 0),
(205, 42, 188, 107.7, 0),
(206, 43, 189, 118.2, 0),
(207, 44, 191, 239.9, 0),
(208, 45, 192, 12.94, 0),
(209, 52, 193, 211.2, 0),
(210, 52, 194, 211.2, 0),
(211, 52, 195, 185, 1),
(212, 52, 196, 185, 1),
(213, 52, 197, 151.38, 1),
(214, 52, 198, 151.38, 1),
(215, 53, 199, 814.08, 1),
(216, 40, 182, 399.2, 0),
(217, 40, 232, 102.5, 0),
(218, 40, 183, 468.96, 0),
(219, 40, 233, 114.97, 0),
(220, 40, 184, 352.21, 0),
(221, 40, 184, 338.17, 0),
(222, 40, 184, 338.17, 0),
(223, 40, 234, 84.54, 0),
(224, 40, 234, 338.17, 0),
(225, 20, 123, 110.59, 0),
(226, 1, 236, 18.5, 0),
(227, 21, 237, 58, 0),
(228, 1, 238, 21.5, 0),
(229, 6, 239, 277.5, 0),
(230, 27, 240, 591.74, 1),
(231, 9, 247, 224.5, 0),
(232, 57, 243, 188.76, 0),
(233, 9, 248, 199.9, 0),
(234, 9, 249, 199.9, 0),
(235, 9, 250, 199.9, 0),
(236, 57, 244, 188.76, 0),
(237, 57, 245, 188.76, 0),
(238, 57, 246, 216.5, 0),
(239, 25, 251, 84.12, 0),
(240, 25, 252, 101.34, 0),
(241, 16, 253, 86.66, 0),
(242, 16, 254, 160.89, 0),
(243, 16, 257, 128.94, 1),
(244, 30, 259, 190.66, 1),
(245, 30, 258, 143.33, 1),
(246, 28, 260, 574.8, 0),
(247, 28, 261, 47.9, 0),
(248, 1, 263, 23.3, 0),
(249, 19, 114, 173.5, 0),
(250, 8, 264, 280.72, 0),
(251, 20, 265, 146, 0),
(252, 20, 265, 0, 0),
(253, 60, 266, 171.56, 0),
(254, 27, 267, 339.77, 1),
(255, 27, 268, 316, 0),
(256, 8, 287, 283.27, 0),
(257, 8, 285, 281.6, 0),
(258, 8, 286, 283.2, 0),
(259, 8, 288, 243.47, 0),
(260, 8, 289, 245.9, 0),
(261, 57, 290, 269, 0),
(262, 57, 291, 258, 0),
(263, 57, 292, 245, 0),
(264, 57, 293, 231.76, 0),
(265, 21, 270, 327.32, 0),
(266, 21, 269, 325.36, 0),
(267, 21, 271, 339.93, 0),
(268, 21, 272, 339.33, 0),
(269, 21, 273, 208.9, 0),
(270, 21, 274, 208.9, 0),
(271, 21, 275, 313.25, 0),
(272, 21, 276, 346.37, 0),
(273, 21, 277, 270.6, 0),
(274, 21, 278, 270.6, 0),
(275, 21, 279, 270.6, 0),
(276, 21, 280, 270.6, 0),
(277, 21, 281, 270.6, 0),
(278, 21, 282, 270.6, 0),
(279, 21, 283, 311.6, 0),
(280, 21, 284, 287.1, 0),
(281, 8, 294, 300.9, 0),
(282, 8, 295, 302.43, 0),
(283, 5, 296, 320, 0),
(284, 21, 297, 339.32, 0),
(285, 21, 298, 224.35, 0),
(286, 21, 299, 339.32, 0),
(287, 15, 300, 332, 0),
(288, 15, 300, 33.2, 0),
(289, 6, 301, 420, 1),
(290, 6, 303, 375, 0),
(291, 39, 304, 78.8, 0),
(292, 1, 305, 22, 0),
(293, 1, 306, 25, 0),
(294, 1, 308, 34.5, 1),
(295, 1, 309, 28.5, 0),
(296, 1, 310, 29, 0),
(297, 1, 311, 39.5, 0),
(298, 20, 312, 135.2, 0),
(299, 33, 313, 235.95, 0),
(300, 33, 314, 23.34, 0),
(301, 9, 315, 240.35, 0),
(302, 1, 316, 0, 0),
(303, 20, 118, 268.1, 1),
(304, 13, 317, 189.9, 0),
(305, 1, 318, 40, 0),
(306, 1, 319, 23, 0),
(307, 1, 320, 25, 0),
(308, 15, 321, 459.85, 0),
(309, 1, 322, 23.5, 0),
(310, 1, 323, 22, 0),
(311, 6, 324, 636, 0),
(312, 27, 325, 496.58, 1),
(313, 20, 126, 126, 0),
(314, 20, 327, 123.13, 0),
(315, 27, 329, 351, 0),
(316, 15, 330, 57.025, 0),
(317, 6, 331, 280.9, 0),
(318, 27, 332, 0, 0),
(319, 6, 337, 515, 0),
(320, 6, 336, 250, 0),
(321, 7, 335, 336.5, 0),
(322, 12, 338, 177, 0),
(323, 27, 150, 354, 0),
(324, 27, 340, 414, 0),
(325, 27, 341, 232, 0),
(326, 1, 342, 26, 0),
(327, 6, 344, 343, 0),
(328, 19, 112, 258.75, 0),
(329, 25, 346, 29.68, 0),
(330, 19, 347, 207, 1),
(331, 5, 348, 95, 1),
(332, 5, 349, 70, 1),
(333, 5, 74, 331.5, 0),
(334, 27, 350, 302.37, 1),
(335, 31, 351, 355, 0),
(336, 15, 99, 39.15, 0),
(337, 16, 353, 0, 0),
(338, 20, 354, 149.76, 0),
(339, 13, 352, 393.33, 0),
(340, 33, 356, 415, 0),
(341, 63, 353, 125.83, 0),
(342, 15, 98, 370.17, 0),
(343, 25, 357, 390, 0),
(344, 35, 358, 0, 0),
(345, 35, 359, 0, 0),
(346, 43, 360, 148.75, 0),
(347, 20, 361, 105.47, 0),
(348, 13, 91, 550, 1),
(349, 25, 357, 390, 0),
(350, 32, 362, 0, 1),
(351, 32, 362, 550.5, 0),
(352, 32, 362, 0, 0),
(353, 32, 363, 550.5, 0),
(354, 32, 362, 550.5, 0),
(355, 32, 364, 550.5, 0),
(356, 32, 365, 550.5, 0),
(357, 10, 81, 279.8, 0),
(358, 20, 127, 118, 0),
(359, 5, 367, 187.2, 0),
(360, 53, 366, 786.67, 0),
(361, 6, 49, 454, 0),
(362, 8, 368, 178, 0),
(363, 8, 369, 150, 0),
(364, 17, 370, 461.5, 0),
(365, 6, 371, 390, 0),
(366, 7, 372, 582.27, 1),
(367, 9, 373, 79, 0),
(368, 17, 374, 350, 1),
(369, 13, 375, 199, 1),
(370, 22, 376, 999.2, 1),
(371, 22, 378, 496, 0),
(372, 22, 378, 499.55, 0),
(373, 35, 379, 443.5, 0),
(374, 5, 380, 0, 1),
(375, 5, 380, 0, 1),
(376, 9, 78, 125, 0),
(377, 9, 381, 125, 1),
(378, 9, 381, 125, 0),
(379, 20, 382, 137.5, 0),
(380, 35, 383, 400, 0),
(381, 5, 380, 380, 1),
(382, 35, 359, 392, 0),
(383, 23, 385, 61.66, 0),
(384, 1, 386, 32.5, 0),
(385, 1, 387, 28.5, 0),
(386, 5, 388, 384, 0),
(387, 25, 389, 28.16, 0),
(388, 39, 390, 110, 0),
(389, 5, 391, 435, 0),
(390, 40, 183, 0, 0),
(391, 6, 33, 0, 0),
(392, 6, 35, 0, 0),
(393, 1, 393, 23, 0),
(394, 6, 394, 375, 0),
(395, 1, 395, 29.3, 0),
(396, 64, 396, 50, 0),
(397, 1, 397, 27.5, 0),
(398, 1, 398, 23, 0),
(399, 1, 399, 27, 0),
(400, 63, 400, 2.62, 0),
(401, 1, 401, 23.5, 0),
(402, 1, 402, 23, 0),
(403, 1, 403, 27, 0),
(404, 1, 405, 31.3, 0),
(405, 1, 406, 24.5, 0),
(406, 33, 407, 27.5, 1),
(407, 33, 408, 275, 1),
(408, 22, 409, 47.74, 0),
(409, 23, 410, 685.76, 0),
(410, 23, 411, 1582, 1),
(411, 23, 412, 65.91, 1),
(412, 23, 413, 28.58, 0),
(413, 65, 414, 113.3, 0),
(414, 6, 415, 811.19, 0),
(415, 6, 41, 572, 1),
(416, 6, 42, 476.6, 1),
(417, 39, 417, 168, 1),
(418, 6, 415, 927.06, 0),
(419, 1, 418, 33, 0),
(420, 33, 419, 210, 0),
(421, 33, 420, 17.5, 0),
(422, 13, 352, 422.5, 1),
(423, 1, 421, 33, 0),
(424, 1, 422, 33.5, 0),
(425, 20, 117, 270, 1),
(426, 1, 323, 35, 0),
(427, 19, 423, 425, 1),
(428, 1, 21, 31, 0),
(429, 1, 424, 30.5, 0),
(430, 1, 422, 31, 0),
(431, 5, 391, 446, 1),
(432, 1, 405, 30.5, 0),
(433, 1, 425, 31.2, 0),
(434, 1, 418, 32, 0),
(435, 1, 428, 36, 0),
(436, 1, 306, 33, 0),
(437, 1, 421, 31.2, 0),
(438, 20, 429, 130, 0),
(439, 1, 430, 33.5, 0),
(440, 1, 422, 36, 0),
(441, 1, 425, 32.5, 0),
(442, 1, 422, 31.5, 0),
(443, 1, 431, 32, 0),
(444, 1, 393, 0, 0),
(445, 1, 12, 32.5, 0),
(446, 15, 321, 56.625, 1),
(447, 19, 111, 531.8, 1),
(448, 19, 112, 279.41, 1),
(449, 1, 422, 32, 0),
(450, 6, 33, 796.6, 1),
(451, 6, 34, 889.8, 1),
(452, 6, 43, 0, 1),
(453, 6, 43, 710, 1),
(454, 6, 44, 0, 1),
(455, 6, 44, 0, 1),
(456, 6, 44, 690, 1),
(457, 6, 49, 592, 1),
(458, 1, 424, 33.7, 0),
(459, 5, 72, 458.35, 0),
(460, 7, 432, 1006.79, 1),
(461, 15, 433, 24.49, 1),
(462, 8, 70, 388.31, 1),
(463, 8, 71, 365.89, 1),
(464, 23, 434, 159.5, 1),
(465, 27, 436, 106.29, 1),
(466, 1, 10, 43, 0),
(467, 1, 204, 34, 0),
(468, 13, 437, 321.37, 1),
(469, 66, 438, 605, 1),
(470, 8, 67, 365.36, 0),
(471, 8, 68, 367.26, 0),
(472, 8, 69, 367.26, 0),
(473, 67, 189, 139.16, 0),
(474, 9, 381, 175.63, 1),
(475, 28, 439, 79.02, 1),
(476, 68, 213, 509.5, 1),
(477, 1, 440, 33.5, 0),
(478, 20, 441, 255.15, 0),
(479, 20, 442, 255.15, 0),
(480, 39, 181, 168, 1),
(481, 1, 428, 500, 1),
(482, 1, 393, 26, 1),
(483, 69, 444, 360, 0),
(484, 7, 445, 499, 1),
(485, 1, 446, 29.5, 0),
(486, 18, 447, 850, 1),
(487, 13, 448, 550, 1),
(488, 5, 450, 432, 1),
(489, 5, 449, 1070.4, 1),
(490, 35, 175, 520, 1),
(491, 35, 176, 500, 1),
(492, 26, 451, 942.28, 1),
(493, 26, 452, 235.5, 1),
(494, 1, 453, 33, 0),
(495, 1, 454, 35, 0),
(496, 70, 458, 126, 1),
(497, 70, 457, 32.62, 1),
(498, 70, 456, 132, 1),
(499, 70, 455, 34, 1),
(500, 10, 459, 355.9, 1),
(501, 9, 464, 308, 1),
(502, 9, 460, 0, 0),
(503, 9, 461, 311.1, 1),
(504, 9, 462, 308, 1),
(505, 10, 81, 414.79, 1),
(506, 10, 463, 69.13, 1),
(507, 5, 465, 613.87, 1),
(508, 5, 466, 153.5, 1),
(509, 70, 468, 136.11, 1),
(510, 70, 467, 205.3, 1),
(511, 23, 469, 314, 0),
(512, 36, 470, 46, 1),
(513, 1, 440, 0, 0),
(514, 70, 471, 11, 1),
(515, 70, 472, 9.33, 1),
(516, 1, 454, 42, 0),
(517, 6, 473, 339, 1),
(518, 1, 446, 31, 0),
(519, 12, 474, 130, 1),
(520, 19, 475, 528, 0),
(521, 33, 476, 252, 0),
(522, 33, 477, 14, 0),
(523, 20, 429, 136, 0),
(524, 1, 386, 35, 0),
(525, 22, 378, 512, 0),
(526, 11, 478, 22.5, 1),
(527, 1, 453, 43, 0),
(528, 5, 479, 483.6, 1),
(529, 12, 480, 190, 1),
(530, 28, 483, 34.51, 1),
(531, 5, 481, 520, 1),
(532, 5, 482, 130, 1),
(533, 12, 484, 153, 0),
(534, 20, 429, 138.6, 1),
(535, 1, 454, 26, 0),
(536, 1, 319, 28.5, 0),
(537, 1, 24, 28.5, 0),
(538, 1, 485, 25, 0),
(539, 1, 323, 34.5, 1),
(540, 1, 311, 29, 0),
(541, 1, 440, 27.5, 0),
(542, 1, 446, 30, 1),
(543, 1, 19, 28.5, 0),
(544, 1, 21, 28, 0),
(545, 1, 487, 28, 0),
(546, 20, 127, 135, 1),
(547, 18, 493, 345.77, 1),
(548, 12, 495, 369.9, 1),
(549, 22, 497, 30.35, 1),
(550, 1, 418, 34.5, 0),
(551, 1, 425, 34.5, 0),
(552, 1, 430, 28, 1),
(553, 1, 426, 26.5, 1),
(554, 1, 443, 22, 1),
(555, 1, 427, 28.5, 0),
(556, 70, 27, 141.96, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sproductosub`
--

DROP TABLE IF EXISTS `sproductosub`;
CREATE TABLE IF NOT EXISTS `sproductosub` (
  `subId` int(11) NOT NULL AUTO_INCREMENT,
  `productoaddId` int(11) NOT NULL,
  `PresentacionId` int(11) NOT NULL,
  `tipo` int(1) NOT NULL,
  `precio` float NOT NULL,
  `stok` float NOT NULL,
  `precio_mm` float NOT NULL,
  `cantidad_mm` float NOT NULL,
  `precio_m` float NOT NULL,
  `cantidad_m` float NOT NULL,
  `precio2` float NOT NULL,
  `stok2` float NOT NULL,
  `precio_mm2` float NOT NULL,
  `cantidad_mm2` float NOT NULL,
  `precio_m2` float NOT NULL,
  `cantidad_m2` float NOT NULL,
  `precio3` float NOT NULL,
  `stok3` float NOT NULL,
  `precio_mm3` float NOT NULL,
  `cantidad_mm3` float NOT NULL,
  `precio_m3` float NOT NULL,
  `cantidad_m3` float NOT NULL,
  PRIMARY KEY (`subId`)
) ENGINE=InnoDB AUTO_INCREMENT=849 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sproductosub`
--

INSERT INTO `sproductosub` (`subId`, `productoaddId`, `PresentacionId`, `tipo`, `precio`, `stok`, `precio_mm`, `cantidad_mm`, `precio_m`, `cantidad_m`, `precio2`, `stok2`, `precio_mm2`, `cantidad_mm2`, `precio_m2`, `cantidad_m2`, `precio3`, `stok3`, `precio_mm3`, `cantidad_mm3`, `precio_m3`, `cantidad_m3`) VALUES
(1, 1, 30, 1, 800, 65, 700, 10, 600, 20, 800, 65, 700, 10, 600, 20, 800, 50, 700, 10, 600, 20),
(3, 2, 32, 1, 20, 12, 20, 1, 20, 1, 20, 12, 20, 1, 20, 1, 20, 12, 20, 1, 20, 1),
(4, 3, 30, 1, 350, 100, 300, 10, 250, 15, 350, 100, 300, 10, 250, 15, 350, 100, 300, 10, 250, 15),
(5, 3, 33, 0, 200, 200, 150, 10, 100, 15, 200, 200, 150, 10, 100, 15, 200, 200, 150, 10, 100, 15),
(6, 4, 30, 1, 275, 50, 270, 5, 265, 10, 275, 50, 270, 5, 265, 10, 275, 50, 270, 5, 265, 10),
(7, 5, 32, 1, 23, 12, 23, 1, 23, 1, 23, 12, 23, 1, 23, 1, 23, 12, 23, 1, 23, 1),
(8, 6, 30, 1, 22, 50, 21.5, 5, 21, 10, 22, 50, 21.5, 5, 21, 10, 22, 50, 21.5, 5, 21, 10),
(9, 6, 33, 0, 22, 100, 22, 1, 22, 1, 22, 100, 22, 1, 22, 1, 22, 100, 22, 1, 22, 1),
(10, 7, 30, 1, 88, 100, 85, 5, 83, 10, 88, 100, 85, 5, 83, 10, 88, 100, 85, 5, 83, 10),
(11, 8, 30, 1, 88, 100, 85, 5, 83, 10, 88, 100, 85, 5, 83, 10, 88, 100, 85, 5, 83, 10),
(12, 9, 30, 1, 88, 100, 85, 5, 83, 10, 88, 100, 85, 5, 83, 10, 88, 100, 85, 5, 86, 10),
(13, 10, 30, 1, 28, 0, 28, 1, 28, 1, 28, 0, 28, 1, 28, 1, 28, 0, 28, 1, 28, 1),
(14, 10, 33, 0, 28, 0, 28, 1, 28, 1, 28, 0, 28, 1, 28, 1, 28, 0, 28, 1, 28, 1),
(15, 11, 30, 1, 25, 0, 25, 1, 25, 1, 25, 0, 25, 1, 25, 1, 25, 0, 25, 1, 25, 1),
(16, 11, 33, 0, 25, 0, 25, 1, 25, 1, 25, 0, 25, 1, 25, 1, 25, 0, 25, 1, 25, 1),
(17, 12, 30, 1, 24, 0, 24, 1, 24, 1, 24, 0, 24, 1, 24, 1, 24, 0, 24, 1, 23, 7),
(18, 12, 33, 0, 24, 0, 24, 1, 24, 1, 24, 0, 24, 1, 24, 1, 24, 0, 24, 1, 24, 1),
(19, 13, 30, 1, 32, 0, 32, 1, 30.5, 11.5, 32, 0, 32, 1, 32, 1, 32, 11.5, 32, 1, 30.5, 11.5),
(20, 13, 33, 0, 32, 0, 32, 1, 32, 1, 32, 0, 32, 1, 32, 1, 32, 23, 32, 1, 32, 1),
(21, 14, 30, 1, 33, 0, 33, 1, 33, 1, 34, 0, 34, 1, 33.1, 50, 33, 0, 33, 1, 33, 1),
(22, 14, 33, 0, 33, 0, 33, 1, 33, 1, 34, 0, 34, 1, 34, 1, 33, 0, 33, 1, 33, 1),
(23, 15, 30, 1, 25, 0, 25, 1, 25, 1, 25, 0, 25, 1, 25, 1, 25, 0, 25, 1, 25, 1),
(24, 15, 33, 0, 25, 0, 25, 1, 25, 1, 25, 0, 25, 1, 25, 1, 25, 0, 25, 1, 25, 1),
(25, 16, 30, 1, 30, 0, 30, 1, 30, 1, 30, 0, 30, 1, 30, 1, 30, 0, 30, 1, 30, 1),
(26, 16, 33, 0, 30, 0, 30, 1, 30, 1, 30, 0, 30, 1, 30, 1, 30, 0, 30, 1, 30, 1),
(27, 17, 30, 1, 38, 0, 38, 1, 38, 1, 38, 0, 38, 1, 38, 1, 38, 0, 38, 1, 38, 1),
(28, 17, 33, 0, 38, 0, 38, 1, 38, 1, 38, 0, 38, 1, 38, 1, 38, 0, 38, 1, 38, 1),
(29, 18, 30, 1, 29, 0, 29, 1, 29, 1, 29, 0, 29, 1, 29, 1, 29, 0, 29, 1, 29, 1),
(30, 18, 33, 0, 29, 0, 29, 1, 29, 1, 29, 0, 29, 1, 29, 1, 29, 0, 29, 1, 29, 1),
(31, 19, 30, 1, 28, 0, 28, 1, 28, 1, 28, 0, 28, 1, 28, 1, 28, 0, 28, 1, 28, 1),
(32, 19, 33, 0, 28, 0, 28, 1, 28, 1, 28, 0, 28, 1, 28, 1, 28, 0, 28, 1, 28, 1),
(33, 20, 30, 1, 27, 0, 27, 1, 27, 1, 27, 0, 27, 1, 27, 1, 27, 0, 27, 1, 26, 10),
(34, 20, 33, 0, 27, 0, 27, 1, 27, 1, 27, 0, 27, 1, 27, 1, 27, 0, 27, 1, 27, 1),
(35, 21, 30, 1, 38, 0, 38, 1, 38, 1, 38, 0, 38, 1, 38, 1, 38, 0, 38, 1, 38, 1),
(36, 21, 33, 0, 38, 0, 38, 1, 38, 1, 38, 0, 38, 1, 38, 1, 38, 0, 38, 1, 38, 1),
(37, 22, 34, 1, 70, 0, 70, 1, 70, 1, 80, 4, 80, 1, 80, 1, 70, 0, 70, 1, 70, 1),
(38, 23, 35, 1, 350, 13, 350, 1, 350, 1, 400, 13, 400, 1, 400, 1, 350, 13, 350, 1, 350, 1),
(39, 24, 30, 1, 400, 12, 400, 1, 400, 1, 400, 10, 400, 1, 400, 1, 400, 12, 400, 1, 400, 1),
(40, 24, 33, 0, 200, 24, 200, 1, 200, 1, 200, 20, 200, 1, 200, 1, 200, 24, 200, 1, 200, 1),
(41, 24, 34, 0, 60, 12, 60, 1, 60, 1, 60, 10, 60, 1, 60, 1, 60, 12, 60, 1, 60, 1),
(42, 25, 30, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(43, 25, 33, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(44, 26, 30, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(45, 26, 33, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(46, 27, 30, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(47, 27, 33, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(48, 28, 30, 1, 39, 0, 39, 1, 39, 1, 39, 0, 39, 1, 39, 1, 39, 0, 39, 1, 39, 1),
(49, 28, 33, 0, 0, 0, 39, 1, 39, 1, 0, 0, 39, 1, 39, 1, 0, 0, 39, 1, 39, 1),
(50, 29, 30, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(51, 29, 33, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(52, 30, 30, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(53, 30, 33, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(54, 31, 30, 1, 505, 0, 505, 1, 505, 1, 505, 0, 505, 1, 500, 10, 505, 0, 505, 1, 505, 1),
(55, 31, 33, 0, 252.5, 0, 252.5, 1, 252.5, 1, 252.5, 0, 252.5, 1, 252.5, 1, 252.5, 0, 252.5, 1, 252.5, 1),
(56, 32, 30, 1, 550, 0, 550, 1, 550, 1, 550, 0, 550, 1, 550, 1, 550, 2.5, 550, 1, 550, 1),
(57, 32, 33, 0, 275, 0, 275, 1, 275, 1, 275, 0, 275, 1, 275, 1, 275, 5, 275, 1, 275, 1),
(58, 33, 32, 1, 940, 0, 940, 1, 940, 1, 940, 0, 940, 1, 940, 1, 940, 6, 940, 1, 940, 1),
(59, 34, 35, 1, 915, 0, 915, 1, 915, 1, 915, 0, 915, 1, 915, 1, 915, 0, 915, 1, 915, 1),
(60, 35, 30, 1, 830, 0, 830, 1, 830, 1, 830, 0, 830, 1, 830, 1, 830, 0, 830, 1, 830, 1),
(61, 36, 32, 1, 220, 0, 220, 1, 220, 1, 220, 10, 220, 1, 220, 1, 220, 0, 220, 1, 220, 1),
(62, 37, 30, 1, 285, 0, 285, 1, 285, 1, 285, 0, 285, 1, 285, 1, 285, 0, 285, 1, 285, 1),
(63, 37, 33, 0, 143, 0, 143, 1, 143, 1, 143, 0, 143, 1, 143, 1, 143, 0, 143, 1, 143, 1),
(64, 38, 30, 1, 290, 0, 290, 1, 290, 1, 290, 0, 290, 1, 290, 1, 290, 0, 290, 1, 290, 1),
(65, 38, 33, 0, 145, 0, 145, 1, 145, 1, 145, 0, 145, 1, 145, 1, 145, 0, 145, 1, 145, 1),
(66, 39, 30, 1, 535, 0, 535, 1, 535, 1, 535, 0, 535, 1, 535, 1, 535, 1, 535, 1, 535, 1),
(67, 39, 33, 0, 268, 0, 268, 1, 268, 1, 268, 0, 268, 1, 268, 1, 268, 2, 268, 1, 268, 1),
(68, 40, 30, 1, 355, 0, 355, 1, 355, 1, 355, 0, 355, 1, 355, 1, 355, 0, 355, 1, 355, 1),
(69, 40, 33, 0, 178, 0, 178, 1, 178, 1, 178, 0, 178, 1, 178, 1, 178, 0, 178, 1, 178, 1),
(70, 41, 30, 1, 257, 0, 257, 1, 257, 1, 257, 0, 257, 1, 257, 1, 257, 0, 257, 1, 257, 1),
(71, 41, 33, 0, 128.5, 0, 128.5, 1, 128.5, 1, 128.5, 0, 128.5, 1, 128.5, 1, 128.5, 0, 128.5, 1, 128.5, 1),
(72, 42, 36, 1, 1535, 0, 1535, 1, 1535, 1, 1535, 3, 1535, 1, 1540, 4, 1535, 1, 1535, 1, 1535, 1),
(73, 43, 36, 1, 585, 0, 585, 1, 585, 1, 595, 18, 595, 1, 595, 1, 585, 0, 585, 1, 585, 1),
(74, 44, 36, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(75, 45, 36, 1, 774, 0, 774, 1, 774, 1, 774, 1, 774, 1, 774, 1, 774, 0, 774, 1, 774, 1),
(76, 46, 36, 1, 711, 0, 711, 1, 711, 1, 711, 0, 711, 1, 711, 1, 711, 0, 711, 1, 711, 1),
(77, 47, 36, 1, 467, 0, 467, 1, 467, 1, 467, 0, 467, 1, 467, 1, 467, 0, 467, 1, 467, 1),
(78, 48, 36, 1, 510, 0, 510, 1, 510, 1, 510, 0, 510, 1, 510, 1, 510, 0, 510, 1, 510, 1),
(79, 49, 36, 1, 699, 0, 699, 1, 699, 1, 699, 0, 699, 1, 699, 1, 699, 0, 699, 1, 699, 1),
(80, 50, 36, 1, 630, 0, 630, 1, 630, 1, 630, 0, 630, 1, 630, 1, 630, 0, 630, 1, 630, 1),
(81, 51, 36, 1, 430, 0, 430, 1, 430, 1, 430, 0, 430, 1, 430, 1, 430, 0, 430, 1, 430, 1),
(82, 52, 36, 1, 330, 0, 330, 1, 330, 1, 330, 0, 330, 1, 330, 1, 330, 0, 330, 1, 330, 1),
(83, 53, 36, 1, 1010, 0, 1010, 1, 1010, 1, 1010, 0, 1010, 1, 1010, 1, 1010, -3, 1010, 1, 1010, 1),
(84, 54, 36, 1, 1010, 13, 1010, 1, 1010, 1, 1010, 0, 1010, 1, 1010, 1, 1010, 7, 1010, 1, 1010, 1),
(85, 55, 36, 1, 980, 0, 980, 1, 980, 1, 980, 0, 980, 1, 980, 1, 980, 0, 980, 1, 980, 1),
(86, 56, 36, 1, 565, 0, 565, 1, 565, 1, 565, 0, 565, 1, 565, 1, 565, 1, 565, 1, 565, 1),
(87, 57, 36, 1, 492, 0, 492, 1, 492, 1, 492, 0, 492, 1, 492, 1, 492, 0, 492, 1, 492, 1),
(88, 58, 36, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(89, 59, 36, 1, 310, 0, 310, 1, 310, 1, 310, 0, 310, 1, 310, 1, 310, 0, 310, 1, 310, 1),
(90, 60, 36, 1, 385, 0, 385, 1, 361, 10, 385, 0, 385, 1, 385, 1, 385, 0, 385, 1, 361, 10),
(91, 61, 36, 1, 275, 0, 275, 1, 275, 1, 275, 0, 275, 1, 275, 1, 275, 0, 275, 1, 275, 1),
(92, 62, 36, 1, 300, 0, 300, 1, 300, 1, 300, 0, 300, 1, 300, 1, 300, 0, 300, 1, 300, 1),
(93, 63, 36, 1, 345, 0, 345, 1, 345, 1, 345, 1, 345, 1, 345, 1, 345, 0, 345, 1, 345, 1),
(94, 64, 36, 1, 561, 0, 561, 1, 540, 10, 561, 0, 561, 1, 540, 10, 561, 20, 561, 1, 540, 10),
(95, 65, 36, 1, 420, 0, 420, 1, 420, 1, 420, 0, 420, 1, 420, 1, 420, 0, 420, 1, 420, 1),
(96, 66, 36, 1, 860, 1, 860, 1, 860, 1, 860, 2, 860, 1, 860, 1, 860, 0, 860, 1, 860, 1),
(97, 67, 36, 1, 441, 0, 441, 1, 441, 1, 441, 0, 441, 1, 441, 1, 441, 5, 441, 1, 441, 1),
(98, 68, 30, 1, 370, 6, 370, 1, 370, 1, 370, 0, 370, 1, 370, 1, 370, 11.5, 370, 1, 370, 1),
(99, 68, 33, 0, 185, 13, 185, 1, 185, 1, 185, 0, 185, 1, 185, 1, 185, 23, 185, 1, 185, 1),
(100, 69, 30, 1, 370, 23, 370, 1, 370, 1, 370, 1, 370, 1, 370, 1, 370, 15.5, 370, 1, 370, 1),
(101, 69, 33, 0, 185, 46, 185, 1, 185, 1, 185, 2, 185, 1, 185, 1, 185, 31, 185, 1, 185, 1),
(102, 70, 30, 1, 370, 12.5, 370, 1, 370, 1, 370, 1.5, 370, 1, 370, 1, 370, 12, 370, 1, 370, 1),
(103, 70, 33, 0, 185, 25, 185, 1, 185, 1, 185, 3, 185, 1, 185, 1, 185, 24, 185, 1, 185, 1),
(104, 71, 36, 1, 380, 24, 380, 1, 380, 1, 380, 1, 380, 1, 380, 1, 380, 35, 380, 1, 380, 1),
(105, 72, 30, 1, 380, 2.5, 380, 1, 380, 1, 380, 3, 380, 1, 380, 1, 380, 0, 380, 1, 380, 1),
(106, 72, 33, 0, 190, 5, 190, 1, 190, 1, 190, 6, 190, 1, 190, 1, 190, 0, 190, 1, 190, 1),
(107, 73, 30, 1, 380, 10.5, 380, 1, 380, 1, 380, 3, 380, 1, 380, 1, 380, 7, 380, 1, 380, 1),
(108, 73, 33, 0, 190, 21, 190, 1, 190, 1, 190, 6, 190, 1, 190, 1, 190, 14, 190, 1, 190, 1),
(109, 74, 30, 1, 380, 5, 380, 1, 380, 1, 380, 1.5, 380, 1, 380, 1, 380, 6.5, 380, 1, 380, 1),
(110, 74, 33, 0, 190, 10, 190, 1, 190, 1, 190, 3, 190, 1, 190, 1, 190, 13, 190, 1, 190, 1),
(111, 75, 36, 1, 380, 7, 380, 1, 380, 1, 380, 3, 380, 1, 380, 1, 380, 0, 380, 1, 380, 1),
(112, 76, 30, 1, 303, 0, 303, 1, 303, 1, 303, 0, 303, 1, 303, 1, 303, 0, 303, 1, 303, 1),
(113, 76, 33, 0, 152, 0, 152, 1, 152, 1, 152, 0, 152, 1, 152, 1, 152, 0, 152, 1, 152, 1),
(114, 77, 30, 1, 303, 0, 303, 1, 303, 1, 303, 0, 303, 1, 303, 1, 303, 0, 303, 1, 303, 1),
(115, 77, 33, 0, 152, 0, 152, 1, 152, 1, 152, 0, 152, 1, 152, 1, 152, 0, 152, 1, 152, 1),
(116, 78, 30, 1, 315, 0, 315, 1, 315, 1, 315, 0, 315, 1, 315, 1, 315, 0, 315, 1, 315, 1),
(117, 78, 33, 0, 158, 0, 158, 1, 158, 1, 158, 0, 158, 1, 158, 1, 158, 0, 158, 1, 158, 1),
(118, 79, 30, 1, 342, 0, 342, 1, 342, 1, 342, 0, 342, 1, 342, 1, 342, 2.5, 342, 1, 342, 1),
(119, 79, 33, 0, 171, 0, 171, 1, 171, 1, 171, 0, 171, 1, 171, 1, 171, 5, 171, 1, 171, 1),
(120, 80, 30, 1, 277, 0, 277, 1, 277, 1, 277, 0, 277, 1, 277, 1, 277, 2, 277, 1, 277, 1),
(121, 80, 33, 0, 138.5, 0, 138.5, 1, 138.5, 1, 138.5, 0, 138.5, 1, 138.5, 1, 138.5, 4, 138.5, 1, 138.5, 1),
(122, 81, 30, 1, 132, 0, 132, 1, 132, 1, 132, 0, 132, 1, 132, 1, 132, 20, 132, 1, 132, 1),
(123, 81, 33, 0, 66, 0, 66, 1, 66, 1, 66, 0, 66, 1, 66, 1, 66, 40, 66, 1, 66, 1),
(124, 82, 30, 1, 140, 5.5, 140, 1, 140, 1, 140, 0, 140, 1, 140, 1, 140, 8, 140, 1, 140, 1),
(125, 82, 33, 0, 70, 11, 70, 1, 70, 1, 70, 0, 70, 1, 70, 1, 70, 16, 70, 1, 70, 1),
(126, 83, 30, 1, 140, 0, 140, 1, 140, 1, 140, 0, 140, 1, 140, 1, 140, 10, 140, 1, 140, 1),
(127, 83, 33, 0, 70, 0, 70, 1, 70, 1, 70, 0, 70, 1, 70, 1, 70, 20, 70, 1, 70, 1),
(128, 84, 30, 1, 120, 0, 120, 1, 120, 1, 120, 0, 120, 1, 120, 1, 120, 0, 120, 1, 120, 1),
(129, 84, 33, 0, 60, 0, 60, 1, 60, 1, 60, 0, 60, 1, 60, 1, 60, 0, 60, 1, 60, 1),
(130, 85, 30, 1, 290, 0, 290, 1, 290, 1, 290, 0, 290, 1, 290, 1, 290, 0, 290, 1, 290, 1),
(131, 86, 32, 1, 65, 0, 65, 1, 65, 1, 65, 0, 65, 1, 65, 1, 65, 3, 65, 1, 65, 1),
(132, 87, 30, 1, 253, 0, 253, 1, 253, 1, 253, 0, 253, 1, 253, 1, 253, 0, 253, 1, 253, 1),
(133, 87, 33, 0, 126.5, 0, 126.5, 1, 126.5, 1, 126.5, 0, 126.5, 1, 126.5, 1, 126.5, 0, 126.5, 1, 126.5, 1),
(134, 88, 32, 1, 25, 0, 25, 1, 25, 1, 25, 0, 25, 1, 25, 1, 25, 0, 25, 1, 25, 1),
(135, 89, 30, 1, 355, 4, 355, 1, 355, 1, 355, 0, 355, 1, 355, 1, 355, 10, 355, 1, 355, 1),
(136, 89, 33, 0, 177.5, 8, 177.5, 1, 177.5, 1, 177.5, 0, 177.5, 1, 177.5, 1, 177.5, 20, 177.5, 1, 177.5, 1),
(137, 90, 30, 1, 248, 0, 248, 1, 248, 1, 248, 0, 248, 1, 248, 1, 248, 0.5, 248, 1, 248, 1),
(138, 90, 33, 0, 124, 0, 124, 1, 124, 1, 124, 0, 124, 1, 124, 1, 124, 1, 124, 1, 124, 1),
(139, 91, 36, 1, 340, 0, 340, 1, 340, 1, 340, 0, 340, 1, 330, 10, 340, 0, 340, 1, 340, 1),
(140, 92, 36, 1, 315, 0, 315, 1, 315, 1, 315, 0, 315, 1, 315, 1, 315, 10, 315, 1, 315, 1),
(141, 93, 36, 1, 160, 0, 160, 1, 160, 1, 160, 0, 160, 1, 160, 1, 160, 0, 160, 1, 160, 1),
(142, 94, 36, 1, 180, 0, 180, 1, 180, 1, 180, 0, 180, 1, 180, 1, 180, 0, 180, 1, 180, 1),
(143, 95, 30, 1, 269, 0, 269, 1, 269, 1, 269, 0, 269, 1, 269, 1, 269, 0, 269, 1, 269, 1),
(144, 96, 36, 1, 130, 0, 130, 1, 130, 1, 130, 0, 130, 1, 130, 1, 130, 0, 130, 1, 125, 6),
(145, 97, 36, 1, 295, 0, 295, 1, 295, 1, 295, 0, 295, 1, 295, 1, 295, 0, 295, 1, 295, 1),
(146, 98, 36, 1, 175, 0, 175, 1, 175, 1, 175, 0, 175, 1, 175, 1, 175, 0, 175, 1, 175, 1),
(147, 99, 36, 1, 575, 0, 575, 1, 575, 1, 575, 0, 575, 1, 575, 1, 575, 0, 575, 1, 575, 2),
(148, 100, 36, 1, 305, 0, 305, 1, 305, 1, 305, 0, 305, 1, 305, 1, 305, 0, 305, 1, 305, 1),
(149, 101, 36, 1, 290, 0, 290, 1, 290, 1, 290, 0, 290, 1, 290, 1, 290, 0, 290, 1, 290, 1),
(150, 102, 36, 1, 400, 7, 400, 1, 400, 1, 400, 2, 400, 1, 400, 1, 400, 7, 400, 1, 400, 1),
(151, 103, 30, 1, 130, 89, 130, 1, 125, 24, 130, 175, 130, 1, 130, 1, 130, 89, 130, 1, 125, 24),
(152, 104, 30, 1, 365, 0, 365, 1, 365, 1, 365, 0, 365, 1, 365, 1, 365, 0, 365, 1, 365, 1),
(153, 104, 33, 0, 182.5, 0, 182.5, 1, 182.5, 1, 182.5, 0, 182.5, 1, 182.5, 1, 182.5, 0, 182.5, 1, 182.5, 1),
(154, 105, 32, 1, 40, 0, 40, 1, 40, 1, 40, 0, 40, 1, 40, 1, 40, 0, 40, 1, 40, 1),
(155, 106, 30, 1, 1800, 1, 1800, 1, 1800, 1, 1800, 0, 1800, 1, 1800, 1, 1800, 1, 1800, 1, 1800, 1),
(156, 106, 33, 0, 900, 2, 900, 1, 900, 1, 900, 0, 900, 1, 900, 1, 900, 2, 900, 1, 900, 1),
(157, 107, 32, 1, 100, 6, 100, 1, 100, 1, 100, 9, 100, 1, 100, 1, 100, 30, 100, 1, 100, 1),
(158, 108, 36, 1, 160, 0, 160, 1, 160, 1, 160, 0, 160, 1, 160, 1, 160, 0, 160, 1, 160, 1),
(159, 109, 36, 1, 620, 0, 620, 1, 620, 1, 620, 0, 620, 1, 620, 1, 620, 0, 620, 1, 620, 1),
(160, 110, 36, 1, 620, 0, 620, 1, 620, 1, 620, 0, 620, 1, 620, 1, 620, 0, 620, 1, 620, 1),
(161, 111, 30, 1, 273, 0, 273, 1, 273, 5, 273, 0, 273, 1, 273, 1, 273, 0, 273, 1, 273, 5),
(162, 111, 33, 0, 137, 0, 137, 1, 137, 1, 137, 0, 137, 1, 137, 1, 137, 0, 137, 1, 137, 1),
(163, 112, 30, 1, 450, 0, 450, 1, 450, 1, 450, 0, 450, 1, 450, 1, 450, 0, 450, 1, 450, 1),
(164, 112, 33, 0, 225, 0, 225, 1, 225, 1, 225, 0, 225, 1, 225, 1, 225, 0, 225, 1, 225, 1),
(165, 113, 36, 1, 900, 0, 900, 1, 900, 1, 900, 0, 900, 1, 900, 1, 900, 0, 900, 1, 900, 1),
(166, 114, 36, 1, 450, 0, 450, 1, 450, 1, 450, 0, 450, 1, 450, 1, 450, 0, 450, 1, 450, 1),
(167, 115, 36, 1, 500, 0, 500, 1, 500, 1, 500, 0, 500, 1, 500, 1, 500, 0, 500, 1, 500, 1),
(168, 116, 30, 1, 218, 0, 218, 1, 186, 10, 218, 0, 218, 1, 218, 1, 218, 0, 218, 1, 186, 10),
(169, 117, 30, 1, 530, 0, 530, 1, 530, 1, 530, 0, 530, 1, 530, 1, 530, 0, 530, 1, 530, 1),
(170, 117, 33, 0, 265, 0, 265, 1, 265, 1, 265, 0, 265, 1, 265, 1, 265, 0, 265, 1, 265, 1),
(171, 118, 36, 1, 345, 0, 345, 1, 345, 1, 345, 0, 345, 1, 345, 1, 345, 0, 345, 1, 345, 1),
(172, 119, 30, 1, 445, 30, 445, 1, 445, 1, 445, 0, 445, 1, 445, 1, 445, 0.5, 445, 1, 445, 1),
(173, 119, 33, 0, 223, 60, 223, 1, 223, 1, 223, 0, 223, 1, 223, 1, 223, 1, 223, 1, 223, 1),
(174, 120, 30, 1, 274, 0, 274, 1, 274, 1, 274, 0, 274, 1, 274, 1, 274, 0, 274, 1, 274, 1),
(175, 121, 36, 1, 240, 5, 240, 1, 240, 1, 240, 0, 240, 1, 240, 1, 240, 16, 240, 1, 240, 1),
(176, 122, 30, 1, 260, 0, 260, 1, 260, 1, 210, 0, 210, 1, 210, 1, 260, 2, 260, 1, 260, 1),
(177, 123, 30, 1, 241, 0, 241, 1, 241, 1, 241, 0, 241, 1, 241, 1, 241, 5, 241, 1, 241, 1),
(178, 124, 30, 1, 250, 0, 250, 1, 250, 1, 240, 0, 240, 1, 240, 1, 250, 0, 250, 1, 250, 1),
(179, 125, 30, 1, 240, 10, 240, 1, 210, 10, 240, 7, 240, 1, 240, 1, 240, 10, 240, 1, 210, 10),
(180, 126, 30, 1, 200, 0, 200, 1, 200, 1, 200, 4, 200, 1, 200, 1, 200, 7, 200, 1, 200, 1),
(181, 127, 30, 1, 145, 0, 145, 1, 145, 1, 145, 0, 145, 1, 145, 1, 145, 0, 145, 1, 145, 1),
(182, 128, 30, 1, 145, 0, 145, 1, 145, 1, 145, 0, 145, 1, 145, 1, 145, 0, 145, 1, 145, 1),
(183, 129, 30, 1, 220, 2, 220, 1, 220, 1, 220, 8, 220, 1, 220, 1, 220, 9, 220, 1, 220, 1),
(184, 130, 30, 1, 115, 0, 115, 1, 115, 1, 115, 0, 115, 1, 115, 1, 115, 0, 115, 1, 115, 1),
(185, 131, 30, 1, 117, 0, 117, 1, 117, 1, 117, 0, 117, 1, 117, 1, 117, 0, 117, 1, 117, 1),
(186, 132, 30, 1, 338, 0, 338, 1, 338, 1, 338, 0, 338, 1, 338, 1, 338, 0, 338, 1, 338, 1),
(187, 132, 33, 0, 169, 0, 169, 1, 169, 1, 169, 0, 169, 1, 169, 1, 169, 0, 169, 1, 169, 1),
(188, 133, 30, 1, 349, 0, 349, 1, 349, 1, 349, 0, 349, 1, 349, 1, 349, 0, 349, 1, 349, 1),
(189, 133, 33, 0, 175, 0, 175, 1, 175, 1, 175, 0, 175, 1, 175, 1, 175, 0, 175, 1, 175, 1),
(190, 134, 30, 1, 366, 0, 366, 1, 366, 1, 366, 0, 366, 1, 366, 1, 366, 0, 366, 1, 366, 1),
(191, 134, 33, 0, 183, 0, 183, 1, 183, 1, 183, 0, 183, 1, 183, 1, 183, 0, 183, 1, 183, 1),
(192, 135, 30, 1, 168, 0, 168, 1, 168, 1, 168, 0, 168, 1, 168, 1, 168, 0, 168, 1, 168, 1),
(193, 136, 30, 1, 368, 0, 368, 1, 368, 1, 368, 0, 368, 1, 368, 1, 368, 3, 368, 1, 368, 1),
(194, 136, 33, 0, 184, 0, 184, 1, 184, 1, 184, 0, 184, 1, 184, 1, 184, 6, 184, 1, 184, 1),
(195, 137, 32, 1, 49, 0, 49, 1, 49, 1, 49, 0, 49, 1, 49, 1, 49, 0, 49, 1, 49, 1),
(196, 138, 32, 1, 170, 88, 170, 1, 170, 1, 170, 8, 170, 1, 170, 1, 170, 110, 170, 1, 170, 1),
(197, 139, 32, 1, 85, 0, 85, 1, 85, 1, 85, 0, 85, 1, 85, 1, 85, 8, 85, 1, 85, 1),
(198, 140, 32, 1, 85, 16, 85, 1, 85, 1, 85, 8, 85, 1, 85, 1, 85, 29, 85, 1, 85, 1),
(199, 141, 32, 1, 127, 0, 127, 1, 127, 1, 127, 0, 127, 1, 127, 1, 127, 0, 127, 1, 127, 1),
(200, 142, 30, 1, 340, 0, 340, 1, 340, 1, 340, 0, 340, 1, 340, 1, 340, 3, 340, 1, 340, 1),
(201, 143, 30, 1, 470, 0, 470, 1, 470, 1, 470, 0, 470, 1, 470, 1, 470, 5, 470, 1, 470, 1),
(202, 143, 33, 0, 235, 0, 235, 1, 235, 1, 235, 0, 235, 1, 235, 1, 235, 10, 235, 1, 235, 1),
(203, 144, 32, 1, 135, 128, 135, 1, 135, 1, 135, 26, 135, 1, 130, 6, 135, 149, 135, 1, 135, 1),
(204, 145, 36, 1, 280, 0, 280, 1, 280, 1, 280, 0, 280, 1, 280, 1, 280, 0, 280, 1, 280, 1),
(205, 146, 36, 1, 200, 0, 200, 1, 200, 1, 200, 0, 200, 1, 200, 1, 200, 0, 200, 1, 200, 1),
(206, 147, 30, 1, 595, 0, 595, 1, 595, 1, 595, 0, 595, 1, 595, 1, 595, 0, 595, 1, 595, 1),
(207, 148, 30, 1, 365, 0, 365, 1, 365, 1, 365, 0, 365, 1, 365, 1, 365, 0.5, 365, 1, 365, 1),
(208, 148, 33, 0, 183, 0, 183, 1, 183, 1, 183, 0, 183, 1, 183, 1, 183, 1, 183, 1, 183, 1),
(209, 149, 30, 1, 595, 1, 595, 1, 595, 1, 595, 0, 595, 1, 595, 1, 595, 3.5, 595, 1, 595, 1),
(210, 149, 33, 0, 297.5, 2, 297.5, 1, 297.5, 1, 297.5, 0, 297.5, 1, 297.5, 1, 297.5, 7, 297.5, 1, 297.5, 1),
(211, 150, 30, 1, 408, 0, 408, 1, 408, 1, 408, 0, 408, 1, 408, 1, 408, 10, 408, 1, 408, 1),
(212, 150, 33, 0, 204, 0, 204, 1, 204, 1, 204, 0, 204, 1, 204, 1, 204, 20, 204, 1, 204, 1),
(213, 151, 30, 1, 1090, 26, 1090, 1, 1090, 1, 1090, 0, 1090, 1, 1090, 1, 1090, 29, 1090, 1, 1090, 1),
(214, 152, 32, 1, 285, 1, 285, 1, 285, 1, 285, 0, 285, 1, 285, 1, 285, 5, 285, 1, 285, 1),
(215, 153, 30, 1, 520, 13, 520, 1, 520, 1, 520, 0, 520, 1, 520, 1, 520, 14, 520, 1, 520, 1),
(216, 153, 33, 0, 260, 26, 260, 1, 260, 1, 260, 0, 260, 1, 260, 1, 260, 28, 260, 1, 260, 1),
(217, 154, 30, 1, 834, 0, 834, 1, 834, 1, 834, 0, 834, 1, 834, 1, 834, 0, 834, 1, 834, 1),
(218, 154, 33, 0, 417, 0, 417, 1, 417, 1, 417, 0, 417, 1, 417, 1, 417, 0, 417, 1, 417, 1),
(219, 155, 30, 1, 338, 0, 338, 1, 338, 1, 338, 0, 338, 1, 338, 1, 338, 0, 338, 1, 338, 1),
(221, 156, 30, 1, 1040, 4, 1040, 1, 1040, 1, 1040, 0, 1040, 1, 1040, 1, 1040, 6.5, 1040, 1, 1040, 1),
(222, 156, 33, 0, 520, 8, 520, 1, 520, 1, 520, 0, 520, 1, 520, 1, 520, 13, 520, 1, 520, 1),
(223, 157, 30, 1, 310, 0, 310, 1, 310, 1, 310, 0, 310, 1, 310, 1, 310, 0, 310, 1, 310, 1),
(224, 158, 32, 1, 60, 0, 60, 1, 60, 1, 60, 0, 60, 1, 60, 1, 60, 0, 60, 1, 60, 1),
(225, 159, 30, 1, 440, 1.5, 440, 1, 440, 1, 440, 0, 440, 1, 440, 1, 440, 5, 440, 1, 440, 1),
(226, 159, 33, 0, 220, 3, 220, 1, 220, 1, 220, 0, 220, 1, 220, 1, 220, 10, 220, 1, 220, 1),
(227, 160, 30, 1, 530, 18, 530, 1, 530, 1, 530, 0, 530, 1, 530, 1, 530, 20.5, 530, 1, 530, 1),
(228, 160, 33, 0, 265, 36, 265, 1, 265, 1, 265, 0, 265, 1, 265, 1, 265, 41, 265, 1, 265, 1),
(229, 161, 30, 1, 362, 21, 362, 1, 362, 1, 362, 21, 362, 1, 362, 1, 362, 12, 362, 1, 362, 1),
(230, 161, 33, 0, 181, 42, 181, 1, 181, 1, 181, 42, 181, 1, 181, 1, 181, 24, 181, 1, 181, 1),
(231, 162, 30, 1, 440, 7, 440, 1, 440, 1, 440, 0, 440, 1, 440, 1, 440, 7, 440, 1, 440, 1),
(233, 163, 30, 1, 300, 0, 300, 1, 300, 1, 300, 0, 286, 1, 300, 1, 300, 9, 300, 1, 300, 1),
(234, 163, 33, 0, 150, 0, 150, 1, 150, 1, 150, 0, 150, 1, 150, 1, 150, 18, 150, 1, 150, 1),
(235, 164, 30, 1, 477, 0, 477, 1, 477, 1, 477, 0, 477, 1, 477, 1, 477, 0, 477, 1, 477, 1),
(236, 164, 33, 0, 239, 0, 239, 1, 239, 1, 239, 0, 239, 1, 239, 1, 239, 0, 239, 1, 239, 1),
(237, 165, 32, 1, 85, 5, 85, 1, 85, 1, 85, 3, 85, 1, 85, 1, 85, 6, 85, 1, 85, 1),
(238, 166, 32, 1, 115, 4, 115, 1, 115, 1, 115, 0, 115, 1, 115, 1, 115, 6, 115, 1, 115, 1),
(239, 167, 30, 1, 381, 1, 381, 1, 381, 1, 381, 1, 381, 1, 381, 1, 381, 1, 381, 1, 381, 1),
(240, 167, 33, 0, 191, 2, 191, 1, 191, 1, 191, 2, 191, 1, 191, 1, 191, 2, 191, 1, 191, 1),
(241, 168, 30, 1, 381, 0, 381, 1, 381, 1, 381, 0, 381, 1, 381, 1, 381, 0, 381, 1, 381, 1),
(242, 168, 33, 0, 191, 0, 191, 1, 191, 1, 191, 0, 191, 1, 191, 1, 191, 0, 191, 1, 191, 1),
(243, 169, 30, 1, 590, 0, 590, 1, 590, 1, 590, 0, 590, 1, 590, 1, 590, 2, 590, 1, 590, 1),
(244, 169, 33, 0, 295, 0, 295, 1, 295, 1, 295, 0, 295, 1, 295, 1, 295, 4, 295, 1, 295, 1),
(245, 170, 30, 1, 1490, 2.5, 1490, 1, 1490, 1, 1490, 0, 1490, 1, 1490, 1, 1490, 2.5, 1490, 1, 1490, 1),
(246, 170, 33, 0, 745, 5, 745, 1, 745, 1, 745, 0, 745, 1, 745, 1, 745, 5, 745, 1, 745, 1),
(247, 171, 32, 1, 75, 18, 75, 1, 75, 1, 75, 0, 75, 1, 75, 1, 75, 25, 75, 1, 75, 1),
(248, 172, 32, 1, 48, 0, 48, 1, 48, 1, 48, 0, 48, 1, 48, 1, 48, 0, 48, 1, 48, 1),
(249, 173, 30, 1, 1160, 10.5, 1160, 1, 1160, 1, 1160, 1, 1160, 1, 1160, 1, 1160, 13.5, 1160, 1, 1160, 1),
(250, 173, 33, 0, 580, 21, 580, 1, 580, 1, 580, 2, 580, 1, 580, 1, 580, 27, 580, 1, 580, 1),
(251, 174, 30, 1, 920, 22.5, 920, 1, 920, 1, 920, 2, 920, 1, 920, 1, 920, 25.5, 920, 1, 920, 1),
(252, 174, 33, 0, 460, 45, 460, 1, 460, 1, 460, 4, 460, 1, 460, 1, 460, 51, 460, 1, 460, 1),
(253, 175, 32, 1, 103, 12, 103, 1, 103, 1, 103, 12, 103, 1, 103, 1, 103, 11, 103, 1, 103, 1),
(254, 176, 30, 1, 414, 0, 414, 1, 414, 1, 414, 0, 414, 1, 414, 1, 414, 1, 414, 1, 414, 1),
(255, 176, 33, 0, 207, 0, 207, 1, 207, 1, 207, 0, 207, 1, 207, 1, 207, 2, 207, 1, 207, 1),
(256, 177, 30, 1, 453, 0, 453, 1, 453, 1, 453, 0, 453, 1, 453, 1, 453, 0, 453, 1, 453, 1),
(257, 177, 33, 0, 227, 0, 227, 1, 227, 1, 227, 0, 227, 1, 227, 1, 227, 0, 227, 1, 227, 1),
(258, 178, 30, 1, 720, 0, 720, 1, 720, 1, 720, 0, 720, 1, 720, 1, 720, 0, 720, 1, 720, 1),
(259, 178, 33, 0, 360, 0, 360, 1, 360, 1, 360, 0, 360, 1, 360, 1, 360, 0, 360, 1, 360, 1),
(260, 179, 30, 1, 685, 7.5, 685, 1, 685, 1, 685, 0, 685, 1, 685, 1, 685, 15, 685, 1, 685, 1),
(261, 179, 33, 0, 342.5, 15, 342.5, 1, 342.5, 1, 342.5, 0, 342.5, 1, 342.5, 1, 342.5, 30, 342.5, 1, 342.5, 1),
(262, 180, 30, 1, 720, 0, 720, 1, 720, 1, 720, 0, 720, 1, 720, 1, 720, 0, 720, 1, 720, 1),
(263, 180, 33, 0, 360, 0, 360, 1, 360, 1, 360, 0, 360, 1, 360, 1, 360, 0, 360, 1, 360, 1),
(264, 181, 30, 1, 685, 11, 685, 1, 685, 1, 685, 0.5, 685, 1, 685, 1, 685, 14, 685, 1, 685, 1),
(265, 181, 33, 0, 342.5, 22, 342.5, 1, 342.5, 1, 342.5, 1, 342.5, 1, 342.5, 1, 342.5, 28, 342.5, 1, 342.5, 1),
(266, 182, 30, 1, 380, 0, 380, 1, 380, 1, 380, 0, 380, 1, 380, 1, 380, 0, 380, 1, 380, 1),
(267, 182, 33, 0, 190, 0, 190, 1, 190, 1, 190, 0, 190, 1, 190, 1, 190, 0, 190, 1, 190, 1),
(268, 183, 30, 1, 380, 0, 380, 1, 380, 1, 380, 0, 380, 1, 380, 1, 380, 0, 380, 1, 380, 1),
(269, 183, 33, 0, 190, 0, 190, 1, 190, 1, 190, 0, 190, 1, 190, 1, 190, 0, 190, 1, 190, 1),
(270, 184, 30, 1, 290, 0, 290, 1, 290, 1, 290, 0, 290, 1, 290, 1, 290, 0, 290, 1, 290, 1),
(271, 184, 33, 0, 145, 0, 145, 1, 145, 1, 145, 0, 145, 1, 145, 1, 145, 0, 145, 1, 145, 1),
(272, 185, 32, 1, 26, 0, 26, 1, 26, 1, 26, 0, 26, 1, 26, 1, 26, 0, 26, 1, 26, 1),
(273, 186, 30, 1, 395, 0, 395, 1, 395, 1, 395, 0, 395, 1, 395, 1, 395, 0, 395, 1, 395, 1),
(274, 186, 33, 0, 197, 0, 197, 1, 197, 1, 197, 0, 197, 1, 197, 1, 197, 0, 197, 1, 197, 1),
(275, 187, 32, 1, 35, 0, 35, 1, 35, 1, 35, 0, 35, 1, 35, 1, 35, 0, 35, 1, 35, 1),
(276, 188, 30, 1, 455, 0, 455, 1, 455, 1, 455, 0, 455, 1, 455, 1, 455, 2.5, 455, 1, 455, 1),
(277, 188, 33, 0, 228, 0, 228, 1, 228, 1, 228, 0, 228, 1, 228, 1, 228, 5, 228, 1, 228, 1),
(278, 189, 32, 1, 50, 0, 50, 1, 50, 1, 50, 0, 50, 1, 50, 1, 50, 0, 50, 1, 50, 1),
(279, 190, 30, 1, 430, 0, 430, 1, 430, 1, 430, 0, 430, 1, 430, 1, 430, 0, 430, 1, 430, 1),
(280, 190, 33, 0, 215, 0, 215, 1, 215, 1, 215, 0, 215, 1, 215, 1, 215, 0, 215, 1, 215, 1),
(281, 191, 32, 1, 40, 0, 40, 1, 40, 1, 40, 0, 40, 1, 40, 1, 40, 0, 40, 1, 40, 1),
(282, 192, 32, 1, 36, 0, 36, 1, 36, 1, 36, 0, 36, 1, 36, 1, 36, 0, 36, 1, 36, 1),
(283, 193, 30, 1, 188, 0, 188, 1, 188, 1, 188, 0, 188, 1, 188, 1, 188, 0, 188, 1, 188, 1),
(284, 193, 33, 0, 94, 0, 94, 1, 94, 1, 94, 0, 94, 1, 94, 1, 94, 0, 94, 1, 94, 1),
(285, 194, 32, 1, 80, 0, 80, 1, 80, 1, 80, 0, 80, 1, 80, 1, 80, 0, 80, 1, 80, 1),
(286, 195, 30, 1, 471, 0, 471, 1, 471, 1, 471, 0, 471, 1, 471, 1, 471, 1, 471, 1, 471, 1),
(287, 195, 33, 0, 236, 0, 236, 1, 236, 1, 236, 0, 236, 1, 236, 1, 236, 2, 236, 1, 236, 1),
(288, 196, 30, 1, 465, 0, 465, 1, 465, 1, 465, 0, 465, 1, 465, 1, 465, 0, 465, 1, 465, 1),
(289, 196, 33, 0, 232, 0, 232, 1, 232, 1, 232, 0, 232, 1, 232, 1, 232, 0, 232, 1, 232, 1),
(290, 197, 32, 1, 35, 4, 35, 1, 35, 1, 35, 4, 35, 1, 35, 1, 35, 129, 35, 1, 35, 1),
(291, 198, 32, 1, 125, 0, 125, 1, 125, 1, 125, 0, 125, 1, 125, 1, 125, 0, 125, 1, 125, 1),
(292, 199, 32, 1, 195, 0, 195, 1, 195, 1, 195, 0, 195, 1, 195, 1, 195, 0, 195, 1, 195, 1),
(293, 200, 32, 1, 175, 0, 175, 1, 175, 1, 175, 0, 175, 1, 175, 1, 175, 0, 175, 1, 175, 1),
(294, 201, 32, 1, 90, 54, 90, 1, 90, 1, 90, 54, 90, 1, 90, 1, 90, 43, 90, 1, 90, 1),
(295, 202, 32, 1, 175, 0, 175, 1, 175, 1, 175, 0, 175, 1, 175, 1, 175, 0, 175, 1, 175, 1),
(296, 203, 32, 1, 127, 0, 127, 1, 127, 1, 127, 0, 127, 1, 127, 1, 127, 0, 127, 1, 127, 1),
(297, 204, 30, 1, 165, 0, 165, 1, 165, 1, 165, 0, 165, 1, 165, 1, 165, 0, 165, 1, 165, 1),
(298, 205, 30, 1, 118, 0, 118, 1, 118, 1, 118, 0, 118, 1, 118, 1, 118, 0, 118, 1, 118, 1),
(299, 206, 32, 1, 125, 0, 125, 1, 125, 1, 125, 0, 125, 1, 125, 1, 125, 0, 125, 1, 125, 1),
(300, 207, 30, 1, 250, 0, 250, 1, 250, 1, 250, 0, 250, 1, 250, 1, 250, 0, 250, 1, 250, 1),
(301, 207, 33, 0, 125, 0, 125, 1, 125, 1, 125, 0, 125, 1, 125, 1, 125, 0, 125, 1, 125, 1),
(302, 208, 32, 1, 20, 0, 20, 1, 20, 1, 20, 0, 20, 1, 20, 1, 20, 0, 20, 1, 20, 1),
(303, 209, 30, 1, 234, 0, 234, 1, 234, 1, 234, 0, 234, 1, 234, 1, 234, 0, 234, 1, 234, 1),
(304, 209, 33, 0, 117, 0, 117, 1, 117, 1, 117, 0, 117, 1, 117, 1, 117, 0, 117, 1, 117, 1),
(305, 210, 30, 1, 222, 0, 222, 1, 222, 1, 222, 0, 222, 1, 222, 1, 222, 0, 222, 1, 222, 1),
(306, 210, 33, 0, 111, 0, 111, 1, 111, 1, 111, 0, 111, 1, 111, 1, 111, 0, 111, 1, 111, 1),
(307, 211, 30, 1, 200, 1.5, 200, 1, 200, 1, 200, 0, 200, 1, 200, 1, 200, 20, 200, 1, 200, 1),
(308, 211, 33, 0, 100, 3, 100, 1, 100, 1, 100, 0, 100, 1, 100, 1, 100, 40, 100, 1, 100, 1),
(309, 212, 30, 1, 200, 4, 200, 1, 200, 1, 200, 0, 200, 1, 200, 1, 200, 5.5, 200, 1, 200, 1),
(310, 212, 33, 0, 100, 8, 100, 1, 100, 1, 100, 0, 100, 1, 100, 1, 100, 11, 100, 1, 100, 1),
(311, 213, 30, 1, 160, 0, 160, 1, 160, 1, 160, 0, 160, 1, 160, 1, 160, 7, 160, 1, 160, 1),
(312, 213, 33, 0, 80, 0, 80, 1, 80, 1, 80, 0, 80, 1, 80, 1, 80, 14, 80, 1, 80, 1),
(313, 214, 30, 1, 160, 6.5, 160, 1, 160, 1, 160, 2, 160, 1, 160, 1, 160, 10.5, 160, 1, 160, 1),
(314, 214, 33, 0, 80, 13, 80, 1, 80, 1, 80, 4, 80, 1, 80, 1, 80, 21, 80, 1, 80, 1),
(315, 215, 30, 1, 835, 0, 835, 1, 835, 1, 835, 0, 835, 1, 835, 1, 835, 7, 835, 1, 835, 1),
(316, 215, 33, 0, 418, 0, 418, 1, 418, 1, 418, 0, 418, 1, 418, 1, 418, 14, 418, 1, 418, 1),
(317, 216, 30, 1, 420, 0, 420, 1, 420, 1, 420, 0, 420, 1, 420, 1, 420, 0, 420, 1, 420, 1),
(318, 217, 32, 1, 115, 0, 115, 1, 115, 1, 115, 0, 115, 1, 115, 1, 115, 0, 115, 1, 115, 1),
(319, 218, 30, 1, 490, 0, 490, 1, 490, 1, 490, 0, 490, 1, 490, 1, 490, 0, 490, 1, 490, 1),
(320, 219, 32, 1, 135, 0, 135, 1, 135, 1, 135, 0, 135, 1, 135, 1, 135, 0, 135, 1, 135, 1),
(321, 220, 30, 1, 370, 0, 370, 1, 370, 1, 370, 0, 370, 1, 370, 1, 370, 0, 370, 1, 370, 1),
(322, 221, 30, 1, 370, 1, 370, 1, 370, 1, 370, 1, 370, 1, 370, 1, 370, 1, 370, 1, 370, 1),
(323, 222, 30, 1, 370, 1, 370, 1, 370, 1, 370, 1, 370, 1, 370, 1, 370, 1, 370, 1, 370, 1),
(324, 223, 32, 1, 103, 0, 103, 1, 103, 1, 103, 1, 103, 1, 103, 1, 103, 0, 103, 1, 103, 1),
(325, 224, 32, 1, 94, 1, 94, 1, 94, 1, 94, 1, 94, 1, 94, 1, 94, 1, 94, 1, 94, 1),
(326, 225, 30, 1, 100, 0, 100, 1, 80, 28, 100, 0, 100, 1, 80, 28, 100, 0, 100, 1, 100, 1),
(327, 226, 30, 1, 21, 0, 21, 1, 21, 1, 21, 0, 21, 1, 21, 1, 21, 0, 21, 1, 21, 1),
(328, 226, 33, 0, 21, 0, 21, 1, 21, 1, 21, 0, 21, 1, 21, 1, 21, 0, 21, 1, 21, 1),
(329, 227, 32, 1, 70, 0, 70, 1, 70, 1, 70, 0, 70, 1, 70, 1, 70, 0, 70, 1, 70, 1),
(330, 228, 30, 1, 23, 0, 23, 1, 23, 5, 23, 0, 23, 1, 23, 5, 23, 0, 23, 1, 23, 10),
(331, 228, 33, 0, 23, 0, 23, 1, 23, 1, 23, 0, 23, 1, 23, 1, 23, 0, 23, 1, 23, 1),
(332, 229, 36, 1, 280, 0, 280, 1, 279.65, 20, 280, 0, 280, 1, 280, 1, 280, 0, 280, 1, 279.65, 20),
(333, 230, 30, 1, 620, 0, 620, 1, 620, 1, 620, 0, 620, 1, 620, 1, 620, 2, 620, 1, 620, 1),
(334, 231, 30, 1, 235, 0, 235, 1, 235, 1, 235, 0, 235, 1, 235, 1, 235, 0, 235, 1, 235, 1),
(335, 231, 33, 0, 118, 0, 118, 1, 118, 1, 118, 0, 118, 1, 118, 1, 118, 0, 118, 1, 118, 1),
(336, 232, 30, 1, 210, 0, 210, 1, 210, 1, 210, 0, 210, 1, 210, 1, 210, 0, 210, 1, 210, 1),
(337, 232, 33, 0, 105, 0, 105, 1, 105, 1, 105, 0, 105, 1, 105, 1, 105, 0, 105, 1, 105, 1),
(338, 233, 30, 1, 220, 5, 220, 1, 220, 1, 220, 5, 220, 1, 220, 1, 220, 0, 220, 1, 220, 1),
(339, 233, 33, 0, 110, 10, 110, 1, 110, 1, 110, 10, 110, 1, 110, 1, 110, 0, 110, 1, 110, 1),
(340, 234, 30, 1, 220, 0, 220, 1, 220, 1, 220, 0, 220, 1, 220, 1, 220, 0, 220, 1, 220, 1),
(341, 234, 33, 0, 110, 0, 110, 1, 110, 1, 110, 0, 110, 1, 110, 1, 110, 0, 110, 1, 110, 1),
(342, 235, 30, 1, 220, 1, 220, 1, 220, 1, 220, 1, 220, 1, 220, 1, 220, 0, 220, 1, 220, 1),
(343, 235, 33, 0, 110, 2, 110, 1, 110, 1, 110, 2, 110, 1, 110, 1, 110, 0, 110, 1, 110, 1),
(344, 236, 30, 1, 210, 0, 210, 1, 210, 1, 210, 0, 210, 1, 210, 1, 210, 0, 210, 1, 210, 1),
(345, 236, 33, 0, 105, 0, 105, 1, 105, 1, 105, 0, 105, 1, 105, 1, 105, 0, 105, 1, 105, 1),
(346, 237, 30, 1, 210, 0, 210, 1, 210, 1, 210, 0, 210, 1, 210, 1, 210, 0, 210, 1, 210, 1),
(347, 237, 33, 0, 105, 0, 105, 1, 105, 1, 105, 0, 105, 1, 105, 1, 105, 0, 105, 1, 105, 1),
(348, 238, 30, 1, 230, 0, 230, 1, 230, 1, 230, 0, 230, 1, 230, 1, 230, 0, 230, 1, 230, 1),
(349, 238, 33, 0, 115, 0, 115, 1, 115, 1, 115, 0, 115, 1, 115, 1, 115, 0, 115, 1, 115, 1),
(350, 239, 30, 1, 115, 0, 115, 1, 115, 1, 115, 0, 115, 1, 115, 1, 115, 0, 115, 1, 115, 1),
(351, 240, 36, 1, 135, 0, 135, 1, 135, 1, 135, 0, 135, 1, 135, 1, 135, 0, 135, 1, 135, 1),
(352, 241, 36, 1, 100, 0, 100, 1, 100, 1, 100, 0, 100, 1, 100, 1, 100, 0, 100, 1, 100, 1),
(353, 242, 36, 1, 180, 0, 180, 1, 180, 1, 180, 0, 180, 1, 180, 1, 180, 0, 180, 1, 180, 1),
(354, 243, 36, 1, 165, 13, 165, 1, 165, 1, 165, 0, 165, 1, 165, 1, 165, 14, 165, 1, 165, 1),
(355, 116, 33, 0, 109, 0, 109, 1, 109, 1, 109, 0, 109, 1, 109, 1, 109, 0, 109, 1, 109, 1),
(356, 244, 32, 1, 205, 2, 205, 1, 205, 1, 205, 4, 205, 1, 205, 1, 205, 7, 205, 1, 205, 1),
(357, 245, 32, 1, 160, 2, 160, 1, 160, 1, 160, 4, 160, 1, 160, 1, 160, 9, 160, 1, 160, 1),
(358, 246, 30, 1, 650, 0, 650, 1, 650, 1, 650, 0, 650, 1, 650, 1, 650, 0, 650, 1, 650, 1),
(359, 246, 33, 0, 325, 0, 325, 1, 325, 1, 325, 0, 325, 1, 325, 1, 325, 0, 325, 1, 325, 1),
(360, 247, 32, 1, 58, 0, 58, 1, 58, 1, 58, 0, 58, 1, 58, 1, 58, 0, 58, 1, 58, 1),
(361, 248, 30, 1, 27, 0, 27, 1, 27, 1, 27, 0, 27, 1, 27, 1, 26, 0, 26, 1, 26, 10),
(362, 248, 33, 0, 27, 0, 27, 1, 27, 1, 27, 0, 27, 1, 27, 1, 26, 0, 26, 1, 26, 1),
(363, 249, 30, 1, 230, 0, 230, 1, 230, 1, 230, 0, 230, 1, 230, 1, 230, 0, 230, 1, 230, 1),
(364, 250, 30, 1, 330, 0, 330, 1, 330, 1, 330, 0, 330, 1, 330, 1, 330, 0, 330, 1, 330, 1),
(365, 250, 33, 0, 165, 0, 165, 1, 165, 1, 165, 0, 165, 1, 165, 1, 165, 0, 165, 1, 165, 1),
(366, 251, 30, 1, 165, 0, 165, 1, 146.12, 15, 165, 0, 165, 1, 165, 10, 165, 19, 165, 1, 146.12, 15),
(367, 252, 30, 1, 135, 0, 135, 1, 135, 1, 135, 0, 135, 1, 135, 1, 135, 0, 135, 1, 135, 1),
(368, 253, 30, 1, 185, 0, 185, 1, 185, 1, 185, 0, 185, 1, 185, 1, 185, 0, 185, 1, 185, 1),
(369, 253, 33, 0, 93, 0, 93, 1, 93, 1, 93, 0, 93, 1, 93, 1, 93, 0, 93, 1, 93, 1),
(370, 254, 30, 1, 360, 0, 360, 1, 360, 1, 360, 0, 360, 1, 360, 1, 360, 2.5, 360, 1, 360, 1),
(371, 254, 33, 0, 180, 0, 180, 1, 180, 1, 180, 0, 180, 1, 180, 1, 180, 5, 180, 1, 180, 1),
(372, 255, 30, 1, 330, 0, 330, 1, 330, 1, 330, 0, 330, 1, 330, 1, 330, 0, 330, 1, 330, 1),
(373, 255, 33, 0, 165, 0, 165, 1, 165, 1, 165, 0, 165, 1, 165, 1, 165, 0, 165, 1, 165, 1),
(374, 256, 30, 1, 292, 0, 292, 1, 292, 1, 292, 0, 292, 1, 292, 1, 292, 0, 292, 1, 292, 1),
(375, 257, 30, 1, 292, 0, 292, 1, 292, 1, 292, 0, 292, 1, 292, 1, 292, 0, 292, 1, 292, 1),
(376, 258, 30, 1, 292, 0, 292, 1, 292, 1, 292, 0, 292, 1, 292, 1, 292, 0, 292, 1, 292, 1),
(377, 258, 33, 0, 146, 0, 146, 1, 146, 1, 146, 0, 146, 1, 146, 1, 146, 0, 146, 1, 146, 1),
(378, 256, 33, 0, 146, 0, 146, 1, 146, 1, 146, 0, 146, 1, 146, 1, 146, 0, 146, 1, 146, 1),
(379, 257, 33, 0, 146, 0, 146, 1, 146, 1, 146, 0, 146, 1, 146, 1, 146, 0, 146, 1, 146, 1),
(380, 259, 30, 1, 254, 0, 254, 1, 254, 1, 254, 0, 254, 1, 254, 1, 254, 0, 254, 1, 254, 1),
(381, 259, 33, 0, 127, 0, 127, 1, 127, 1, 127, 0, 127, 1, 127, 1, 127, 0, 127, 1, 127, 1),
(382, 260, 30, 1, 255, 0, 255, 1, 255, 1, 255, 0, 255, 1, 255, 1, 255, 0, 255, 1, 255, 1),
(383, 260, 33, 0, 127.5, 0, 127.5, 1, 127.5, 1, 127.5, 0, 127.5, 1, 127.5, 1, 127.5, 0, 127.5, 1, 127.5, 1),
(384, 261, 30, 1, 280, 0, 280, 1, 280, 1, 280, 0, 280, 1, 280, 1, 280, 0, 280, 1, 280, 1),
(385, 261, 33, 0, 140, 0, 140, 1, 140, 1, 140, 0, 140, 1, 140, 1, 140, 0, 140, 1, 140, 1),
(386, 262, 30, 1, 268, 0, 268, 1, 268, 1, 268, 0, 268, 1, 268, 1, 268, 0, 268, 1, 268, 1),
(387, 262, 33, 0, 134, 0, 134, 1, 134, 1, 134, 0, 134, 1, 134, 1, 134, 0, 134, 1, 134, 1),
(388, 263, 30, 1, 255, 0, 255, 1, 255, 1, 255, 0, 255, 1, 255, 1, 255, 0, 255, 1, 255, 1),
(389, 263, 33, 0, 128, 0, 128, 1, 128, 1, 128, 0, 128, 1, 128, 1, 128, 0, 128, 1, 128, 1),
(390, 264, 30, 1, 242, 0, 242, 1, 242, 1, 242, 0, 242, 1, 242, 1, 242, 0, 242, 1, 242, 1),
(391, 264, 33, 0, 121, 0, 121, 1, 121, 1, 121, 0, 121, 1, 121, 1, 121, 0, 121, 1, 121, 1),
(392, 265, 30, 1, 338, 0, 338, 1, 338, 1, 338, 0, 338, 1, 338, 1, 338, 0, 338, 1, 338, 1),
(393, 265, 33, 0, 169, 0, 169, 1, 169, 1, 169, 0, 169, 1, 169, 1, 169, 0, 169, 1, 169, 1),
(394, 266, 30, 1, 336, 0, 336, 1, 336, 1, 336, 0, 336, 1, 336, 1, 336, 0, 336, 1, 336, 1),
(396, 267, 30, 1, 350, 0, 350, 1, 350, 1, 350, 0, 350, 1, 350, 1, 350, 0, 350, 1, 350, 1),
(398, 268, 30, 1, 350, 0, 350, 1, 350, 1, 350, 0, 350, 1, 350, 1, 350, 0, 350, 1, 350, 1),
(399, 268, 33, 0, 175, 0, 175, 1, 175, 1, 175, 0, 175, 1, 175, 1, 175, 0, 175, 1, 175, 1),
(400, 269, 30, 1, 220, 0, 220, 1, 220, 1, 220, 0, 220, 1, 220, 1, 220, 0, 220, 1, 220, 1),
(401, 269, 33, 0, 110, 0, 110, 1, 110, 1, 110, 0, 110, 1, 110, 1, 110, 0, 110, 1, 110, 1),
(402, 270, 30, 1, 220, 0, 220, 1, 220, 1, 220, 0, 220, 1, 220, 1, 220, 0, 220, 1, 220, 1),
(403, 270, 33, 0, 110, 0, 110, 1, 110, 1, 110, 0, 110, 1, 110, 1, 110, 0, 110, 1, 110, 1),
(404, 271, 30, 1, 324, 0, 324, 1, 324, 1, 324, 0, 324, 1, 324, 1, 324, 0, 324, 1, 324, 1),
(405, 272, 30, 1, 356, 0, 356, 1, 356, 1, 356, 0, 356, 1, 356, 1, 356, 0, 356, 1, 356, 1),
(406, 273, 30, 1, 280, 0, 280, 1, 280, 1, 280, 0, 280, 1, 280, 1, 280, 0, 280, 1, 280, 1),
(407, 273, 33, 0, 140, 0, 140, 1, 140, 1, 140, 0, 140, 1, 140, 1, 140, 0, 140, 1, 140, 1),
(408, 274, 30, 1, 280, 0, 280, 1, 280, 1, 280, 0, 280, 1, 280, 1, 280, 0, 280, 1, 280, 1),
(409, 274, 33, 0, 140, 0, 140, 1, 140, 1, 140, 0, 140, 1, 140, 1, 140, 0, 140, 1, 140, 1),
(410, 275, 30, 1, 280, 0, 280, 1, 280, 1, 280, 0, 280, 1, 280, 1, 280, 0, 280, 1, 280, 1),
(411, 275, 33, 0, 140, 0, 140, 1, 140, 1, 140, 0, 140, 1, 140, 1, 140, 0, 140, 1, 140, 1),
(412, 276, 30, 1, 280, 0, 280, 1, 280, 1, 280, 0, 280, 1, 280, 1, 280, 0, 280, 1, 280, 1),
(413, 276, 33, 0, 140, 0, 140, 1, 140, 1, 140, 0, 140, 1, 140, 1, 140, 0, 140, 1, 140, 1),
(414, 277, 30, 1, 280, 0, 280, 1, 280, 1, 280, 0, 280, 1, 280, 1, 280, 0, 280, 1, 280, 1),
(415, 277, 33, 0, 140, 0, 140, 1, 140, 1, 140, 0, 140, 1, 140, 1, 140, 0, 140, 1, 140, 1),
(416, 278, 30, 1, 280, 0, 280, 1, 280, 1, 280, 0, 280, 1, 280, 1, 280, 0, 280, 1, 280, 1),
(417, 278, 33, 0, 140, 0, 140, 1, 140, 1, 140, 0, 140, 1, 140, 1, 140, 0, 140, 1, 140, 1),
(418, 279, 30, 1, 322, 0, 322, 1, 322, 1, 322, 0, 322, 1, 322, 1, 322, 0, 322, 1, 322, 1),
(419, 279, 33, 0, 161, 0, 161, 1, 161, 1, 161, 0, 161, 1, 161, 1, 161, 0, 161, 1, 161, 1),
(420, 280, 30, 1, 298, 0, 298, 1, 298, 1, 298, 0, 298, 1, 298, 1, 298, 0, 298, 1, 298, 1),
(421, 280, 33, 0, 149, 0, 149, 1, 149, 1, 149, 0, 149, 1, 149, 1, 149, 0, 149, 1, 149, 1),
(422, 281, 30, 1, 310, 0, 310, 1, 310, 1, 310, 0, 310, 1, 310, 1, 310, 0, 310, 1, 310, 1),
(423, 281, 33, 0, 155, 0, 155, 1, 155, 1, 155, 0, 155, 1, 155, 1, 155, 0, 155, 1, 155, 1),
(424, 282, 30, 1, 312, 0, 312, 1, 312, 1, 312, 0, 312, 1, 312, 1, 312, 0, 312, 1, 312, 1),
(425, 282, 33, 0, 156, 0, 156, 1, 156, 1, 156, 0, 156, 1, 156, 1, 156, 0, 156, 1, 156, 1),
(426, 283, 30, 1, 330, 0, 330, 1, 330, 1, 330, 0, 330, 1, 330, 1, 330, 0, 330, 1, 330, 1),
(427, 283, 33, 0, 165, 0, 165, 1, 165, 1, 165, 0, 165, 1, 165, 1, 165, 0, 165, 1, 165, 1),
(428, 284, 30, 1, 349, 0, 349, 1, 349, 1, 349, 0, 349, 1, 349, 1, 349, 0, 349, 1, 349, 1),
(429, 285, 30, 1, 270, 0, 270, 1, 270, 1, 270, 0, 270, 1, 270, 1, 270, 0, 270, 1, 270, 1),
(430, 285, 33, 0, 135, 0, 135, 1, 135, 1, 135, 0, 135, 1, 135, 1, 135, 0, 135, 1, 135, 1),
(431, 286, 30, 1, 350, 0, 350, 1, 350, 1, 350, 0, 350, 1, 350, 1, 350, 0, 350, 1, 350, 1),
(432, 286, 33, 0, 175, 0, 175, 1, 175, 1, 175, 0, 175, 1, 175, 1, 175, 0, 175, 1, 175, 1),
(433, 287, 30, 1, 345, 0, 345, 1, 345, 1, 345, 0, 345, 1, 345, 1, 345, 0, 341, 1, 345, 1),
(434, 287, 33, 0, 173, 0, 173, 1, 173, 1, 173, 0, 173, 1, 173, 1, 173, 0, 173, 1, 173, 1),
(435, 288, 32, 1, 38, 0, 38, 1, 38, 1, 38, 0, 38, 1, 38, 1, 38, 0, 38, 1, 38, 1),
(436, 289, 36, 1, 430, 0, 430, 1, 430, 1, 430, 5, 430, 1, 430, 1, 430, 0, 430, 1, 430, 1),
(437, 290, 36, 1, 395, 0, 395, 1, 395, 1, 395, 0, 395, 1, 395, 1, 395, 0, 395, 1, 395, 1),
(438, 291, 32, 1, 70, 0, 70, 1, 70, 1, 70, 0, 70, 1, 70, 1, 70, 0, 70, 1, 70, 1),
(439, 292, 30, 1, 38, 0, 38, 1, 38, 1, 38, 0, 38, 1, 38, 1, 38, 0, 38, 1, 38, 1),
(440, 292, 33, 0, 38, 0, 38, 1, 38, 1, 38, 0, 38, 1, 38, 1, 38, 0, 38, 1, 38, 1),
(441, 293, 30, 1, 31, 0, 31, 1, 31, 1, 31, 0, 31, 1, 31, 1, 31, 0, 31, 1, 31, 1),
(442, 293, 33, 0, 31, 0, 31, 1, 31, 1, 31, 0, 31, 1, 31, 1, 31, 0, 31, 1, 31, 1),
(443, 294, 30, 1, 36, 0, 36, 1, 36, 1, 36, 0, 36, 1, 36, 1, 36, 22.5, 36, 1, 36, 1),
(444, 294, 33, 0, 36, 0, 36, 1, 36, 1, 36, 0, 36, 1, 36, 1, 36, 45, 36, 1, 36, 1),
(445, 295, 30, 1, 29, 0, 29, 1, 29, 1, 29, 0, 29, 1, 29, 1, 29, 0, 29, 1, 29, 1),
(446, 295, 33, 0, 29, 0, 29, 1, 29, 1, 29, 0, 29, 1, 29, 1, 29, 0, 29, 1, 29, 1),
(447, 296, 30, 1, 30, 0, 30, 1, 30, 1, 30, 0, 30, 1, 30, 1, 30, 0, 30, 1, 30, 1),
(448, 296, 33, 0, 30, 0, 30, 1, 30, 1, 30, 0, 30, 1, 30, 1, 30, 0, 30, 1, 30, 1),
(449, 297, 30, 1, 39, 0, 39, 1, 39, 1, 39, 0, 39, 1, 39, 1, 39, 0, 39, 1, 39, 1),
(450, 297, 33, 0, 39, 0, 39, 1, 39, 1, 39, 0, 39, 1, 39, 1, 39, 0, 39, 1, 39, 1),
(451, 298, 30, 1, 144, 0, 144, 1, 144, 1, 144, 0, 144, 1, 144, 1, 144, 0, 144, 1, 144, 1),
(452, 299, 30, 1, 295, 0, 295, 1, 295, 1, 295, 0, 295, 1, 295, 1, 295, 0, 295, 1, 295, 1),
(453, 299, 33, 0, 148, 0, 148, 1, 148, 1, 148, 0, 148, 1, 148, 1, 148, 0, 148, 1, 148, 1),
(454, 300, 32, 1, 38, 0, 38, 1, 38, 1, 38, 0, 38, 1, 38, 1, 38, 0, 38, 1, 38, 1),
(455, 301, 30, 1, 260, 0, 260, 0, 260, 0, 260, 0, 260, 0, 260, 0, 260, 0, 260, 0, 260, 0),
(456, 301, 33, 0, 130, 0, 130, 0, 130, 0, 130, 0, 130, 0, 130, 0, 130, 0, 130, 0, 130, 0),
(457, 302, 30, 1, 39, 0, 39, 1, 39, 1, 39, 0, 39, 1, 39, 1, 39, 0, 39, 1, 39, 1),
(458, 302, 33, 0, 39, 0, 39, 1, 39, 1, 39, 0, 39, 1, 39, 1, 39, 0, 39, 1, 39, 1),
(459, 303, 30, 1, 275, 0, 275, 1, 275, 1, 275, 0, 275, 1, 275, 1, 275, 0, 275, 1, 275, 1),
(460, 304, 36, 1, 269, 0, 269, 1, 269, 1, 269, 0, 269, 1, 269, 1, 269, 0, 269, 1, 269, 1),
(461, 305, 32, 1, 35, 15, 35, 1, 35, 1, 35, 16, 35, 1, 35, 1, 35, 18, 35, 1, 35, 1),
(462, 306, 30, 1, 25, 0, 25, 1, 25, 16.5, 25, 0, 25, 1, 25, 1, 25, 0, 25, 1, 25, 16.5),
(463, 306, 33, 0, 25, 0, 25, 1, 25, 1, 25, 0, 25, 1, 25, 1, 25, 0, 25, 1, 25, 1),
(464, 307, 30, 1, 26, 0, 26, 1, 25, 10, 26, 0, 26, 1, 25, 10, 26, 0, 26, 1, 26, 1),
(465, 307, 33, 0, 26, 0, 26, 1, 26, 1, 26, 0, 26, 1, 26, 1, 26, 0, 26, 1, 26, 1),
(466, 308, 32, 1, 58, 0, 58, 1, 58, 1, 58, 0, 58, 1, 58, 1, 58, 0, 58, 1, 58, 1),
(467, 309, 30, 1, 25.5, 0, 25.5, 1, 26, 1, 25.5, 0, 25.5, 1, 25.5, 1, 25.5, 0, 25.5, 1, 26, 1),
(468, 309, 33, 0, 25.5, 0, 25.5, 1, 25.5, 1, 25.5, 0, 25.5, 1, 25.5, 1, 25.5, 0, 25.5, 1, 25.5, 1),
(469, 310, 30, 1, 23, 0, 23, 1, 23, 1, 23, 0, 23, 1, 23, 1, 23, 0, 23, 1, 23, 1),
(470, 310, 33, 0, 23, 0, 23, 1, 23, 1, 23, 0, 23, 1, 23, 1, 23, 0, 23, 1, 23, 1),
(471, 311, 36, 1, 730, 0, 730, 1, 730, 1, 730, 0, 730, 1, 730, 1, 730, 0, 730, 1, 730, 1),
(472, 312, 30, 1, 517, 10.5, 517, 1, 517, 1, 517, 0, 517, 1, 517, 1, 517, 10.5, 517, 1, 517, 1),
(473, 312, 33, 0, 259, 21, 259, 1, 259, 1, 259, 0, 259, 1, 259, 1, 259, 21, 259, 1, 259, 1),
(474, 313, 30, 1, 126, 0, 126, 1, 126, 1, 126, 0, 126, 1, 126, 1, 126, 0, 126, 1, 126, 1),
(475, 314, 30, 1, 134, 0, 134, 1, 134, 1, 110, 0, 110, 1, 110, 1, 134, 0, 134, 1, 134, 1),
(476, 315, 30, 1, 371, 0, 371, 1, 371, 1, 371, 0, 371, 1, 371, 1, 371, 0, 371, 1, 371, 1),
(477, 315, 33, 0, 186, 0, 186, 1, 186, 1, 186, 0, 186, 1, 186, 1, 186, 0, 186, 1, 186, 1),
(478, 316, 32, 1, 65, 0, 65, 1, 65, 1, 65, 0, 65, 1, 65, 1, 65, 2, 65, 1, 65, 1),
(479, 317, 36, 1, 300, 0, 300, 1, 300, 25, 300, 0, 300, 1, 300, 1, 300, 3, 300, 1, 300, 25),
(480, 318, 30, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1),
(481, 319, 36, 1, 535, 0, 535, 1, 535, 1, 515, 0, 515, 1, 515, 1, 535, 0, 535, 1, 535, 1),
(482, 320, 36, 1, 270, 0, 270, 1, 270, 1, 270, 0, 270, 1, 270, 1, 270, 0, 270, 1, 270, 1),
(483, 321, 36, 1, 357, 0, 357, 1, 357, 1, 357, 0, 357, 1, 357, 1, 357, 0, 357, 1, 357, 1),
(484, 322, 30, 1, 195, 0, 195, 1, 195, 1, 195, 0, 195, 1, 195, 1, 195, 4, 195, 1, 195, 1),
(485, 322, 33, 0, 98, 0, 98, 1, 98, 1, 98, 0, 98, 1, 98, 1, 98, 8, 98, 1, 98, 1),
(486, 323, 30, 1, 374, 0, 374, 1, 374, 1, 374, 0, 374, 1, 374, 1, 374, 0, 374, 1, 374, 1),
(487, 323, 33, 0, 187, 0, 187, 1, 187, 1, 187, 0, 187, 1, 187, 1, 187, 0, 187, 1, 187, 1),
(488, 324, 30, 1, 434, 0, 434, 1, 434, 1, 434, 0, 434, 1, 434, 1, 434, 0, 434, 1, 434, 1),
(489, 324, 33, 0, 217, 0, 217, 1, 217, 1, 217, 0, 217, 1, 217, 1, 217, 0, 217, 1, 217, 1),
(490, 120, 33, 0, 137, 0, 137, 1, 137, 1, 137, 0, 137, 1, 137, 1, 137, 0, 137, 1, 137, 1),
(491, 325, 30, 1, 250, 0, 250, 1, 250, 1, 250, 0, 250, 1, 250, 1, 250, 2.5, 250, 1, 250, 1),
(492, 325, 30, 0, 125, 0, 125, 1, 125, 1, 125, 0, 125, 1, 125, 1, 125, 2.5, 125, 1, 125, 1),
(493, 326, 30, 1, 27, 0, 27, 1, 27, 1, 27, 0, 27, 1, 27, 1, 27, 0, 27, 1, 25, 10),
(494, 326, 33, 0, 27, 0, 27, 1, 27, 1, 27, 0, 27, 1, 27, 1, 27, 0, 27, 1, 27, 1),
(496, 327, 36, 1, 363, 0, 363, 0, 363, 0, 363, 0, 363, 0, 363, 0, 363, 0, 363, 0, 363, 0),
(497, 328, 30, 1, 274, 0, 274, 1, 274, 1, 274, 0, 274, 1, 274, 1, 274, 0, 274, 1, 274, 1),
(498, 329, 30, 1, 40, 0, 40, 1, 40, 1, 40, 0, 40, 1, 40, 1, 40, 0, 40, 1, 40, 1),
(499, 330, 36, 1, 230, 0, 230, 1, 230, 1, 230, 0, 230, 1, 230, 1, 230, 11, 230, 1, 230, 1),
(500, 331, 32, 1, 115, 19, 115, 1, 105, 3, 110, 19, 110, 1, 110, 1, 115, 19, 115, 1, 105, 3),
(501, 332, 32, 1, 95, 1, 95, 1, 95, 1, 95, 2, 95, 1, 95, 1, 95, 14, 95, 1, 95, 1),
(502, 333, 30, 1, 382, 0, 382, 1, 382, 1, 382, 0, 382, 1, 382, 1, 382, 0, 382, 1, 382, 1),
(503, 333, 33, 0, 191, 0, 191, 1, 191, 1, 191, 0, 191, 1, 191, 1, 191, 0, 191, 1, 191, 1),
(504, 334, 30, 1, 325, 0.5, 325, 1, 325, 1, 325, 0, 325, 1, 325, 1, 325, 4.5, 325, 1, 325, 1),
(505, 334, 33, 0, 162.5, 1, 162.5, 1, 162.5, 1, 162.5, 0, 162.5, 1, 162.5, 1, 162.5, 9, 162.5, 1, 162.5, 1),
(506, 335, 30, 1, 375, 0, 375, 0, 375, 0, 375, 0, 375, 0, 375, 0, 375, 0, 375, 0, 375, 0),
(507, 335, 33, 0, 188, 0, 188, 0, 188, 0, 188, 0, 188, 0, 188, 0, 188, 0, 188, 0, 188, 0),
(508, 336, 32, 1, 60, 0, 60, 1, 60, 1, 60, 0, 60, 1, 60, 1, 60, 0, 60, 1, 60, 1),
(509, 337, 30, 1, 80, 0, 80, 0, 80, 0, 80, 0, 80, 0, 80, 0, 80, 0, 80, 0, 80, 0),
(510, 338, 30, 1, 140, 0, 140, 0, 140, 0, 140, 0, 140, 0, 140, 0, 140, 0, 140, 0, 140, 0),
(511, 339, 36, 1, 415, 0, 415, 0, 415, 0, 415, 0, 415, 0, 415, 0, 415, 0, 415, 0, 415, 0),
(512, 340, 30, 1, 465, 0, 465, 0, 465, 0, 465, 0, 465, 0, 465, 0, 465, 0, 465, 0, 465, 0),
(513, 340, 33, 0, 233, 0, 233, 0, 233, 0, 233, 0, 233, 0, 233, 0, 233, 0, 233, 0, 233, 0),
(514, 341, 30, 1, 120, 0, 120, 1, 120, 1, 120, 0, 120, 1, 120, 1, 120, 0, 120, 1, 120, 1),
(515, 342, 30, 1, 420, 0, 420, 0, 375, 2, 420, 0, 420, 0, 375, 2, 420, 0, 420, 0, 375, 2),
(516, 342, 33, 0, 210, 0, 210, 0, 210, 0, 210, 0, 210, 0, 210, 0, 210, 0, 210, 0, 210, 0),
(517, 343, 30, 1, 420, 0, 420, 0, 420, 0, 420, 0, 420, 0, 420, 0, 420, 0, 420, 0, 420, 0),
(518, 343, 33, 0, 210, 0, 210, 0, 210, 0, 210, 0, 210, 0, 210, 0, 210, 0, 210, 0, 210, 0),
(519, 344, 32, 1, 38, 0, 38, 0, 38, 0, 38, 0, 38, 0, 38, 0, 38, 0, 38, 0, 38, 0),
(520, 345, 32, 1, 42, 0, 42, 0, 42, 0, 42, 0, 42, 0, 42, 0, 42, 0, 42, 0, 42, 0),
(521, 346, 30, 1, 154, 0, 154, 1, 154, 1, 154, 0, 154, 1, 154, 1, 154, 0, 154, 1, 154, 1),
(522, 347, 30, 1, 115, 0, 115, 0, 115, 0, 115, 0, 115, 0, 115, 0, 115, 0, 115, 0, 115, 0),
(523, 348, 36, 1, 580, 0, 580, 1, 580, 1, 580, 0, 580, 1, 580, 1, 580, 5, 580, 1, 580, 1),
(524, 349, 30, 1, 410, 0, 410, 1, 410, 1, 410, 0, 410, 1, 410, 1, 410, 0, 410, 1, 410, 1),
(525, 351, 30, 1, 570, 0, 570, 1, 570, 1, 570, 0, 570, 1, 570, 1, 570, 0, 570, 1, 570, 1),
(526, 352, 30, 1, 570, 0, 570, 0, 570, 0, 570, 0, 570, 0, 570, 0, 570, 0, 570, 0, 570, 0),
(527, 352, 33, 0, 285, 0, 285, 0, 285, 0, 285, 0, 285, 0, 285, 0, 285, 0, 285, 0, 285, 0),
(528, 353, 30, 1, 570, 0, 570, 0, 570, 0, 570, 0, 570, 0, 570, 0, 570, 0, 570, 0, 570, 0),
(529, 353, 33, 0, 285, 0, 285, 0, 285, 0, 285, 0, 285, 0, 285, 0, 285, 0, 285, 0, 285, 0),
(530, 351, 33, 0, 285, 0, 285, 1, 285, 1, 285, 0, 285, 1, 285, 1, 285, 0, 285, 1, 285, 1),
(531, 354, 30, 1, 570, 0, 570, 1, 570, 1, 570, 0, 570, 1, 570, 1, 570, 0, 570, 1, 570, 1),
(532, 354, 33, 0, 285, 0, 285, 1, 285, 1, 285, 0, 285, 1, 285, 1, 285, 0, 285, 1, 285, 1),
(533, 355, 30, 1, 570, 0, 570, 1, 570, 1, 570, 0, 570, 1, 570, 1, 570, 0, 570, 1, 570, 1),
(534, 355, 33, 0, 285, 0, 285, 1, 285, 1, 285, 0, 285, 1, 285, 1, 285, 0, 285, 1, 285, 1),
(535, 356, 30, 1, 570, 0, 570, 1, 570, 1, 570, 0, 570, 1, 570, 1, 570, 0, 570, 1, 570, 1),
(536, 356, 33, 0, 285, 0, 285, 1, 285, 1, 285, 0, 285, 1, 285, 1, 285, 0, 285, 1, 285, 1),
(537, 357, 30, 1, 310, 0, 310, 0, 310, 0, 310, 0, 310, 0, 310, 0, 310, 0, 310, 0, 310, 0),
(538, 357, 33, 0, 155, 0, 155, 0, 155, 0, 155, 0, 155, 0, 155, 0, 155, 0, 155, 0, 155, 0),
(539, 358, 30, 1, 128, 0, 128, 0, 128, 0, 128, 0, 128, 0, 128, 0, 128, 0, 128, 0, 128, 0),
(540, 359, 30, 1, 198, 0, 198, 0, 198, 0, 198, 0, 198, 0, 198, 0, 198, 0, 198, 0, 198, 0),
(541, 360, 30, 1, 797, 0, 797, 0, 797, 0, 797, 0, 797, 0, 797, 0, 797, 0, 797, 0, 797, 0),
(542, 360, 33, 0, 399, 0, 399, 0, 399, 0, 399, 0, 399, 0, 399, 0, 399, 0, 399, 0, 399, 0),
(543, 361, 36, 1, 475, 0, 475, 0, 475, 0, 475, 0, 475, 0, 475, 0, 475, 0, 475, 0, 475, 0),
(544, 362, 36, 1, 195, 0, 195, 0, 178.2, 5, 195, 0, 195, 0, 195, 0, 195, 5, 195, 0, 178.2, 5),
(545, 363, 36, 1, 170, 0, 170, 0, 150.2, 5, 170, 0, 170, 0, 170, 0, 170, 3, 170, 0, 150.2, 5),
(546, 364, 30, 1, 482, 0, 482, 1, 482, 1, 482, 0, 482, 1, 482, 1, 482, 0, 482, 1, 482, 1),
(547, 364, 33, 0, 241, 0, 241, 0, 241, 0, 241, 0, 241, 0, 241, 0, 241, 0, 241, 0, 241, 0),
(548, 365, 36, 1, 420, 0, 420, 1, 420, 1, 420, 0, 420, 1, 420, 1, 420, 0, 420, 1, 420, 1),
(549, 366, 36, 1, 585, 9, 585, 1, 585, 1, 592, 13, 592, 1, 592, 1, 585, 10, 585, 1, 585, 1),
(550, 367, 30, 1, 94, 0, 94, 1, 94, 1, 94, 0, 94, 1, 94, 1, 94, 0, 94, 1, 94, 1),
(551, 367, 33, 0, 47, 0, 47, 1, 47, 1, 47, 0, 47, 1, 47, 1, 47, 0, 47, 1, 47, 1),
(552, 368, 30, 1, 380, 0, 380, 1, 380, 1, 380, 0.5, 380, 1, 380, 1, 380, 0, 380, 1, 380, 1),
(553, 368, 33, 0, 190, 0, 190, 1, 190, 1, 190, 1, 190, 1, 190, 1, 190, 0, 190, 1, 190, 1),
(554, 369, 30, 1, 250, 0, 250, 1, 250, 1, 250, 0, 250, 1, 250, 1, 250, 4, 250, 1, 250, 1),
(555, 369, 33, 0, 125, 0, 125, 1, 125, 1, 125, 0, 125, 1, 125, 1, 125, 8, 125, 1, 125, 1),
(556, 370, 30, 1, 1020, 0, 1020, 1, 1020, 1, 1020, 0, 1020, 1, 1020, 1, 1020, 0, 1020, 1, 1020, 1),
(557, 371, 30, 1, 516, 0, 516, 1, 516, 1, 516, 0, 516, 1, 516, 1, 516, 0, 516, 1, 516, 1),
(558, 372, 30, 1, 520, 0, 520, 1, 520, 1, 520, 0, 520, 1, 520, 1, 520, 0, 520, 1, 520, 1),
(559, 373, 30, 1, 470, 0, 470, 1, 470, 1, 470, 0, 470, 1, 470, 1, 470, 2.5, 470, 1, 470, 1),
(560, 373, 33, 0, 235, 0, 235, 1, 235, 1, 235, 0, 235, 1, 235, 1, 235, 5, 235, 1, 235, 1),
(561, 376, 30, 1, 135, 0, 135, 1, 135, 1, 135, 0, 135, 1, 135, 1, 135, 1, 135, 1, 135, 1),
(562, 378, 30, 1, 145, 0, 145, 1, 145, 1, 145, 0, 145, 1, 145, 1, 145, 0, 145, 1, 145, 1),
(563, 378, 33, 0, 73, 0, 73, 1, 73, 1, 73, 0, 73, 1, 73, 1, 73, 0, 73, 1, 73, 1),
(564, 376, 33, 0, 68, 0, 68, 1, 68, 1, 68, 0, 68, 1, 68, 1, 68, 2, 68, 1, 68, 1),
(565, 379, 30, 1, 148, 0, 148, 1, 148, 1, 148, 0, 148, 1, 148, 1, 148, 0, 148, 1, 148, 1),
(566, 380, 30, 1, 430, 0, 430, 1, 430, 1, 430, 0, 430, 1, 430, 1, 430, 0, 430, 1, 430, 1),
(567, 380, 33, 0, 215, 0, 215, 1, 215, 1, 215, 0, 215, 1, 215, 1, 215, 0, 215, 1, 215, 1),
(568, 381, 32, 1, 400, 0, 400, 1, 400, 1, 400, 0, 400, 1, 400, 1, 400, 8, 400, 1, 400, 1),
(569, 382, 30, 1, 412, 0, 412, 1, 412, 1, 412, 0, 412, 1, 412, 1, 412, 0.5, 412, 1, 412, 1),
(570, 383, 32, 1, 65, 0, 65, 1, 65, 1, 0, 0, 0, 1, 0, 1, 65, 6, 65, 1, 65, 1),
(571, 383, 33, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 12, 0, 1, 0, 1),
(572, 382, 33, 0, 206, 0, 206, 1, 206, 1, 206, 0, 206, 1, 206, 1, 206, 1, 206, 1, 206, 1),
(573, 384, 30, 1, 34, 0, 34, 1, 33, 23, 34, 0, 34, 1, 34, 1, 34, 23, 34, 1, 33, 23);
INSERT INTO `sproductosub` (`subId`, `productoaddId`, `PresentacionId`, `tipo`, `precio`, `stok`, `precio_mm`, `cantidad_mm`, `precio_m`, `cantidad_m`, `precio2`, `stok2`, `precio_mm2`, `cantidad_mm2`, `precio_m2`, `cantidad_m2`, `precio3`, `stok3`, `precio_mm3`, `cantidad_mm3`, `precio_m3`, `cantidad_m3`) VALUES
(574, 384, 33, 0, 34, 0, 34, 1, 34, 1, 34, 0, 34, 1, 34, 1, 34, 46, 34, 1, 34, 1),
(575, 385, 30, 1, 25, 0, 25, 1, 25, 1, 25, 0, 25, 1, 24.5, 9, 25, 0, 25, 1, 25, 1),
(576, 385, 33, 0, 25, 0, 25, 1, 25, 1, 25, 0, 25, 1, 25, 1, 25, 0, 25, 1, 25, 1),
(577, 386, 30, 1, 440, 0, 440, 1, 420.1, 5, 440, 0, 440, 1, 440, 1, 440, 0, 440, 1, 420.1, 5),
(578, 386, 33, 0, 220, 0, 220, 1, 220, 1, 220, 0, 220, 1, 220, 1, 220, 0, 220, 1, 220, 1),
(579, 387, 32, 1, 40, 0, 40, 1, 40, 1, 40, 0, 40, 1, 40, 1, 40, 0, 40, 1, 40, 1),
(580, 388, 30, 1, 115, 0, 115, 1, 110.1, 24, 110, 1, 110, 1, 110, 1, 115, 24, 115, 1, 110.1, 24),
(581, 389, 30, 1, 460, 0, 460, 1, 435.1, 6, 460, 0, 460, 1, 460, 1, 460, 6, 460, 1, 435.1, 6),
(582, 389, 33, 0, 230, 0, 230, 1, 230, 1, 230, 0, 230, 1, 230, 1, 230, 12, 230, 1, 230, 1),
(583, 390, 30, 1, 490, 13, 490, 1, 490, 1, 490, 13, 490, 1, 490, 1, 490, 13, 490, 1, 490, 1),
(584, 391, 36, 1, 774, 7, 774, 1, 774, 1, 774, 10, 774, 1, 774, 1, 774, 10, 774, 1, 774, 1),
(585, 392, 36, 1, 492, 15, 492, 1, 492, 1, 492, 15, 492, 1, 492, 1, 492, 15, 492, 1, 492, 1),
(586, 393, 30, 1, 28, 0, 28, 1, 28, 1, 30, 0, 30, 1, 29.5, 5, 28, 15.5, 28, 1, 28, 1),
(587, 393, 33, 0, 28, 0, 28, 1, 28, 1, 30, 0, 30, 1, 30, 1, 28, 31, 28, 1, 28, 1),
(588, 394, 36, 1, 395, 0, 395, 1, 395, 1, 395, 0, 395, 1, 395, 1, 395, 0, 395, 1, 395, 1),
(589, 395, 30, 1, 30.5, 0, 30.5, 1, 30.5, 1, 30.5, 0, 30.5, 1, 30.5, 1, 30.5, 0, 30.5, 1, 30.5, 1),
(590, 395, 33, 0, 30.5, 0, 30.5, 1, 30.5, 1, 30.5, 0, 30.5, 1, 30.5, 1, 30.5, 0, 30.5, 1, 30.5, 1),
(591, 23, 33, 0, 175, 26, 175, 1, 175, 1, 200, 26, 200, 1, 200, 1, 175, 26, 175, 1, 175, 1),
(592, 396, 32, 1, 100, 103, 90, 10, 80, 20, 100, 99, 90, 10, 80, 20, 100, 100, 90, 10, 80, 20),
(593, 397, 30, 1, 29, 0, 29, 1, 29, 1, 29, 0, 29, 1, 29, 1, 29, 3.5, 29, 1, 29, 1),
(594, 397, 33, 0, 29, 0, 29, 1, 29, 1, 29, 0, 29, 1, 29, 1, 29, 7, 29, 1, 29, 1),
(595, 398, 30, 1, 26, 0, 26, 1, 23.1, 60, 26, 0, 26, 1, 26, 1, 26, 60, 26, 1, 23.1, 60),
(596, 398, 33, 0, 26, 0, 26, 1, 26, 1, 26, 0, 26, 1, 26, 1, 26, 120, 26, 1, 26, 1),
(597, 399, 30, 1, 27.5, 0, 27.5, 1, 27.5, 16, 27.5, 0, 27.5, 1, 27.5, 1, 27.5, 0, 27.5, 1, 27.5, 16),
(598, 399, 33, 0, 27.5, 0, 27.5, 1, 27.5, 1, 27.5, 0, 27.5, 1, 27.5, 1, 27.5, 0, 27.5, 1, 27.5, 1),
(599, 400, 30, 1, 6, 0, 6, 1, 6, 1, 6, 0, 6, 1, 6, 1, 6, 0, 6, 1, 6, 1),
(600, 401, 30, 1, 26, 0, 26, 1, 26.2, 14, 26, 0, 26, 1, 25, 3, 26, 0, 26, 1, 26.2, 14),
(601, 401, 33, 0, 26, 0, 26, 1, 26, 1, 26, 0, 26, 1, 26, 1, 26, 0, 26, 1, 26, 1),
(602, 402, 30, 1, 26, 0, 26, 1, 24, 24, 26, 0, 26, 1, 26, 1, 26, 24, 26, 1, 24, 24),
(603, 402, 33, 0, 26, 0, 26, 1, 26, 1, 26, 0, 26, 1, 26, 1, 26, 48, 26, 1, 26, 1),
(604, 403, 30, 1, 30, 0, 30, 1, 30, 1, 30, 0, 30, 1, 29, 14, 30, 0, 30, 1, 30, 1),
(605, 403, 33, 0, 30, 0, 30, 1, 30, 1, 30, 0, 30, 1, 30, 1, 30, 0, 30, 1, 30, 1),
(606, 404, 30, 1, 32.5, 0, 32.5, 1, 32.5, 1, 32.5, 0, 32.5, 1, 32, 10, 32.5, 0, 32.5, 1, 32.5, 1),
(607, 404, 33, 0, 32.5, 0, 32.5, 1, 32.5, 1, 32.5, 0, 32.5, 1, 32.5, 1, 32.5, 0, 32.5, 1, 32.5, 1),
(608, 405, 30, 1, 29, 0, 29, 1, 29, 1, 29, 0, 29, 1, 29, 290, 29, 0, 29, 1, 29, 1),
(609, 405, 33, 0, 29, 0, 29, 1, 29, 1, 29, 0, 29, 1, 29, 1, 29, 0, 29, 1, 29, 1),
(610, 406, 32, 1, 35, 7, 35, 1, 35, 1, 35, 1, 35, 1, 35, 1, 35, 11, 35, 1, 35, 1),
(611, 407, 30, 1, 320, 4, 320, 1, 320, 1, 320, 1, 320, 1, 320, 1, 320, 7, 320, 1, 320, 1),
(612, 408, 32, 1, 40, 0, 40, 1, 40, 1, 40, 0, 40, 1, 40, 1, 40, 0, 40, 1, 40, 1),
(613, 409, 30, 1, 695, 0, 695, 1, 695, 1, 695, 0, 695, 1, 695, 1, 695, 0, 695, 1, 695, 1),
(614, 409, 33, 0, 347, 0, 347, 1, 347, 1, 347, 0, 347, 1, 347, 1, 347, 0, 347, 1, 347, 1),
(615, 410, 30, 1, 1600, 0, 1600, 1, 1600, 1, 1600, 0, 1600, 1, 1600, 1, 1600, 0, 1600, 1, 1600, 1),
(616, 410, 33, 0, 800, 0, 800, 1, 800, 1, 800, 0, 800, 1, 800, 1, 800, 0, 800, 1, 800, 1),
(617, 411, 32, 1, 80, 10, 80, 1, 80, 1, 80, 0, 80, 1, 80, 1, 80, 15, 80, 1, 80, 1),
(618, 412, 32, 1, 30, 0, 30, 1, 30, 1, 30, 0, 30, 1, 30, 1, 30, 30, 30, 1, 30, 1),
(619, 413, 30, 1, 134, 0, 134, 1, 134, 1, 134, 0, 134, 1, 124, 10, 134, 0, 134, 1, 134, 1),
(620, 414, 36, 1, 832, 0, 832, 0, 832, 0, 832, 10, 832, 0, 832, 0, 832, 0, 832, 0, 832, 0),
(621, 415, 36, 1, 600, 0, 600, 1, 600, 1, 595, 23, 595, 1, 595, 1, 600, 0, 600, 1, 600, 1),
(622, 416, 36, 1, 510, 0, 510, 1, 510, 1, 510, 0, 510, 1, 510, 1, 510, 10, 510, 1, 510, 1),
(623, 417, 30, 1, 173, 84, 173, 1, 173, 1, 173, 77, 173, 1, 173, 1, 173, 94, 173, 1, 173, 1),
(624, 418, 36, 1, 950, 0, 950, 1, 950, 1, 950, 0, 950, 1, 950, 1, 950, 0, 950, 1, 950, 1),
(625, 419, 30, 1, 35, 0, 35, 1, 35, 1, 35, 0, 35, 1, 35, 1, 35, 0, 35, 1, 35, 1),
(626, 419, 33, 0, 35, 0, 35, 1, 35, 1, 35, 0, 35, 1, 35, 1, 35, 0, 35, 1, 35, 1),
(627, 420, 30, 1, 225, 0, 225, 1, 225, 1, 225, 0, 225, 1, 225, 1, 225, 1, 225, 1, 225, 1),
(628, 420, 33, 0, 113, 0, 113, 0, 113, 0, 113, 0, 113, 0, 113, 0, 113, 2, 113, 0, 113, 0),
(629, 421, 32, 1, 28, 0, 28, 1, 28, 1, 28, 0, 28, 1, 28, 1, 28, 9, 28, 1, 28, 1),
(630, 422, 36, 1, 445, 0, 445, 1, 445, 1, 445, 0, 445, 1, 445, 1, 445, 4, 445, 1, 445, 1),
(631, 423, 30, 1, 35, 0, 35, 1, 35, 1, 35, 0, 35, 1, 35, 1, 35, 19, 35, 1, 35, 1),
(632, 423, 33, 0, 35, 0, 35, 1, 35, 1, 35, 0, 35, 1, 35, 1, 35, 38, 35, 1, 35, 1),
(633, 424, 30, 1, 35, 0, 35, 1, 35, 1, 35, 0, 35, 1, 34, 45, 35, 0, 35, 1, 35, 1),
(634, 424, 33, 0, 35, 0, 35, 1, 35, 1, 35, 0, 35, 1, 35, 1, 35, 0, 35, 1, 35, 1),
(635, 425, 30, 1, 278, 1, 278, 1, 278, 1, 278, 6, 278, 1, 278, 1, 278, 0, 278, 1, 278, 1),
(636, 426, 30, 1, 40, 0, 40, 1, 40, 1, 40, 0, 40, 1, 40, 1, 40, 5.5, 40, 1, 40, 1),
(637, 426, 33, 0, 40, 0, 40, 1, 40, 1, 40, 0, 40, 1, 40, 1, 40, 11, 40, 1, 40, 1),
(638, 427, 30, 1, 460, 0, 460, 1, 460, 1, 460, 0, 460, 1, 460, 1, 460, 18, 460, 1, 460, 1),
(639, 427, 33, 0, 230, 0, 230, 1, 230, 1, 230, 0, 230, 1, 230, 1, 230, 36, 230, 1, 230, 1),
(640, 428, 30, 1, 33, 0, 33, 1, 33, 1, 33, 0, 33, 1, 33, 1, 33, 13.5, 33, 1, 33, 1),
(641, 428, 33, 0, 33, 0, 33, 1, 33, 1, 33, 0, 33, 1, 33, 1, 33, 27, 33, 1, 33, 1),
(642, 429, 30, 1, 33, 0, 33, 1, 33, 1, 33, 0, 33, 1, 32, 8, 33, 0, 33, 1, 33, 1),
(643, 429, 33, 0, 33, 0, 33, 1, 33, 1, 33, 0, 33, 1, 33, 1, 33, 0, 33, 1, 33, 1),
(644, 430, 30, 1, 34, 0, 34, 1, 34, 1, 34, 0, 34, 1, 34, 1, 34, 0, 34, 1, 34, 1),
(645, 430, 33, 0, 34, 0, 34, 1, 34, 1, 34, 0, 34, 1, 34, 1, 34, 0, 34, 1, 34, 1),
(646, 431, 30, 1, 470, 0, 470, 1, 470, 1, 470, 0, 470, 1, 470, 1, 470, 6, 470, 1, 470, 1),
(647, 431, 33, 0, 235, 0, 235, 1, 235, 1, 235, 0, 235, 1, 235, 1, 235, 12, 235, 1, 235, 1),
(648, 432, 30, 1, 35, 0, 35, 1, 34.5, 4, 35, 0, 35, 1, 35, 1, 35, 0, 35, 1, 34.5, 4),
(649, 432, 33, 0, 35, 0, 35, 1, 35, 1, 35, 0, 35, 1, 35, 1, 35, 0, 35, 1, 35, 1),
(650, 433, 30, 1, 33, 0, 33, 1, 33, 1, 33, 0, 33, 1, 33, 1, 33, 19.5, 33, 1, 33, 1),
(651, 433, 33, 0, 33, 0, 33, 1, 33, 1, 33, 0, 33, 1, 33, 1, 33, 39, 33, 1, 33, 1),
(652, 434, 30, 1, 36, 0, 36, 1, 36, 1, 36, 0, 36, 1, 35, 5, 36, 0, 36, 1, 36, 1),
(653, 434, 33, 0, 36, 0, 36, 1, 36, 1, 36, 0, 36, 1, 36, 1, 36, 0, 36, 1, 36, 1),
(654, 435, 30, 1, 39, 0, 39, 1, 39, 1, 39, 0, 39, 1, 39, 1, 39, 12, 39, 1, 39, 1),
(655, 435, 33, 0, 39, 0, 39, 1, 39, 1, 39, 0, 39, 1, 39, 1, 39, 24, 39, 1, 39, 1),
(656, 436, 30, 1, 36, 0, 36, 1, 35.5, 2, 36, 0, 36, 1, 34, 20, 36, 0, 36, 1, 35.5, 2),
(657, 436, 33, 0, 36, 0, 36, 1, 36, 1, 36, 0, 36, 1, 36, 1, 36, 0, 36, 1, 36, 1),
(658, 437, 30, 1, 34, 0, 34, 1, 34, 1, 34, 18, 34, 1, 34, 1, 34, 0, 34, 1, 34, 1),
(659, 437, 33, 0, 34, 0, 34, 1, 34, 1, 34, 36, 34, 1, 34, 1, 34, 0, 34, 1, 34, 1),
(660, 438, 30, 1, 140, 0, 140, 1, 140, 1, 140, 0, 140, 1, 140, 1, 140, 0, 140, 1, 140, 1),
(661, 439, 30, 1, 34, 0, 34, 1, 34, 1, 34, 0, 34, 1, 34, 1, 34, 5, 34, 1, 34, 1),
(662, 439, 33, 0, 34, 0, 34, 1, 34, 1, 34, 0, 34, 1, 34, 1, 34, 10, 34, 1, 34, 1),
(663, 440, 30, 1, 37.5, 0, 37.5, 1, 37.5, 1, 38, 0, 38, 1, 38, 1, 37.5, 6, 37.5, 1, 37.5, 1),
(664, 440, 33, 0, 37.5, 0, 37.5, 1, 37.5, 1, 38, 0, 38, 1, 38, 1, 37.5, 12, 37.5, 1, 37.5, 1),
(665, 441, 30, 1, 32, 0, 32, 1, 32, 1, 32, 0, 32, 1, 32, 1, 32, 9.5, 32, 1, 32, 1),
(666, 441, 33, 0, 32, 0, 32, 1, 32, 1, 32, 0, 32, 1, 32, 1, 32, 19, 32, 1, 32, 1),
(667, 442, 30, 1, 34, 0, 34, 1, 34, 1, 34, 0, 34, 1, 34, 1, 34, 24.5, 34, 1, 34, 1),
(668, 442, 33, 0, 34, 0, 34, 1, 34, 1, 34, 0, 34, 1, 34, 1, 34, 49, 34, 1, 34, 1),
(669, 443, 30, 1, 34, 0, 34, 1, 34, 1, 34, 0, 34, 1, 34, 1, 34, 0, 34, 1, 34, 1),
(670, 443, 33, 0, 34, 0, 34, 1, 34, 1, 34, 0, 34, 1, 34, 1, 34, 0, 34, 1, 34, 1),
(671, 444, 30, 1, 34, 0, 34, 1, 34, 1, 34, 0, 34, 1, 34, 1, 34, 0, 34, 1, 34, 1),
(672, 444, 33, 0, 34, 0, 34, 1, 34, 1, 34, 0, 34, 1, 34, 1, 34, 0, 34, 1, 34, 1),
(673, 445, 30, 1, 38, 0, 38, 1, 38, 1, 38, 0, 38, 1, 38, 1, 38, 0, 38, 1, 38, 1),
(674, 445, 33, 0, 38, 0, 38, 1, 38, 1, 38, 0, 38, 1, 38, 1, 38, 0, 38, 1, 38, 1),
(675, 446, 32, 1, 70, 0, 70, 1, 70, 1, 70, 3, 70, 1, 70, 1, 70, 2, 70, 1, 70, 1),
(676, 447, 36, 1, 570, 11, 570, 1, 570, 1, 570, 0, 570, 1, 570, 1, 570, 11, 570, 1, 570, 1),
(677, 447, 33, 0, 285, 22, 285, 1, 285, 1, 285, 0, 285, 1, 285, 1, 285, 22, 285, 1, 285, 1),
(678, 448, 36, 1, 300, 0, 300, 1, 300, 1, 300, 0, 300, 1, 300, 1, 300, 13, 300, 1, 300, 1),
(679, 449, 30, 1, 36, 0, 36, 1, 36, 1, 36, 0, 36, 1, 36, 1, 36, 21.5, 36, 1, 36, 1),
(680, 449, 33, 0, 36, 0, 36, 1, 36, 1, 36, 0, 36, 1, 36, 1, 36, 43, 36, 1, 36, 1),
(681, 450, 36, 1, 825, 0, 825, 1, 825, 1, 825, 0, 825, 1, 825, 1, 825, 0, 825, 1, 825, 1),
(682, 451, 36, 1, 910, 0, 910, 1, 910, 1, 910, 0, 910, 1, 910, 1, 910, 5, 910, 1, 910, 1),
(683, 453, 36, 1, 730, 0, 730, 1, 730, 1, 730, 0, 730, 1, 730, 1, 730, 15, 730, 1, 730, 1),
(684, 456, 36, 1, 710, 7, 710, 1, 710, 1, 710, 0, 710, 1, 710, 1, 710, 10, 710, 1, 710, 1),
(685, 457, 36, 1, 615, 2, 615, 1, 615, 1, 615, 0, 615, 1, 615, 1, 615, 5, 615, 1, 615, 1),
(686, 458, 30, 1, 37, 0, 37, 1, 37, 1, 37, 0, 37, 1, 37, 1, 37, 0, 37, 1, 37, 1),
(687, 458, 33, 0, 37, 0, 37, 1, 37, 1, 37, 0, 37, 1, 37, 1, 37, 0, 37, 1, 37, 1),
(688, 459, 30, 1, 468, 0, 468, 1, 468, 1, 468, 0, 468, 1, 468, 1, 468, 4, 468, 1, 468, 1),
(689, 460, 36, 1, 1027, 7, 1027, 1, 1027, 1, 1027, 5, 1027, 1, 1027, 1, 1027, 0, 1027, 1, 1027, 1),
(690, 461, 32, 1, 30, 19, 30, 1, 30, 1, 30, 2, 30, 1, 30, 1, 30, 38, 30, 1, 30, 1),
(691, 462, 30, 1, 410, 5, 410, 1, 410, 1, 410, 0, 410, 1, 410, 1, 410, 6, 410, 1, 410, 1),
(692, 462, 33, 0, 205, 10, 205, 1, 205, 1, 205, 0, 205, 1, 205, 1, 205, 12, 205, 1, 205, 1),
(693, 463, 30, 1, 385, 3, 385, 1, 385, 1, 385, 0, 385, 1, 385, 1, 385, 4, 385, 1, 385, 1),
(694, 463, 33, 0, 192.5, 6, 192.5, 1, 192.5, 1, 192.5, 0, 192.5, 1, 192.5, 1, 192.5, 8, 192.5, 1, 192.5, 1),
(695, 464, 32, 1, 175, 14, 175, 1, 175, 1, 175, 0, 175, 1, 175, 1, 175, 23, 175, 1, 175, 1),
(696, 465, 32, 1, 120, 0, 120, 1, 120, 1, 120, 0, 120, 1, 120, 1, 120, 30, 120, 1, 120, 1),
(697, 466, 30, 1, 46, 0, 46, 1, 46, 1, 46, 0, 46, 1, 45, 7.5, 46, 0, 46, 1, 46, 1),
(698, 466, 33, 0, 46, 0, 46, 1, 46, 1, 46, 0, 46, 1, 46, 1, 46, 0, 46, 1, 46, 1),
(699, 467, 30, 1, 37, 0, 37, 1, 37, 1, 37, 0, 37, 1, 37, 1, 37, 8, 37, 1, 37, 1),
(700, 467, 33, 0, 37, 0, 37, 1, 37, 1, 37, 0, 37, 1, 37, 1, 37, 16, 37, 1, 37, 1),
(701, 468, 36, 1, 395, 2, 395, 1, 395, 1, 395, 4, 395, 1, 395, 1, 395, 2, 395, 1, 395, 1),
(702, 469, 32, 1, 640, 0, 640, 1, 640, 1, 635, 0, 635, 1, 635, 1, 640, 0, 640, 1, 640, 1),
(703, 470, 30, 1, 390, 0, 390, 1, 390, 1, 390, 0, 390, 1, 390, 1, 390, 2.5, 390, 1, 390, 1),
(704, 470, 33, 0, 195, 0, 195, 1, 195, 1, 195, 0, 195, 1, 195, 1, 195, 5, 195, 1, 195, 1),
(705, 471, 30, 1, 390, 0, 390, 1, 390, 1, 390, 0, 390, 1, 390, 1, 390, 2, 390, 1, 390, 1),
(706, 471, 33, 0, 195, 0, 195, 1, 195, 1, 195, 0, 195, 1, 195, 1, 195, 4, 195, 1, 195, 1),
(707, 472, 30, 1, 390, 0, 390, 1, 390, 1, 390, 0, 390, 1, 390, 1, 390, 2, 390, 1, 390, 1),
(708, 472, 33, 0, 195, 0, 195, 1, 195, 1, 195, 0, 195, 1, 195, 1, 195, 4, 195, 1, 195, 1),
(709, 473, 32, 1, 160, 0, 160, 1, 160, 1, 160, 0, 160, 1, 160, 1, 160, 0, 160, 1, 160, 1),
(710, 474, 30, 1, 195, 0, 195, 1, 195, 1, 195, 0, 195, 1, 195, 1, 195, 3, 195, 1, 195, 1),
(711, 474, 33, 0, 97.5, 0, 97.5, 1, 97.5, 1, 97.5, 0, 97.5, 1, 97.5, 1, 97.5, 6, 97.5, 1, 97.5, 1),
(712, 475, 32, 1, 95, 0, 95, 1, 95, 1, 95, 0, 95, 1, 95, 1, 95, 0, 95, 1, 95, 1),
(713, 476, 30, 1, 535, 4, 535, 1, 535, 1, 535, 0, 535, 1, 535, 1, 535, 9, 535, 1, 535, 1),
(714, 477, 30, 1, 33, 0, 33, 1, 33, 1, 34, 0, 34, 1, 32, 6, 33, 0, 33, 1, 33, 1),
(715, 477, 33, 0, 33, 0, 33, 1, 33, 1, 34, 0, 34, 1, 34, 1, 33, 0, 33, 1, 33, 1),
(716, 469, 33, 0, 320, 0, 320, 1, 320, 1, 318, 0, 318, 1, 318, 1, 320, 0, 320, 1, 320, 1),
(717, 478, 30, 1, 275, 0, 275, 1, 275, 1, 275, 0, 275, 1, 275, 1, 275, 0, 275, 1, 275, 1),
(718, 479, 30, 1, 275, 0, 275, 1, 275, 1, 275, 0, 275, 1, 275, 1, 275, 0, 275, 1, 275, 1),
(719, 480, 30, 1, 173, 70, 173, 1, 173, 1, 173, 87, 173, 1, 173, 1, 173, 82, 173, 1, 173, 1),
(720, 481, 30, 1, 31, 72.5, 31, 1, 31, 1, 31, 538.5, 31, 1, 23.5, 30, 31, 0, 31, 1, 31, 1),
(721, 481, 33, 0, 31, 143, 31, 1, 31, 1, 31, 1077, 31, 1, 31, 1, 31, 0, 31, 1, 31, 1),
(722, 479, 33, 0, 137.5, 0, 137.5, 1, 137.5, 1, 137.5, 0, 137.5, 1, 137.5, 1, 137.5, 0, 137.5, 1, 137.5, 1),
(723, 478, 33, 0, 137.5, 0, 137.5, 1, 137.5, 1, 137.5, 0, 137.5, 1, 137.5, 1, 137.5, 0, 137.5, 1, 137.5, 1),
(724, 438, 33, 0, 70, 0, 70, 1, 70, 1, 70, 0, 70, 1, 70, 1, 70, 0, 70, 1, 70, 1),
(725, 425, 33, 0, 139, 2, 139, 1, 139, 1, 139, 12, 139, 1, 139, 1, 139, 0, 139, 1, 139, 1),
(726, 303, 33, 0, 137.5, 0, 137.5, 1, 137.5, 1, 137.5, 0, 137.5, 1, 137.5, 1, 137.5, 0, 137.5, 1, 137.5, 1),
(727, 129, 33, 0, 110, 4, 110, 1, 110, 1, 110, 16, 110, 1, 110, 1, 110, 18, 110, 1, 110, 1),
(728, 126, 33, 0, 100, 0, 100, 1, 100, 1, 100, 8, 100, 1, 100, 1, 100, 14, 100, 1, 100, 1),
(729, 125, 33, 0, 120, 20, 120, 1, 120, 1, 120, 14, 120, 1, 120, 1, 120, 20, 120, 1, 120, 1),
(730, 124, 33, 0, 125, 0, 125, 1, 125, 1, 120, 0, 120, 1, 120, 1, 125, 0, 125, 1, 125, 1),
(731, 482, 30, 1, 33, 0, 33, 1, 33, 1, 33, 48.5, 33, 1, 27.5, 30, 33, 0, 33, 1, 33, 1),
(732, 482, 33, 0, 33, 0, 33, 1, 33, 1, 33, 97, 33, 1, 33, 1, 33, 0, 33, 1, 33, 1),
(733, 483, 30, 1, 375, 0, 375, 0, 375, 0, 375, 0, 375, 0, 375, 0, 375, 10, 375, 0, 375, 0),
(734, 483, 33, 0, 187.5, 0, 187.5, 0, 187.5, 0, 187.5, 0, 187.5, 0, 187.5, 0, 187.5, 10, 187.5, 0, 187.5, 0),
(735, 484, 36, 1, 510, 0, 510, 1, 510, 1, 515, 2, 515, 1, 515, 1, 510, 0, 510, 1, 510, 1),
(736, 485, 30, 1, 36, 0, 36, 1, 36, 1, 36, 0, 36, 1, 36, 1, 36, 0, 36, 1, 36, 1),
(737, 485, 33, 0, 36, 0, 36, 1, 36, 1, 36, 0, 36, 1, 36, 1, 36, 0, 36, 1, 36, 1),
(738, 486, 36, 1, 880, 0, 880, 1, 880, 1, 880, 0, 880, 1, 860, 17, 880, 8, 880, 1, 880, 1),
(739, 487, 36, 1, 580, 2, 580, 1, 580, 1, 580, 5, 580, 1, 580, 1, 580, 6, 580, 1, 580, 1),
(740, 488, 30, 1, 490, 0, 490, 1, 490, 1, 490, 0, 490, 1, 490, 1, 490, 16.5, 490, 1, 490, 1),
(741, 488, 33, 0, 245, 0, 245, 1, 245, 1, 245, 0, 245, 1, 245, 1, 245, 33, 245, 1, 245, 1),
(742, 489, 30, 1, 1100, 0, 1100, 0, 1100, 0, 1100, 0, 1100, 0, 1100, 0, 1100, 0, 1100, 0, 1100, 0),
(743, 490, 30, 1, 540, 0, 540, 1, 540, 1, 540, 0, 540, 1, 540, 1, 540, 3.5, 540, 1, 540, 1),
(744, 490, 33, 0, 270, 0, 270, 0, 270, 0, 270, 0, 270, 0, 270, 0, 270, 7, 270, 0, 270, 0),
(745, 491, 30, 1, 200, 0, 200, 1, 200, 1, 200, 0, 200, 1, 200, 1, 200, 0, 200, 1, 200, 1),
(746, 491, 33, 0, 100, 0, 100, 1, 100, 1, 100, 0, 100, 1, 100, 1, 100, 0, 100, 1, 100, 1),
(747, 492, 30, 1, 960, 10, 960, 1, 960, 1, 960, 0, 960, 1, 960, 1, 960, 10, 960, 1, 960, 1),
(748, 493, 32, 1, 250, 3, 250, 1, 250, 1, 250, 0, 250, 1, 250, 1, 250, 4, 250, 1, 250, 1),
(749, 494, 30, 1, 37, 0, 37, 1, 37, 1, 39, 0, 39, 1, 36.5, 5, 37, 9, 37, 1, 37, 1),
(750, 494, 33, 0, 37, 0, 37, 1, 37, 1, 39, 0, 39, 1, 39, 1, 37, 18, 37, 1, 37, 1),
(751, 495, 30, 1, 38, 0, 38, 1, 38, 1, 38, 0, 38, 1, 38, 1, 38, 12, 38, 1, 38, 1),
(752, 495, 33, 0, 38, 0, 38, 1, 38, 1, 38, 0, 38, 1, 38, 1, 38, 24, 38, 1, 38, 1),
(753, 496, 30, 1, 145, 4, 145, 1, 145, 1, 145, 0, 145, 1, 145, 1, 145, 4, 145, 1, 145, 1),
(754, 496, 33, 0, 72.5, 8, 72.5, 1, 72.5, 1, 72.5, 0, 72.5, 1, 72.5, 1, 72.5, 8, 72.5, 1, 72.5, 1),
(755, 497, 32, 1, 50, 4, 50, 1, 50, 1, 50, 0, 50, 1, 50, 1, 50, 8, 50, 1, 50, 1),
(756, 498, 30, 1, 155, 0, 155, 1, 155, 1, 155, 0, 155, 1, 155, 1, 155, 0, 155, 1, 155, 1),
(757, 498, 33, 0, 77.5, 0, 77.5, 1, 77.5, 1, 77.5, 0, 77.5, 1, 77.5, 1, 77.5, 0, 77.5, 1, 77.5, 1),
(758, 499, 32, 1, 50, 0, 50, 1, 50, 1, 50, 0, 50, 1, 50, 1, 50, 1, 50, 1, 50, 1),
(759, 500, 30, 1, 375, 5, 375, 1, 375, 1, 375, 0, 375, 1, 375, 1, 375, 6.5, 375, 1, 375, 1),
(760, 500, 33, 0, 187.5, 11, 187.5, 1, 187.5, 1, 187.5, 0, 187.5, 1, 187.5, 1, 187.5, 13, 187.5, 1, 187.5, 1),
(761, 501, 30, 1, 335, 3.5, 335, 1, 335, 1, 335, 0, 335, 1, 335, 1, 335, 5, 335, 1, 335, 1),
(762, 501, 33, 0, 167.5, 7, 167.5, 1, 167.5, 1, 167.5, 0, 167.5, 1, 167.5, 1, 167.5, 10, 167.5, 1, 167.5, 1),
(763, 502, 30, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1),
(764, 502, 33, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1),
(765, 503, 30, 1, 335, 4, 335, 1, 335, 1, 335, 0, 335, 1, 335, 1, 335, 5.5, 335, 1, 335, 1),
(766, 503, 33, 0, 167.5, 8, 167.5, 1, 167.5, 1, 167.5, 0, 167.5, 1, 167.5, 1, 167.5, 11, 167.5, 1, 167.5, 1),
(767, 504, 30, 1, 335, 4, 335, 1, 335, 1, 335, 0.5, 335, 1, 335, 1, 335, 5, 335, 1, 335, 1),
(768, 504, 33, 0, 167.5, 8, 167.5, 1, 167.5, 1, 167.5, 1, 167.5, 1, 167.5, 1, 167.5, 10, 167.5, 1, 167.5, 1),
(769, 505, 30, 1, 435, 1, 435, 1, 435, 1, 435, 0, 435, 1, 435, 1, 435, 1, 435, 1, 435, 1),
(770, 505, 33, 0, 217.5, 2, 217.5, 1, 217.5, 1, 217.5, 0, 217.5, 1, 217.5, 1, 217.5, 2, 217.5, 1, 217.5, 1),
(771, 506, 32, 1, 85, 5, 85, 1, 85, 1, 85, 4, 85, 1, 85, 1, 85, 6, 85, 1, 85, 1),
(772, 507, 30, 1, 650, 0, 650, 1, 650, 1, 650, 0, 650, 1, 650, 1, 650, 8, 650, 1, 650, 1),
(773, 508, 32, 1, 175, 0, 175, 1, 175, 1, 175, 0, 175, 1, 175, 1, 175, 7, 175, 1, 175, 1),
(774, 509, 30, 1, 160, 0, 160, 1, 160, 1, 160, 0, 160, 1, 160, 1, 160, 0, 160, 1, 160, 1),
(775, 510, 30, 1, 225, 2, 225, 1, 225, 1, 225, 0, 225, 1, 225, 1, 225, 3, 225, 1, 225, 1),
(776, 510, 33, 0, 112.5, 4, 112.5, 1, 112.5, 1, 112.5, 0, 112.5, 1, 112.5, 1, 112.5, 6, 112.5, 1, 112.5, 1),
(777, 511, 32, 1, 315, 0, 315, 1, 315, 1, 315, 6, 315, 1, 315, 1, 315, 0, 315, 1, 315, 1),
(778, 512, 32, 1, 50, 1, 50, 1, 50, 1, 50, 2, 50, 1, 50, 1, 50, 2, 50, 1, 50, 1),
(779, 513, 30, 1, 31, 0, 31, 1, 31, 1, 31, 0, 31, 1, 31, 1, 31, 0, 31, 1, 31, 1),
(780, 513, 33, 0, 31, 0, 31, 1, 31, 1, 31, 0, 31, 1, 31, 1, 31, 0, 31, 1, 31, 1),
(781, 514, 32, 1, 20, 0, 20, 1, 20, 1, 20, 1, 20, 1, 20, 1, 20, 0, 20, 1, 20, 1),
(782, 515, 32, 1, 18, 12, 18, 1, 18, 1, 18, 4, 18, 1, 18, 1, 18, 12, 18, 1, 18, 1),
(783, 516, 30, 1, 43, 0, 43, 1, 43, 1, 43, 0, 43, 1, 43, 1, 43, 7.5, 43, 1, 43, 1),
(784, 516, 33, 0, 43, 0, 43, 1, 43, 1, 43, 0, 43, 1, 43, 1, 43, 15, 43, 1, 43, 1),
(785, 517, 36, 1, 340, 0, 340, 1, 340, 1, 340, 0, 340, 1, 340, 1, 340, 0, 340, 1, 340, 1),
(786, 518, 30, 1, 34, 0, 34, 1, 34, 1, 34, 0, 34, 1, 33, 2, 34, 0, 34, 1, 34, 1),
(787, 518, 33, 0, 34, 0, 34, 1, 34, 1, 34, 0, 34, 1, 34, 1, 34, 0, 34, 1, 34, 1),
(788, 519, 36, 1, 150, 0, 150, 1, 140, 4, 150, 0, 150, 1, 150, 1, 150, 0, 150, 1, 140, 4),
(789, 520, 30, 1, 550, 3, 550, 1, 550, 1, 550, 3, 550, 1, 550, 1, 550, 0, 550, 1, 550, 1),
(790, 520, 33, 0, 275, 6, 275, 1, 275, 1, 275, 6, 275, 1, 275, 1, 275, 0, 275, 1, 275, 1),
(791, 521, 30, 1, 280, 0, 280, 1, 280, 1, 280, 0, 280, 1, 280, 1, 280, 0, 280, 1, 280, 1),
(792, 522, 32, 1, 20, 0, 20, 1, 20, 1, 20, 0, 20, 1, 20, 1, 20, 0, 20, 1, 20, 1),
(793, 523, 30, 1, 150, 0, 150, 1, 150, 1, 150, 0, 150, 1, 150, 1, 150, 10, 150, 1, 150, 1),
(794, 524, 30, 1, 38, 0, 38, 1, 38, 1, 38, 0, 38, 1, 38, 1, 38, 0, 38, 1, 38, 1),
(795, 524, 33, 0, 38, 0, 38, 1, 38, 1, 38, 0, 38, 1, 38, 1, 38, 0, 38, 1, 38, 1),
(796, 525, 30, 1, 532, 0, 532, 1, 532, 1, 532, 2, 532, 1, 532, 1, 532, 0, 532, 1, 532, 1),
(797, 526, 30, 1, 26, 6, 26, 1, 26, 1, 25, 5, 25, 1, 25, 1, 26, 6, 26, 1, 26, 1),
(799, 527, 30, 1, 42, 0, 42, 1, 42, 1, 43, 0, 43, 1, 43, 1, 42, 11, 42, 1, 42, 1),
(800, 527, 33, 0, 42, 0, 42, 1, 42, 1, 43, 0, 43, 1, 43, 1, 42, 22, 42, 1, 42, 1),
(801, 528, 30, 1, 505, 0, 505, 1, 505, 1, 505, 0, 505, 1, 505, 1, 505, 1, 505, 1, 505, 1),
(802, 528, 33, 0, 252.5, 0, 252.5, 1, 252.5, 1, 252.5, 0, 252.5, 1, 252.5, 1, 252.5, 2, 252.5, 1, 252.5, 1),
(803, 529, 30, 1, 215, 0, 215, 1, 215, 1, 215, 0.5, 215, 1, 215, 1, 215, 0, 215, 1, 215, 1),
(804, 530, 32, 1, 42, 2, 42, 1, 42, 1, 42, 0, 42, 1, 42, 1, 42, 2, 42, 1, 42, 1),
(805, 531, 30, 1, 540, 0, 540, 1, 540, 1, 540, 0, 540, 1, 540, 1, 540, 0, 540, 1, 540, 1),
(806, 532, 32, 1, 145, 0, 145, 1, 145, 1, 145, 0, 145, 1, 145, 1, 145, 0, 145, 1, 145, 1),
(807, 533, 36, 1, 180, 0, 180, 1, 180, 1, 180, 0, 180, 1, 180, 1, 180, 0, 180, 1, 180, 1),
(808, 534, 30, 1, 150, 0, 150, 1, 150, 1, 150, 0, 150, 1, 150, 1, 150, 10, 150, 1, 150, 1),
(809, 534, 33, 0, 75, 0, 75, 1, 75, 1, 75, 0, 75, 1, 75, 1, 75, 20, 75, 1, 75, 1),
(810, 535, 30, 1, 26, 0, 26, 1, 26, 1, 26, 0, 26, 1, 26, 1, 26, 14.5, 26, 1, 26, 1),
(811, 535, 33, 0, 26, 0, 26, 1, 26, 1, 26, 0, 26, 1, 26, 1, 26, 29, 26, 1, 26, 1),
(812, 536, 30, 1, 24, 0, 24, 1, 23, 3, 24, 0, 24, 1, 24, 1, 24, 0, 24, 1, 23, 3),
(813, 536, 30, 0, 24, 0, 24, 1, 24, 1, 24, 0, 24, 1, 24, 1, 24, 0, 24, 1, 24, 1),
(814, 537, 30, 1, 25, 0, 25, 1, 25, 1, 25, 0, 25, 1, 25, 1, 25, 0, 25, 1, 25, 1),
(815, 537, 33, 0, 25, 0, 25, 1, 25, 1, 25, 0, 25, 1, 25, 1, 25, 0, 25, 1, 25, 1),
(816, 538, 30, 1, 28, 0, 28, 1, 28, 1, 28, 0, 28, 1, 28, 1, 28, 0, 28, 1, 28, 1),
(817, 538, 33, 0, 28, 0, 28, 1, 28, 1, 28, 0, 28, 1, 28, 1, 28, 0, 28, 1, 28, 1),
(818, 539, 30, 1, 36, 0, 36, 1, 36, 1, 36, 0, 36, 1, 34.5, 75, 36, 0, 36, 1, 36, 1),
(819, 539, 33, 0, 36, 0, 36, 1, 36, 1, 36, 0, 36, 1, 36, 1, 36, 0, 36, 1, 36, 1),
(820, 540, 30, 1, 30, 0, 30, 1, 30, 1, 28.7, 0, 28.7, 1, 28.7, 4, 30, 0, 30, 1, 30, 1),
(821, 540, 33, 0, 30, 0, 30, 1, 30, 1, 30, 0, 30, 1, 30, 1, 30, 0, 30, 1, 30, 1),
(822, 541, 30, 1, 30, 0, 30, 1, 30, 1, 30, 0, 30, 1, 30, 1, 30, 0, 30, 1, 30, 1),
(823, 541, 33, 0, 30, 0, 30, 1, 30, 1, 30, 0, 30, 1, 30, 1, 30, 0, 30, 1, 30, 1),
(824, 542, 30, 1, 35, 0, 35, 1, 35, 1, 35, 26, 35, 1, 35, 1, 35, 0, 35, 1, 35, 1),
(825, 542, 33, 0, 35, 0, 35, 1, 35, 1, 35, 52, 35, 1, 35, 1, 35, 0, 35, 1, 35, 1),
(826, 543, 30, 1, 28, 0, 28, 1, 28, 1, 28, 0, 28, 1, 28, 1, 28, 0, 28, 1, 28, 1),
(827, 543, 33, 0, 28, 0, 28, 1, 28, 1, 28, 0, 28, 1, 28, 1, 28, 0, 28, 1, 28, 1),
(828, 544, 30, 1, 28, 0, 28, 1, 28, 1, 28, 0, 28, 1, 28, 1, 28, 0, 28, 1, 28, 1),
(829, 544, 33, 0, 28, 0, 28, 1, 28, 1, 28, 0, 28, 1, 28, 1, 28, 0, 28, 1, 28, 1),
(830, 545, 30, 1, 29, 0, 29, 1, 29, 1, 29, 0, 29, 1, 29, 1, 29, 0, 29, 1, 29, 1),
(831, 545, 33, 0, 29, 0, 29, 1, 29, 1, 29, 0, 29, 1, 29, 1, 29, 0, 29, 1, 29, 1),
(832, 546, 30, 1, 160, 0, 160, 1, 160, 1, 160, 19, 160, 1, 155, 4, 160, 0, 160, 1, 160, 1),
(833, 547, 36, 1, 370, 1, 370, 1, 370, 1, 370, 256, 370, 1, 370, 1, 370, 1, 370, 1, 370, 1),
(834, 548, 36, 1, 390, 6, 390, 1, 390, 1, 390, 0, 390, 1, 390, 1, 390, 8, 390, 1, 390, 1),
(835, 549, 30, 1, 35, 25, 35, 1, 35, 1, 35, 0, 35, 1, 35, 1, 35, 25, 35, 1, 35, 1),
(836, 550, 30, 1, 35, 0, 35, 1, 35, 1, 35, 0, 35, 1, 37, 10, 35, 0, 35, 1, 35, 1),
(837, 550, 33, 0, 35, 0, 35, 1, 35, 1, 35, 0, 35, 1, 35, 1, 35, 0, 35, 1, 35, 1),
(838, 551, 30, 1, 35, 0, 35, 1, 35, 1, 35, 0, 35, 1, 35, 1, 35, 26.5, 35, 1, 35, 1),
(839, 551, 33, 0, 35, 0, 35, 1, 35, 1, 35, 0, 35, 1, 35, 1, 35, 53, 35, 1, 35, 1),
(840, 552, 30, 1, 32, 0, 32, 1, 32, 1, 32, 1.5, 32, 1, 31, 5, 32, 0, 32, 1, 32, 1),
(841, 552, 33, 0, 32, 0, 32, 1, 32, 1, 32, 3, 32, 1, 32, 1, 32, 0, 32, 1, 32, 1),
(842, 553, 30, 1, 30, 0, 30, 1, 30, 1, 29, 0, 29, 1, 29, 1, 30, 0, 30, 1, 30, 1),
(843, 553, 33, 0, 30, 0, 30, 1, 30, 1, 29, 0, 29, 1, 29, 1, 30, 0, 30, 1, 30, 1),
(844, 554, 30, 1, 25, 50, 25, 1, 25, 1, 25, 0, 25, 1, 23.5, 5, 25, 0, 25, 1, 25, 1),
(845, 554, 33, 0, 25, 96, 25, 1, 25, 1, 25, 0, 25, 1, 25, 1, 25, 0, 25, 1, 25, 1),
(846, 555, 30, 1, 31, 0, 31, 1, 31, 1, 31, 0, 31, 1, 31, 1, 31, 0, 31, 1, 31, 1),
(847, 555, 33, 0, 31, 0, 31, 1, 31, 1, 31, 0, 31, 1, 31, 1, 31, 0, 31, 1, 31, 1),
(848, 556, 30, 1, 153.79, 114, 140, 10, 120, 15, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ticket`
--

DROP TABLE IF EXISTS `ticket`;
CREATE TABLE IF NOT EXISTS `ticket` (
  `id_ticket` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` text CHARACTER SET utf8 NOT NULL,
  `mensaje` text CHARACTER SET utf8 NOT NULL,
  `mensaje2` text CHARACTER SET utf8 NOT NULL,
  `fuente` int(1) NOT NULL,
  `tamano` int(2) NOT NULL,
  `margensup` int(2) NOT NULL,
  PRIMARY KEY (`id_ticket`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ticket`
--

INSERT INTO `ticket` (`id_ticket`, `titulo`, `mensaje`, `mensaje2`, `fuente`, `tamano`, `margensup`) VALUES
(1, 'AVICOLA SANTOS', 'DIRECIÓN: CENTRAL DE ABASTOS', '¡GRACIAS POR SU COMPRA! ', 2, 12, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `turno`
--

DROP TABLE IF EXISTS `turno`;
CREATE TABLE IF NOT EXISTS `turno` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bodega` int(1) NOT NULL,
  `fecha` date NOT NULL,
  `horaa` time NOT NULL,
  `fechacierre` date NOT NULL,
  `horac` time NOT NULL,
  `cantidad` float NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `status` varchar(50) NOT NULL,
  `user` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `turno`
--

INSERT INTO `turno` (`id`, `bodega`, `fecha`, `horaa`, `fechacierre`, `horac`, `cantidad`, `nombre`, `status`, `user`) VALUES
(1, 1, '2023-10-12', '19:02:52', '2024-06-05', '22:41:31', 1, 'Mangoo', 'cerrado', 'user'),
(2, 1, '2024-06-05', '22:41:46', '2024-06-05', '22:42:16', 2, 'JESUS', 'cerrado', 'user'),
(3, 1, '2024-06-05', '22:42:37', '0000-00-00', '00:00:00', 500, 'DANIEL', 'abierto', 'user');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE IF NOT EXISTS `usuarios` (
  `UsuarioID` int(11) NOT NULL AUTO_INCREMENT,
  `perfilId` int(11) NOT NULL,
  `personalId` int(11) NOT NULL,
  `Usuario` varchar(45) DEFAULT NULL,
  `contrasena` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`UsuarioID`),
  KEY `fk_usuarios_Perfiles1_idx` (`perfilId`),
  KEY `fk_usuarios_personal1_idx` (`personalId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`UsuarioID`, `perfilId`, `personalId`, `Usuario`, `contrasena`) VALUES
(1, 1, 1, 'admin', '$2y$10$.yBpfsTTEmhYhECki.qy3eL45XaLo3MinuIPgiPsjmXBYItZXHUga'),
(2, 2, 2, 'jesus', '$2y$10$AtxEuEw6DICCVcRIKt73I.SCvSa3UbgK6ArCj31Acjk2FzmmiYgEy');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ventas`
--

DROP TABLE IF EXISTS `ventas`;
CREATE TABLE IF NOT EXISTS `ventas` (
  `id_venta` int(11) NOT NULL AUTO_INCREMENT,
  `id_personal` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `metodo` int(1) NOT NULL COMMENT '1 efectivo, 2 credito, 3 debito',
  `bodega` int(1) NOT NULL,
  `monto_total` double NOT NULL,
  `facturada` int(1) NOT NULL DEFAULT 0,
  `datosfactura` text CHARACTER SET utf8 NOT NULL,
  `descuento` float NOT NULL,
  `cancelado` int(11) NOT NULL,
  `hcancelacion` date NOT NULL,
  `reg` timestamp NOT NULL DEFAULT current_timestamp(),
  `idventa_alias` int(11) NOT NULL DEFAULT 0,
  `alias` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_venta`),
  UNIQUE KEY `id_venta_UNIQUE` (`id_venta`),
  KEY `constraint_fk_30` (`id_cliente`),
  KEY `constraint_fk_31` (`id_personal`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ventas`
--

INSERT INTO `ventas` (`id_venta`, `id_personal`, `id_cliente`, `metodo`, `bodega`, `monto_total`, `facturada`, `datosfactura`, `descuento`, `cancelado`, `hcancelacion`, `reg`, `idventa_alias`, `alias`) VALUES
(1, 1, 45, 1, 1, 1010, 0, '', 0, 1, '2023-10-12', '2023-10-12 20:46:47', 1, 0),
(2, 1, 45, 1, 1, 620, 0, '', 0, 0, '0000-00-00', '2023-10-13 21:24:07', 2, 0),
(3, 1, 45, 5, 1, 185, 0, '', 0, 0, '0000-00-00', '2023-10-13 21:28:53', 3, 0),
(4, 1, 45, 1, 1, 1210, 0, '', 0, 0, '0000-00-00', '2023-10-16 21:07:47', 4, 0),
(5, 1, 45, 1, 1, 5619.5, 0, '', 0, 1, '2023-10-16', '2023-10-16 21:50:16', 5, 0),
(6, 2, 45, 1, 1, 1181, 0, '', 0, 0, '0000-00-00', '2023-10-20 19:10:52', 6, 0),
(7, 1, 45, 1, 1, 496, 0, '', 0, 0, '0000-00-00', '2023-10-20 21:29:37', 7, 0),
(8, 1, 45, 1, 1, 150, 0, '', 0, 0, '0000-00-00', '2023-11-17 21:50:21', 8, 0),
(9, 1, 45, 1, 1, 153.79, 0, '', 0, 1, '2023-11-28', '2023-11-28 22:06:02', 9, 0),
(10, 1, 45, 1, 1, 682, 0, '', 0, 0, '0000-00-00', '2023-11-28 22:21:07', 10, 0),
(11, 1, 45, 1, 1, 341, 0, '', 0, 1, '2023-11-28', '2023-11-28 22:23:24', 11, 0),
(12, 1, 45, 1, 1, 930, 0, '', 0, 1, '2023-11-28', '2023-11-28 22:31:13', 12, 0),
(13, 1, 45, 1, 1, 153.79, 0, '', 0, 1, '2023-11-28', '2023-11-28 22:35:29', 13, 0),
(14, 1, 45, 1, 1, 153.79, 0, '', 0, 0, '0000-00-00', '2023-11-30 16:00:44', 14, 0),
(15, 1, 45, 1, 1, 153.79, 0, '', 0, 0, '0000-00-00', '2023-11-30 16:00:44', 15, 0),
(16, 1, 45, 1, 1, 153.79, 0, '', 0, 0, '0000-00-00', '2023-11-30 16:03:52', 16, 0),
(17, 1, 45, 1, 1, 153.79, 0, '', 0, 0, '0000-00-00', '2023-11-30 16:04:08', 17, 0),
(18, 1, 45, 1, 1, 153.79, 0, '', 0, 0, '0000-00-00', '2023-11-30 16:04:09', 18, 0),
(19, 1, 45, 1, 1, 153.79, 0, '', 0, 1, '2023-11-30', '2023-11-30 16:04:34', 19, 0),
(20, 1, 45, 1, 1, 153.79, 0, '', 0, 0, '0000-00-00', '2023-11-30 16:08:03', 20, 0),
(21, 1, 45, 1, 1, 250, 0, '', 0, 1, '2024-06-27', '2024-06-27 21:08:11', 21, 0),
(22, 1, 45, 1, 1, 250, 0, '', 0, 1, '2024-06-27', '2024-06-27 21:16:17', 22, 0),
(23, 1, 45, 1, 1, 250, 0, '', 0, 1, '2024-06-27', '2024-06-27 21:33:37', 23, 0),
(24, 1, 45, 1, 1, 250, 0, '', 0, 1, '2024-06-27', '2024-06-27 22:05:36', 24, 0),
(25, 1, 45, 1, 1, 275, 0, '', 0, 1, '2024-06-27', '2024-06-27 22:07:37', 25, 0),
(26, 1, 45, 1, 1, 275, 0, '', 0, 1, '2024-06-27', '2024-06-27 22:09:34', 26, 0),
(27, 1, 45, 1, 1, 275, 0, '', 0, 1, '2024-06-27', '2024-06-27 22:13:37', 27, 0),
(28, 1, 45, 1, 1, 275, 0, '', 0, 1, '2024-06-27', '2024-06-27 22:23:09', 28, 0),
(29, 1, 45, 1, 1, 275, 0, '', 0, 1, '2024-06-27', '2024-06-27 22:25:49', 29, 0),
(30, 1, 45, 1, 1, 275, 0, '', 0, 1, '2024-06-27', '2024-06-27 22:43:53', 30, 0),
(31, 1, 45, 1, 1, 275, 0, '', 0, 1, '2024-06-27', '2024-06-27 22:57:42', 31, 0),
(32, 1, 45, 1, 1, 275, 0, '', 0, 1, '2024-06-27', '2024-06-27 23:01:18', 32, 0),
(33, 1, 45, 1, 1, 275, 0, '', 0, 0, '0000-00-00', '2024-06-27 23:06:04', 33, 0),
(34, 1, 45, 1, 1, 275, 0, '', 0, 1, '2024-06-27', '2024-06-27 23:19:37', 34, 0),
(35, 1, 45, 1, 1, 275, 0, '', 0, 1, '2024-06-27', '2024-06-27 23:32:05', 35, 0),
(36, 1, 45, 1, 1, 550, 0, '', 0, 1, '2024-06-27', '2024-06-27 23:34:20', 36, 0),
(37, 1, 45, 1, 1, 550, 0, '', 0, 1, '2024-06-27', '2024-06-27 23:51:51', 37, 0),
(38, 1, 45, 1, 1, 625, 0, '', 0, 1, '2024-06-27', '2024-06-28 00:07:25', 38, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `venta_detalle`
--

DROP TABLE IF EXISTS `venta_detalle`;
CREATE TABLE IF NOT EXISTS `venta_detalle` (
  `id_detalle_venta` int(11) NOT NULL AUTO_INCREMENT,
  `id_venta` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `cantidad` float NOT NULL,
  `kilos` float NOT NULL COMMENT 'los kilos solo sera para el caso de los huevos',
  `precio` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id_detalle_venta`),
  UNIQUE KEY `id_detalle_venta_UNIQUE` (`id_detalle_venta`),
  KEY `fk_detalle_producto` (`id_producto`),
  KEY `fk_detalle_venta` (`id_venta`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `venta_detalle`
--

INSERT INTO `venta_detalle` (`id_detalle_venta`, `id_venta`, `id_producto`, `cantidad`, `kilos`, `precio`) VALUES
(1, 1, 84, 1, 0, '1010.00'),
(2, 2, 720, 1, 20, '31.00'),
(3, 3, 99, 1, 0, '185.00'),
(4, 4, 84, 1, 0, '1000.00'),
(5, 4, 721, 1, 7, '30.00'),
(6, 5, 84, 3, 0, '1000.00'),
(7, 5, 99, 1, 0, '184.00'),
(8, 5, 721, 1, 8, '31.00'),
(9, 5, 760, 1, 0, '187.50'),
(10, 5, 150, 5, 0, '400.00'),
(11, 6, 84, 1, 0, '1001.00'),
(12, 6, 101, 1, 0, '180.00'),
(13, 7, 720, 1, 16, '31.00'),
(14, 8, 848, 1, 0, '150.00'),
(15, 9, 848, 1, 0, '153.79'),
(16, 10, 721, 1, 22, '31.00'),
(17, 11, 721, 1, 11, '31.00'),
(18, 12, 720, 1, 30, '31.00'),
(19, 13, 848, 1, 0, '153.79'),
(20, 14, 848, 1, 0, '153.79'),
(21, 15, 848, 1, 0, '153.79'),
(22, 16, 848, 1, 0, '153.79'),
(23, 17, 848, 1, 0, '153.79'),
(24, 18, 848, 1, 0, '153.79'),
(25, 19, 848, 1, 0, '153.79'),
(26, 20, 848, 1, 0, '153.79'),
(27, 21, 845, 1, 10, '25.00'),
(28, 22, 845, 1, 10, '25.00'),
(29, 23, 845, 1, 10, '25.00'),
(30, 24, 845, 1, 10, '25.00'),
(31, 25, 845, 1, 11, '25.00'),
(32, 26, 845, 1, 11, '25.00'),
(33, 27, 845, 1, 11, '25.00'),
(34, 28, 845, 1, 11, '25.00'),
(35, 29, 845, 10, 11, '25.00'),
(36, 30, 845, 1, 11, '25.00'),
(37, 31, 845, 1, 11, '25.00'),
(38, 32, 845, 1, 11, '25.00'),
(39, 33, 845, 1, 11, '25.00'),
(40, 34, 845, 1, 11, '25.00'),
(41, 35, 845, 1, 11, '25.00'),
(42, 36, 844, 2, 22, '25.00'),
(43, 37, 844, 2, 22, '25.00'),
(44, 38, 844, 2, 25, '25.00');

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `compras`
--
ALTER TABLE `compras`
  ADD CONSTRAINT `fk_compra_proveedor` FOREIGN KEY (`id_proveedor`) REFERENCES `proveedores` (`id_proveedor`) ON UPDATE NO ACTION;

--
-- Filtros para la tabla `compra_detalle`
--
ALTER TABLE `compra_detalle`
  ADD CONSTRAINT `fk_compra_compra` FOREIGN KEY (`id_compra`) REFERENCES `compras` (`id_compra`),
  ADD CONSTRAINT `fk_compra_producto` FOREIGN KEY (`id_producto`) REFERENCES `sproductosub` (`subId`) ON UPDATE NO ACTION;

--
-- Filtros para la tabla `menu_sub`
--
ALTER TABLE `menu_sub`
  ADD CONSTRAINT `fk_menu_sub_menu` FOREIGN KEY (`MenuId`) REFERENCES `menu` (`MenuId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `pagos_ventas`
--
ALTER TABLE `pagos_ventas`
  ADD CONSTRAINT `venta_fk_pagos` FOREIGN KEY (`id_venta`) REFERENCES `ventas` (`id_venta`) ON UPDATE NO ACTION;

--
-- Filtros para la tabla `perfiles_detalles`
--
ALTER TABLE `perfiles_detalles`
  ADD CONSTRAINT `fk_Perfiles_detalles_Perfiles1` FOREIGN KEY (`perfilId`) REFERENCES `perfiles` (`perfilId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Perfiles_detalles_menu_sub1` FOREIGN KEY (`MenusubId`) REFERENCES `menu_sub` (`MenusubId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `personal_menu`
--
ALTER TABLE `personal_menu`
  ADD CONSTRAINT `personal_fkmenu` FOREIGN KEY (`MenuId`) REFERENCES `menu_sub` (`MenusubId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `personal_fkpersona` FOREIGN KEY (`personalId`) REFERENCES `personal` (`personalId`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `sproducto`
--
ALTER TABLE `sproducto`
  ADD CONSTRAINT `producto_fk_categoria` FOREIGN KEY (`productoId`) REFERENCES `categoria` (`categoriaId`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `producto_fk_marca` FOREIGN KEY (`MarcaId`) REFERENCES `marca` (`marcaid`) ON UPDATE NO ACTION;

--
-- Filtros para la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD CONSTRAINT `fk_usuarios_Perfiles1` FOREIGN KEY (`perfilId`) REFERENCES `perfiles` (`perfilId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `usuarios_ibfk_1` FOREIGN KEY (`personalId`) REFERENCES `personal` (`personalId`);

--
-- Filtros para la tabla `ventas`
--
ALTER TABLE `ventas`
  ADD CONSTRAINT `fk_venta_cliente` FOREIGN KEY (`id_cliente`) REFERENCES `clientes` (`ClientesId`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_venta_presonal` FOREIGN KEY (`id_personal`) REFERENCES `personal` (`personalId`) ON UPDATE NO ACTION;

--
-- Filtros para la tabla `venta_detalle`
--
ALTER TABLE `venta_detalle`
  ADD CONSTRAINT `fk_detalle_producto` FOREIGN KEY (`id_producto`) REFERENCES `sproductosub` (`subId`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_detalle_venta` FOREIGN KEY (`id_venta`) REFERENCES `ventas` (`id_venta`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
